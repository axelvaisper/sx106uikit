<?php
########################################################################
# ******************  SX CONTENT MANAGEMENT SYSTEM  ****************** #
# *       Copyright © Alexander Voloshin * All Rights Reserved       * #
# ******************************************************************** #
# *  http://sx-cms.ru   *  cms@sx-cms.ru  *   http://www.status-x.ru * #
# ******************************************************************** #
########################################################################
if (!defined('SX_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

if (!get_active('content')) {
    SX::object('Core')->notActive();
}
SX::object('Content')->get(Arr::getRequest('id'));


switch (Arr::getRequest('action')) {
    default:
        SX::object('Content')->get(Arr::getRequest('id'));
        break;

    case 'categ':
        SX::object('Content')->categ(Arr::getRequest('id'));
        break;
}