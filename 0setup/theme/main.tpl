<!DOCTYPE html>
<html lang="ru" dir="ltr" class="uk-height-1-1">
<head>
<meta charset="UTF-8" />
<title>{$title}</title>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="content-language" content="ru" />
<meta name="generator" content="{#version#}" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link type="text/css" rel="stylesheet" href="{$setupdir}/theme/setup.css" />
<script src="{$setupdir}/theme/jquery-2.1.1.js"></script>
<script src="{$homedir}/js/jvalidate.js"></script>
{* <script src="{$setupdir}/theme/setup.js"></script> *}

{* <script>
<!-- //
$(function() {

});
//-->
</script> *}

</head>
<body class="uk-height-1-1">

<div class="uk-flex uk-flex-center uk-flex-middle uk-background-muted uk-height-viewport">
  <div class="uk-width-medium uk-padding-small">
    <div class="uk-margin">
      {$content}
    </div>
  </div>
</div>

</body>
</html>
