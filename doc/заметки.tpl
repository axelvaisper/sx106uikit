      {if $admin_settings.Navi == 'right'}
        {include file="$incpath/navielements/navielements.tpl"}
      {/if}

       {if $admin_settings.Navi == 'left'}
      {include file="$incpath/navielements/navielements.tpl"}
    {/if}

     {if $admin_settings.Navi_Anime == 1}
            <img id="navielements" style="cursor: pointer" class="stip" title="{#NaviAnime#}" src="{$imgpath}/navielem.png" alt="" />
          {/if}

          uk-toggle="animation:  uk-animation-slide-left-small, uk-animation-slide-left-small; cls: ; mode: media; media: @s"

          {if $new != 1}

            {* <header class="uk-background-fixed uk-background-cover uk-flex uk-flex-center uk-flex-middle" style="background-image: url({$imgpath}/page/cover.jpg);min-height:50vh">
    <div class="uk-width-1-1 uk-flex uk-flex-center uk-flex-middle">

      <div class="uk-position-top zindex-2">
        {navigation id=2 tpl='navi-topbar.tpl'}
        <hr class="uk-margin-remove" style="border-color:#0000001a">
        {navigation id=1 tpl='shopnav.tpl'}
      </div>

        <div class="uk-width-1-3@m slogan">
          <h4 class="uk-heading-divider uk-margin-remove" uk-scrollspy="cls: uk-animation-slide-top-small;delay:1000">
          {$settings.Firma|sanitize}</h4>
          <h1 class="uk-margin-remove" uk-scrollspy="cls: uk-animation-slide-bottom-small;delay:1000">{$settings.Seitenname|sanitize}</h1>
        </div>

    </div>
  </header> *}

   <div id="art2" class="uk-modal-full" uk-modal>
    <div class="uk-modal-dialog">
      <button class="uk-modal-close-full uk-text-muted" type="button" uk-icon="icon:close;ratio:2"></button>
      <div uk-overflow-auto>
        {content id=2}
      </div>
    </div>
  </div>