<div class="wrapper">
  <h1 class="title">{$galname|sanitize}</h1>

  {if empty($galleries)}
      <div class="box-content">
        {#Gallery_NoGalleries#}
      </div>
  {else}
      <form method="post" action="index.php?p=gallery&amp;action=showincluded&amp;categ={$smarty.request.categ}&amp;name=-&amp;area={$area}">
        <div class="input-group">
          <input class="form-control" type="text" name="q" value="{$smarty.request.q|urldecode|sanitize|replace:'empty': ''}" />
          <select class="form-control" name="searchtype">
            <option value="full" {if $smarty.request.searchtype == 'full'}selected="selected" {/if}>{#SearchFull#}</option>
            <option value="tags" {if $smarty.request.searchtype == 'tags'}selected="selected" {/if}>{#SearchTags#}</option>
          </select>
          <span class="input-group-append">
            <input class="btn btn-primary" type="submit" value="{#Search#}" />
          </span>
        </div>
        <input name="log" type="hidden" value="1" />
      </form>

      <div class="spacer flex-line justify-content-center">
        <span class="mr-3">{#SortBy#}:</span>
        <a class="mr-3" href="index.php?p=gallery&amp;action=showincluded&amp;categ={$smarty.request.categ}&amp;name={$galname|translit}&amp;q={$smarty.request.q|sanitize|default:'empty'}&amp;searchtype={$smarty.request.searchtype|sanitize}&amp;page={$smarty.request.page|default:1}&amp;sort={$def_sort_name|default:'namedesc'}&amp;area={$area}">
          {#SortName#}<i class="{$def_sort_img_name}"></i>
        </a>
        <a class="mr-3" href="index.php?p=gallery&amp;action=showincluded&amp;categ={$smarty.request.categ}&amp;name={$galname|translit}&amp;q={$smarty.request.q|sanitize|default:'empty'}&amp;searchtype={$smarty.request.searchtype|sanitize}&amp;page={$smarty.request.page|default:1}&amp;sort={$def_sort_date|default:'datedesc'}&amp;area={$area}">
          {#SortDate#}<i class="{$def_sort_img_date}"></i>
        </a>
        <a class="mr-3" href="index.php?p=gallery&amp;action=showincluded&amp;categ={$smarty.request.categ}&amp;name={$galname|translit}&amp;q={$smarty.request.q|sanitize|default:'empty'}&amp;searchtype={$smarty.request.searchtype|sanitize}&amp;page={$smarty.request.page|default:1}&amp;sort={$def_sort_author|default:'userdesc'}&amp;area={$area}">
          {#SortBy_Author#}<i class="{$def_sort_img_author}"></i>
        </a>
      </div>

      {foreach from=$galleries item=item}
          <div class="box-content">
            <h3 class="listing-head text-truncate">
              <a href="{$item->Link}">{$item->GalName|sanitize}</a>
            </h3>
            <div class="d-flex">
              <div class="mr-3" data-toggle="tooltip" title="{$item->GalText|tooltip}">
                {if !empty($item->Thumb)}
                    <a href="{$item->Link}">
                      <img class="img-thumbnail" src="{$item->Thumb}" alt="{$item->GalName|sanitize}" />
                    </a>
                {else}
                    <img class="img-fluid" src="uploads/other/noimage.png" alt="" />
                {/if}
              </div>

              <div>
                {if !empty($item->GalText)}
                    <div class="spacer">
                      {$item->GalText|html_truncate:200}
                    </div>
                {/if}
                {if $item->subGalleries}
                    <div class="spacer">
                      {foreach from=$item->subGalleries name=c item=child}
                          <a class="mr-2 text-nowrap" href="{$child->Link}">
                            <i class="icon-folder-open"></i>{$child->SubGalName|sanitize}
                          </a>
                      {/foreach}
                    </div>
                {/if}
                {if $item->Tags}
                    <div class="spacer">
                      {foreach from=$item->Tags item=ttags name=tag}
                          {if !empty($ttags)}
                              <a class="mr-2 text-nowrap" href="index.php?p=gallery&amp;action=showincluded&amp;categ={$smarty.request.categ}&amp;name={$galname|translit}&amp;q={$ttags|urlencode|tagchars}&amp;searchtype=tags&amp;page=1&amp;sort=nameasc&amp;area={$area}">
                                <i class="icon-tags"></i>{$ttags|tagchars}
                              </a>
                          {/if}
                      {/foreach}
                    </div>
                {/if}
              </div>
            </div>

            <div class="listing-foot flex-line justify-content-center">
              <span class="mr-4" data-toggle="tooltip" title="{#GlobalAutor#}">
                <i class="icon-user"></i><a href="{$item->AuthorLink}">{$item->Author|sanitize}</a>
              </span>
              <span class="mr-4" data-toggle="tooltip" title="{#Added#}">
                <i class="icon-calendar"></i>{$item->Datum|date_format:$lang.DateFormatSimple}
              </span>
              <span class="mr-4" data-toggle="tooltip" title="{#Images#}">
                <i class="icon-picture"></i>{$item->ImageCount->ImageCount}
              </span>
            </div>
          </div>
      {/foreach}

      {if !empty($GalNavi)}
          <div class="wrapper">
            {$GalNavi}
          </div>
      {/if}

      {if $tagCloud}
          <div class="h3 title">{#Tagcloud#}</div>
          <div class="box-content d-flex flex-wrap align-items-center">
            {foreach from=$tagCloud item=tC}
                <div class="{$tC->Class} mr-3">
                  <a href="index.php?p=gallery&amp;action=showincluded&amp;categ={$smarty.request.categ}&amp;name={$galname|translit}&amp;q={$tC->Name|urlencode|tagchars}&amp;searchtype=tags&amp;page=1&amp;sort=nameasc&amp;area={$area}">
                    <i class="icon-tags"></i>{$tC->Name|tagchars}
                  </a>
                </div>
            {/foreach}
          </div>
      {/if}
  {/if}
</div>
