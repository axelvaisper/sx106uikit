{if $Galsettings->Favoriten == 1}
<script>
<!-- //
function addFavorite(id, image, galid) {
    $.get('index.php?action=addfavorite&p=gallery', {
        img_id: image,
        gal_id: galid
    },
    function(data){
        document.getElementById(id).innerHTML = data;
        document.getElementById('favolink_' + image).style.display = 'none';
    });
}
function deleteFavorite(id, image, galid) {
    $.get('index.php?action=deletefavorite&p=gallery', {
        img_id: image,
        gal_id: galid
    },
    function(data) {
        document.getElementById(id).innerHTML = data;
        document.getElementById('del_favolink_' + image).style.display = 'none';
    });
}
//-->
</script>
{/if}

<div class="wrapper">
  <h1 class="title text-truncate">{$data->ImageName|sanitize|default:$lang.GlobalNoName}</h1>
  <div class="d-flex justify-content-center">
  <img class="img-fluid" src="{$data->Image}" alt="{$item->name}" title="{$item->name}" />
  </div>
  <div class="box-content">
    <h2>{$data->ImageName|sanitize|default:$lang.GlobalNoName}</h2>
    {if !empty($data->ImageText)}
        <div class="spacer">
          {$data->ImageText}
        </div>
    {/if}
  </div>

  <div class="d-flex justify-content-between">
    <div data-toggle="tooltip" title="{#Gallery_ImagePrev#}">
      {if $data->PrefImageLink}
          <a href="{$data->PrefImageLink}"><i class="icon-left-open"></i></a>
          <a href="{$data->PrefImageLink}">{$data->PrefImage}</a>
      {/if}
    </div>
    <div data-toggle="tooltip" title="{#Gallery_ImageNext#}">
      {if $data->NextImageLink}
          <a href="{$data->NextImageLink}">{$data->NextImage}</a>
          <a href="{$data->NextImageLink}"><i class="icon-right-open"></i></a>
          {/if}
    </div>
  </div>
</div>

<div class="h3 title">{#GlobalActions#}</div>
<div class="box-content">
  {if $Galsettings->Favoriten == 1 && $loggedin}
      {if $Favorites && in_array($data->Id, $Favorites)}
          <div id="del_favolink_{$data->Id}">
            <a href="javascript:void(0);" onclick="deleteFavorite('favo_{$data->Id}',{$data->Id},{$smarty.request.galid});">
              <i class="icon-cancel"></i>{#Gallery_DeleteFavorite#}
            </a>
          </div>
      {else}
          <div id="favolink_{$data->Id}">
            {if get_active('gallery_favorites')}
                <a href="javascript:void(0);" onclick="{if !$loggedin}alert('{#Gallery_JsAlertNotLogged#}');{else}addFavorite('favo_{$data->Id}',{$data->Id},{$smarty.request.galid});{/if}">
                  <i class="icon-plus"></i>{#Gallery_AddFavorite#}
                </a>
            {/if}
          </div>
      {/if}
      <div class="text-success" id="favo_{$data->Id}"></div>
  {/if}

  {if permission('gallery_download') && $gs->Download == 1}
      <a href="javascript:void(0);" onclick="location.href = 'index.php?download=1&amp;p=gallery&amp;action=showimage&amp;id={$smarty.request.id}&amp;galid={$smarty.request.galid}';">
        <i class="icon-download"></i>{#Gallery_Download#} ({$data->Image_Width}x{$data->Image_Height})
      </a>
  {/if}
</div>

{$GetComments}
