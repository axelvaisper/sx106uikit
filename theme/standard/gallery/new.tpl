{if !empty($NewGalleryEntries)}
    <div>
      <div class="h3 title">{#Gallery_NameNew#}</div>
      {foreach from=$NewGalleryEntries item=e}
          <div class="box-content">
            <h3 class="listing-head text-truncate">
              <a title="{$e->Name|sanitize}" href="{$e->Link}">{$e->Name|sanitize}</a>
            </h3>
            <div class="clearfix">
              {if !empty($e->Img)}
                  <a href="{$e->Link}">
                    <img class="img-fluid listing-img-right" src="{$e->Img}" alt="{$e->GalName|sanitize}" />
                  </a>
              {/if}
              <div class="text-justify">
                {$e->Text|truncate:500}
              </div>
            </div>
          </div>
      {/foreach}
    </div>
{/if}
