<div class="wrapper">
  <h1 class="title text-truncate">{$Gallery_inf->TitleGalName|sanitize}</h1>

  <form method="post" action="index.php">
    <div class="input-group">
      <select class="form-control" name="ascdesc">
        <option value="desc" {if isset($smarty.request.ascdesc) && $smarty.request.ascdesc == "desc"}selected="selected"{/if}>{#NewestFirst#}</option>
        <option value="asc" {if isset($smarty.request.ascdesc) && $smarty.request.ascdesc == "asc"}selected="selected"{/if}>{#OldestFirst#}</option>
      </select>

      <select class="form-control" name="pp">
        <option value="{$Galsettings->Bilder_Seite}" {if isset($smarty.request.pp) && $smarty.request.pp == $Galsettings->Bilder_Seite}selected="selected"{/if}>{$Galsettings->Bilder_Seite} {#Gallery_ImagesPP#}</option>
        {section name=pp loop=95 step=5}
            <option value="{$smarty.section.pp.index+10}" {if isset($smarty.request.pp) && $smarty.request.pp == $smarty.section.pp.index+10}selected="selected"{/if}>{$smarty.section.pp.index+10} {#Gallery_ImagesPP#}</option>
        {/section}
      </select>
      <span class="input-group-append">
        <input class="btn btn-primary" type="submit" value="{#Gallery_PPButton#}" />
      </span>
    </div>
    <input type="hidden" name="p" value="gallery" />
    <input type="hidden" name="action" value="showgallery" />
    <input type="hidden" name="id" value="{$smarty.request.id}" />
    <input type="hidden" name="categ" value="{$smarty.request.categ}" />
    <input type="hidden" name="name" value="xxx" />
    <input type="hidden" name="page" value="1" />
    {if isset($smarty.request.favorites) && $smarty.request.favorites == 1}
        <input type="hidden" name="favorites" value="1" />
    {/if}
  </form>

  <div class="box-content row no-gutters">
    <div class="col-auto mr-3">

      {if !empty($items)}
          <div class="carousel slide" data-ride="carousel" data-interval="3000">
            <div class="carousel-inner" role="listbox">
              {foreach from=$items item=gal name=sls}
                  <div class="carousel-item{if $smarty.foreach.sls.first} active{/if}">
                    <a class="d-block img-fluid" href="index.php?p=gallery&amp;action=showimage&amp;id={$gal->Id}&amp;galid={$smarty.request.id}&amp;ascdesc={$smarty.request.ascdesc|default:'desc'}&amp;categ={$smarty.request.categ}&amp;area={$area}"><img src="{$gal->Thumbnail}" alt="" /></a>
                  </div>
              {/foreach}
            </div>
          </div>
      {else}
          <img class="img-fluid" src="uploads/other/noimage.png" alt="" />
      {/if}

    </div>
    <div class="col">
      {if $Gallery_inf->GalText}
          {$Gallery_inf->GalText}
      {/if}
      {if $subGalleries}
          <div class="spacer">
            {foreach from=$subGalleries item=subgallery name=subgalleries}
                <a class="mr-2 text-nowrap" href="{$subgallery->Link}">
                  <i class="icon-folder-open"></i>{$subgallery->SubGalName|sanitize}
                </a>
            {/foreach}
          </div>
      {/if}
    </div>
  </div>

  <div class="box-content">
    {if empty($items)}
        {#Gallery_NoImages#}
    {else}
        <div class="row spacer">
          {assign var=c value=0}
          {foreach from=$items item=gal name=sls}
              {assign var=c value=$c+1}
              <div class="col d-flex justify-content-center">
                <div>
                  <a href="index.php?p=gallery&amp;action=showimage&amp;id={$gal->Id}&amp;galid={$smarty.request.id}&amp;ascdesc={$smarty.request.ascdesc|default:'desc'}&amp;categ={$smarty.request.categ}&amp;area={$area}">
                    <img class="img-fluid" data-toggle="tooltip" title="{$gal->ImageName}" src="{$gal->Thumbnail}" alt="{$gal->ImageName}" />
                  </a>
                  {if $Galsettings->Info_Klein == 1}
                      <div class="text-center">
                        <span class="mr-1" data-toggle="tooltip" title="{#Global_Hits#}"><i class="icon-eye"></i>{$gal->Klicks}</span>
                        <span data-toggle="tooltip" title="{#Comments#}"><i class="icon-chat"></i>{$gal->Comments}</span>
                      </div>
                  {/if}
                </div>
              </div>

              {if $c % $Galsettings->Bilder_Zeile == 0 && !$smarty.foreach.sls.last}
              </div>
              <div class="row spacer">
              {/if}
          {/foreach}
        </div>

        <div class="listing-foot flex-line justify-content-center">
          <span class="mr-4" data-toggle="tooltip" title="{#GlobalAutor#}">
            <i class="icon-user"></i>{$Gallery_inf->AutorLink}
          </span>
          <span class="mr-4" data-toggle="tooltip" title="{#Added#}">
            <i class="icon-calendar"></i>{$Gallery_inf->Datum|date_format:$lang.DateFormatSimple}
          </span>
          <span class="mr-4" data-toggle="tooltip" title="{#Images#}">
            <i class="icon-picture"></i>{$Gallery_inf->Images}
          </span>
        </div>
    {/if}
  </div>
</div>

{if $GalNavi}
    <div class="wrapper">
      {$GalNavi}
    </div>
{/if}

<div class="h3 title">{#GlobalActions#}</div>
<div class="box-content">
  {if $Galsettings->Favoriten == 1 && $loggedin}
      {if isset($smarty.request.favorites) && $smarty.request.favorites == 1}
          <a class="d-block" href="index.php?p=gallery&amp;action=showgallery&amp;id={$smarty.request.id}&amp;categ={$smarty.request.categ}&amp;name={$Gallery_inf->GalName|translit}&amp;area={$area}">
            <i class="icon-th"></i>{#Gallery_ShowAllImages#}
          </a>
          {if $items}
              <a class="d-block" href="index.php?p=gallery&amp;action=delete_allfavorites&amp;galid={$smarty.request.id}&amp;categ={$smarty.request.categ}&amp;name={$Gallery_inf->GalName|translit}">
                <i class="icon-cancel"></i>{#Gallery_DelFavorites#}
              </a>
          {/if}
      {else}
          {if get_active('gallery_favorites')}
              <a class="d-block" href="index.php?p=gallery&amp;action=showgallery&amp;id={$smarty.request.id}&amp;categ={$smarty.request.categ}&amp;name={$Gallery_inf->GalName|translit}&amp;favorites=1&amp;area={$area}">
                <i class="icon-bookmark-empty"></i>{#Gallery_ShowFavorites#}
              </a>
          {/if}
      {/if}
  {/if}
</div>
