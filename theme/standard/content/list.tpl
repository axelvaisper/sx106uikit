<div class="wrapper">
  <h1 class="title text-truncate">{$cattitle|sanitize}</h1>

  {foreach from=$contents item=e}
      <div class="box-content">
        <h3 class="listing-head">
          <a title="{$e.Titel|sanitize}" href="{$e.Link}">{$e.Titel|sanitize}</a>
        </h3>

        <div class="clearfix">
          {if !empty($e.Bild)}
              <a href="{$e.Link}">
                <img class="img-fluid {if $e.BildAusrichtung == 'right'}listing-img-right{else}listing-img-left{/if}" src="uploads/content/{$e.Bild}" alt="{$e.Titel|sanitize}" />
              </a>
          {/if}
          <div class="text-justify">
            {$e.Inhalt|html_truncate:500}
          </div>
        </div>

        <div class="listing-foot text-right">
          <a href="{$e.Link}"><i class="icon-right-open"></i>{#MoreDetails#}</a>
        </div>
      </div>
  {/foreach}
</div>

{if !empty($Navi)}
    <div class="wrapper">
      {$Navi}
    </div>
{/if}
