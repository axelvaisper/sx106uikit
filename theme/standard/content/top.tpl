{if $contentitems}
    <div>
      {foreach from=$contentitems item=content}
          <div class="box-content clearfix">
            {if !empty($content.TopcontentBild)}
                <a href="index.php?p=content&amp;id={$content.Id}&amp;name={$content.Titel|translit}&amp;area={$content.Sektion}">
                  <img class="img-fluid img-left" src="uploads/content/{$content.TopcontentBild}" alt="{$content.Titel|sanitize}" />
                </a>
            {elseif !empty($content.Bild)}
                <a href="index.php?p=content&amp;id={$content.Id}&amp;name={$content.Titel|translit}&amp;area={$content.Sektion}">
                  <img class="img-fluid {if $content.BildAusrichtung == 'right'}img-right{else}img-left{/if}" src="uploads/content/{$content.Bild}" alt="{$content.Titel|sanitize}" />
                </a>
            {/if}
            <h3>
              <a href="index.php?p=content&amp;id={$content.Id}&amp;name={$content.Titel|translit}&amp;area={$content.Sektion}">{$content.Titel|sanitize}</a>
            </h3>
            {$content.Inhalt|html_truncate:400}
          </div>
      {/foreach}
    </div>
{/if}
