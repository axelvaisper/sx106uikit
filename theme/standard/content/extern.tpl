{if $externContent}
    <div class="wrapper">
      <div class="h3 title">{#ParentDocs#}</div>
      <div class="box-content">
        {foreach from=$externContent item=ne}
            <div data-toggle="tooltip" title="{$ne.Inhalt|tooltip:200}" class="text-truncate">
              <a href="index.php?p=content&amp;id={$ne.Id}&amp;name={$ne.Titel|translit}&amp;area={$area}">
                {$ne.Titel|sanitize}
              </a>
            </div>
        {/foreach}
      </div>
    </div>
{/if}
