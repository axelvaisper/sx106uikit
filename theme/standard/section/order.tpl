<section id="order" class="uk-background-fixed uk-background-cover uk-flex uk-flex-center uk-flex-middle" style="padding-bottom: 4em; padding-top: 4em; background-image: url({$imgpath}/page/order.jpg);">
  <div class="uk-container uk-container-small">
      {contact id=3 tpl='order.tpl'}
  </div>
</section>