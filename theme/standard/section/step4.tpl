<section id="step4" class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
  <div class="uk-container@s uk-container-small">

    <div class="uk-text-center">
      <h2 class="module-title">Как мы работаем</h2>
      <h3 class="module-subtitle uk-margin-remove">Большинство сделок обычно проходят в четыре простых шага.</h3>
    </div>

    <div class="uk-child-width-1-2@m" uk-grid uk-scrollspy="target: > div; cls:uk-animation-fade; delay: 500">

      <div >
        <div class="service-style-4">
          <span class="service-count" uk-icon="icon:mail;ratio:.15"></span>
          <h4>1.Заявка и консультация</h4>
          <p>После получении заявки мы проводим консультацию по предпочтениям клиента.</p>
        </div>
      </div>
      <div >
        <div class="service-style-4">
          <span class="service-count" uk-icon="icon:copy;ratio:4"></span>
          <h4>2.Заключение договора</h4>
          <p>Далее оговариваются обязанности сторон и заключается договор.</p>
        </div>
      </div>
      <div >
        <div class="service-style-4">
          <span class="service-count" uk-icon="icon:users;ratio:4"></span>
          <h4>3.Юридическое сопровождение</h4>
          <p>Проходит сопровождение сделки сотрудниками нашего агентства.</p>
        </div>
      </div>
      <div >
        <div class="service-style-4">
          <span class="service-count" uk-icon="icon:happy;ratio:4"></span>
          <h4>4.Завершение сделки</h4>
          <p>Заключительный расчёт сторон и получение всех документов.</p>
        </div>
      </div>

    </div>

    <div class="uk-margin-small-top uk-text-center">
      <a class="uk-button uk-button-primary" href="#order" uk-scroll>Начать с нами работать</a>
    </div>

  </div>
</section>