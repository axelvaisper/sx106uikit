<div class="uk-child-width-1-3@m" uk-grid uk-lightbox="animation: scale">
    <div>
        <a class="uk-inline" href="images/photo.jpg" data-caption="Caption 1">
            <img src="images/photo.jpg" alt="">
        </a>
    </div>
    <div>
        <a class="uk-inline" href="images/dark.jpg" data-caption="Caption 2">
            <img src="images/dark.jpg" alt="">
        </a>
    </div>
    <div>
        <a class="uk-inline" href="images/light.jpg" data-caption="Caption 3">
            <img src="images/light.jpg" alt="">
        </a>
    </div>
</div>



<div id="modal-1" class="uk-modal-full" uk-modal>
  <div class="uk-modal-dialog">
    <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>

    <div class="uk-contaier" uk-slideshow="animation:pull;ratio: false">
      <div class="uk-position-relative uk-visible-toggle">
        <ul class="uk-slideshow-items" uk-height-viewport>

          <li>
            <img src="/uploads/content/1.jpg" alt="" uk-cover>
            <div uk-slideshow-parallax="x: 600,-600" class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small">
              <h3 class="uk-margin-remove">Станок виброформовочный для изготовления железобетонных колец
                и крышек.</h3>
              <p class="uk-margin-remove">Гидравлический привод для подъёма и переворота матрицы, управляемый бункер для заполнения и выравнивания раствора. виброплатформа и сменные матрицы.</p>
            </div>
          </li>

          <li>
            <img src="/uploads/content/1-1.jpg" alt="" uk-cover>
            <div uk-slideshow-parallax="x: 600,-600" class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small">
              <h3 class="uk-margin-remove">Станок виброформовочный для изготовления железобетонных колец
                и крышек.</h3>
              <p class="uk-margin-remove">Гидравлический привод для подъёма и переворота матрицы, управляемый бункер для заполнения и выравнивания раствора. виброплатформа и сменные матрицы.</p>
            </div>
          </li>

        </ul>

        <a class="uk-slidenav-large uk-position-center-left uk-position-small uk-hidden-hover uk-slidenav-previous uk-icon uk-icon-button uk-slidenav"
          href="#" uk-slidenav-previous="" uk-slideshow-item="previous">
        </a>
        <a class="uk-slidenav-large uk-position-center-right uk-position-small uk-hidden-hover uk-slidenav-next uk-icon uk-icon-button uk-slidenav"
          href="#" uk-slidenav-next="" uk-slideshow-item="next">
        </a>

        <div class="uk-position-top-center uk-position-medium">
          <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin">
            <li uk-slideshow-item="0"><a href="#"></a></li>
            <li uk-slideshow-item="1"><a href="#"></a></li>
          </ul>
        </div>

      </div>
    </div>
  </div>
</div>

<div id="modal-2" class="uk-modal-full" uk-modal>
  <div class="uk-modal-dialog">
    <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>

    <div class="uk-contaier" uk-slideshow="animation:pull;ratio: false">
      <div class="uk-position-relative uk-visible-toggle">
        <ul class="uk-slideshow-items" uk-height-viewport>

          <li>
            <img src="/uploads/content/2.jpg" alt="" uk-cover>
            <div uk-slideshow-parallax="x: 600,-600" class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small">
              <h3 class="uk-margin-remove">Портальный фрезер</h3>
              <p class="uk-margin-remove"></p>
            </div>
          </li>

          <li>
            <img src="/uploads/content/2-1.jpg" alt="" uk-cover>
            <div class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small" uk-slideshow-parallax="x: 600,-600">
              <h3 class="uk-margin-remove">Портальный фрезер</h3>
              <p class="uk-margin-remove"></p>
            </div>
          </li>

        </ul>

        <a class="uk-slidenav-large uk-position-center-left uk-position-small uk-hidden-hover uk-slidenav-previous uk-icon uk-icon-button uk-slidenav"
          href="#" uk-slidenav-previous="" uk-slideshow-item="previous">
        </a>
        <a class="uk-slidenav-large uk-position-center-right uk-position-small uk-hidden-hover uk-slidenav-next uk-icon uk-icon-button uk-slidenav"
          href="#" uk-slidenav-next="" uk-slideshow-item="next">
        </a>

        <div class="uk-position-top-center uk-position-medium">
          <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin">
            <li uk-slideshow-item="0"><a href="#"></a></li>
            <li uk-slideshow-item="1"><a href="#"></a></li>
          </ul>
        </div>

      </div>
    </div>
  </div>
</div>

<div id="modal-3" class="uk-modal-full" uk-modal>
  <div class="uk-modal-dialog">
    <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>

    <div class="uk-contaier" uk-slideshow="animation:pull;ratio: false">
      <div class="uk-position-relative uk-visible-toggle">
        <ul class="uk-slideshow-items" uk-height-viewport>

          <li>
            <img src="/uploads/content/4.jpg" alt="" uk-cover>
            <div uk-slideshow-parallax="x: 600,-600" class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small">
              <h3 class="uk-margin-remove">Стенд для измерения клиновых ремней</h3>
              <p class="uk-margin-remove"></p>
            </div>
          </li>

          <li>
            <img src="/uploads/content/4-1.jpg" alt="" uk-cover>
            <div class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small" uk-slideshow-parallax="x: 600,-600">
              <h3 class="uk-margin-remove">Стенд для измерения клиновых ремней</h3>
            </div>
          </li>

        </ul>

        <a class="uk-slidenav-large uk-position-center-left uk-position-small uk-hidden-hover uk-slidenav-previous uk-icon uk-icon-button uk-slidenav"
          href="#" uk-slidenav-previous="" uk-slideshow-item="previous">
        </a>
        <a class="uk-slidenav-large uk-position-center-right uk-position-small uk-hidden-hover uk-slidenav-next uk-icon uk-icon-button uk-slidenav"
          href="#" uk-slidenav-next="" uk-slideshow-item="next">
        </a>

        <div class="uk-position-top-center uk-position-medium">
          <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin">
            <li uk-slideshow-item="0"><a href="#"></a></li>
            <li uk-slideshow-item="1"><a href="#"></a></li>
          </ul>
        </div>

      </div>
    </div>
  </div>
</div>

<div id="modal-4" class="uk-modal-full" uk-modal>
  <div class="uk-modal-dialog">
    <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>

    <div class="uk-contaier" uk-slideshow="animation:pull;ratio: false">
      <div class="uk-position-relative uk-visible-toggle">
        <ul class="uk-slideshow-items" uk-height-viewport>

          <li>
            <img src="/uploads/content/6.jpg" alt="" uk-cover>
            <div uk-slideshow-parallax="x: 600,-600" class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small">
              <h3 class="uk-margin-remove">Линия механизированной лепки кирпича</h3>
            </div>
          </li>

        </ul>

      </div>
    </div>
  </div>
</div>

<div id="modal-5" class="uk-modal-full" uk-modal>
  <div class="uk-modal-dialog">
    <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>

    <div class="uk-contaier" uk-slideshow="animation:pull;ratio: false">
      <div class="uk-position-relative uk-visible-toggle">
        <ul class="uk-slideshow-items" uk-height-viewport>

          <li>
            <img src="/uploads/content/7.jpg" alt="" uk-cover>
            <div uk-slideshow-parallax="x: 600,-600" class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small">
              <h3 class="uk-margin-remove">Светильники</h3>
            </div>
          </li>

        </ul>

      </div>
    </div>
  </div>
</div>

<div id="modal-6" class="uk-modal-full" uk-modal>
  <div class="uk-modal-dialog">
    <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>

    <div class="uk-contaier" uk-slideshow="animation:pull;ratio: false">
      <div class="uk-position-relative uk-visible-toggle">
        <ul class="uk-slideshow-items" uk-height-viewport>

          <li>
            <img src="/uploads/content/8.jpg" alt="" uk-cover>
            <div uk-slideshow-parallax="x: 600,-600" class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small">
              <h3 class="uk-margin-remove">Станина для фрезера Makita</h3>
            </div>
          </li>

        </ul>

      </div>
    </div>
  </div>
</div>

<div id="modal-7" class="uk-modal-full" uk-modal>
  <div class="uk-modal-dialog">
    <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>

    <div class="uk-contaier" uk-slideshow="animation:pull;ratio: false">
      <div class="uk-position-relative uk-visible-toggle">
        <ul class="uk-slideshow-items" uk-height-viewport>

          <li>
            <img src="/uploads/content/12.jpg" alt="" uk-cover>
            <div uk-slideshow-parallax="x: 600,-600" class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small">
              <h3 class="uk-margin-remove">Планировка склада</h3>
            </div>
          </li>

        </ul>

      </div>
    </div>
  </div>
</div>

<div id="modal-8" class="uk-modal-full" uk-modal>
  <div class="uk-modal-dialog">
    <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>

    <div class="uk-contaier" uk-slideshow="animation:pull;ratio: false">
      <div class="uk-position-relative uk-visible-toggle">
        <ul class="uk-slideshow-items" uk-height-viewport>

          <li>
            <img src="/uploads/content/10.jpg" alt="" uk-cover>
            <div uk-slideshow-parallax="x: 600,-600" class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small">
              <h3 class="uk-margin-remove">Гидрозахват</h3>
            </div>
          </li>

        </ul>

      </div>
    </div>
  </div>
</div>

<div id="modal-9" class="uk-modal-full" uk-modal>
  <div class="uk-modal-dialog">
    <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>

    <div class="uk-contaier" uk-slideshow="animation:pull;ratio: false">
      <div class="uk-position-relative uk-visible-toggle">
        <ul class="uk-slideshow-items" uk-height-viewport>

          <li>
            <img src="/uploads/content/11.jpg" alt="" uk-cover>
            <div class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small" uk-slideshow-parallax="x: 600,-600">
              <h3 class="uk-margin-remove">Меланжеры Kadzama</h3>
              <p class="uk-margin-remove">
                Универсальные машины для получения однородных масс из твердых ингредиентов, точнее шоколада.
                GraviKB участвовало в модернизации, а так же разработке новых типоразмеров.
                Сайт изготовителя - <a href="http://kadzama.com">kadzama.com</a></p>
            </div>
          </li>

        </ul>

      </div>
    </div>
  </div>
</div>