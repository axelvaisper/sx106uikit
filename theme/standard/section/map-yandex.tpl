<div class="uk-position-relative">
    <button id="toggle-map" class="uk-button uk-overlay-default uk-position-cover uk-position-z-index uk-flex uk-flex-center uk-flex-middle uk-width-1-1" uk-toggle="target: #toggle-map;animation: uk-animation-fade">
        Нажмите чтобы воспользоваться картой
    </button>
    <div id="map" style="width:100%;height:400px"></div>
</div>

{script file="//api-maps.yandex.ru/2.1/?lang=ru_RU" position='head' priority='800'}
<script>
    ymaps.ready(init);
    function init(){
        var address = '{$settings.Strasse}';
        var geocoder = ymaps.geocode(address);
        geocoder.then(
                function (res) {
                    var coordinates = res.geoObjects.get(0).geometry.getCoordinates();
                    var map = new ymaps.Map("map", {
                        center: coordinates,
                        zoom: 15,
                        controls: [
                            'typeSelector',
                            'zoomControl'
                        ]
                    });
                    // map.behaviors.disable('scrollZoom');
                    map.geoObjects.add(new ymaps.Placemark(
                            coordinates,
                            {
                              'hintContent': '{$settings.Stadt}',
                            },
                    ));
                }
        );
    }
</script>
