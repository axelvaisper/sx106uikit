<section id="about">
  <div class="uk-container">
    <div class="uk-child-width-1-1@s uk-child-width-1-2@m" uk-grid>

    <div>
      <img src="{$imgpath}/page/about.jpg" alt="office">
    </div>

    <div uk-scrollspy="cls: uk-animation-slide-right-small; delay:300">
      {$insert.about}
    </div>

  </div>
</div>
</section>