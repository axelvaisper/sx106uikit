<section id="contact" class="uk-background-default uk-padding">
    <div class="uk-container">

      <div class="uk-flex uk-flex-center">
        <div class="section-head uk-text-center uk-width-1-2@m">
          <h1>Остались вопросы? Спрашивайте</h1>
          <p>Нас всегда можно найти в Вконтакте. Позвонить самим или оставить номер и мы вам перезвоним, а также написать на почту либо в соц. сети. </p>
        </div>
      </div>

      <div class="uk-child-width-1-4@s uk-grid-divider uk-flex-center" uk-grid>

        {if !empty({$settings.Telefon})}
        <div >
          <a href="tel:{$settings.Telefon|regex_replace:'/[^\d\+]/':''}">
            <div class="uk-card uk-card-body uk-card-hover uk-padding-small">
              <i uk-icon="icon:phone;ratio:2"></i>
              <h5>{#Phone#}</h5>
              <p>{$settings.Telefon|sanitize}</p>
            </div>
          </a>
        </div>
        {/if}

        {if !empty({$settings.Mail_Absender})}
        <div >
          <a href="mailto:{$settings.Mail_Absender|sanitize}" rel="nofollow">
            <div class="uk-card uk-card-body uk-card-hover uk-padding-small">
            <i uk-icon="icon:mail;ratio:.1"></i>
            <h5>{#Email#}</h5>
            <p>{$settings.Mail_Absender|sanitize}</p>
          </div>
          </a>
        </div>
        {/if}

        {if !empty({$settings.Strasse})}
        <div>
          <a href="{$baseurl}/index.php?p=imprint">
            <div class="uk-card uk-card-body uk-card-hover uk-padding-small">
            <i uk-icon="icon:location;ratio:2"></i>
            <h5>Адрес</h5>
            {$settings.Strasse|sanitize}
          </div>
          </a>
        </div>
        {/if}

        {if !empty({$settings.Fax})}
        <div >
          {* <a href="https://vk.com/public{$settings.Fax}">
            <div class="uk-card uk-card-body uk-card-hover">
            <i uk-icon="icon:vk;ratio:.1"></i>
            <h5>Группа VK</h5>
            vk.com/{$settings.Buh|sanitize}
          </div>
          </a> *}

          <div id="vk_groups" class="uk-card uk-card-hover"></div>
          <script type="text/javascript">
          VK.Widgets.Group("vk_groups", { mode: 3, width: "auto", color1: 'F9F9F6', color2: '1C1C1C' }, {$settings.Fax|sanitize});
          </script>
        </div>
        {/if}

      </div>
    </div>
</section>