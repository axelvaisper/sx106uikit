<section id="portfolio" class="uk-container" uk-slideshow="animation:pull;ratio: false">
  <h2 class="uk-text-center">Портфолио</h2>
  <div class="uk-position-relative uk-visible-toggle">
    <ul class="uk-slideshow-items uk-height-large">

      <li>
        <img src="/uploads/content/1.jpg" alt="" uk-cover>
        <div uk-slideshow-parallax="x: 600,-600" class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small">
          <h3 class="uk-margin-remove">Станок виброформовочный для изготовления железобетонных колец и крышек.</h3>
          <p class="uk-margin-remove">Гидравлический привод для подъема и переворота матрицы, управляемый бункер для заполнения и выравнивания раствора. виброплатформа и сменные матрицы.</p>
        </div>
      </li>

      <li class="uk-active uk-transition-active">
        <img src="/uploads/content/2.jpg" alt="" uk-cover>
        <div class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small" uk-slideshow-parallax="x: 600,-600">
          <h3 class="uk-margin-remove">Портальный фрезер</h3>
          <p class="uk-margin-remove">Проектирование</p>
        </div>
      </li>

      <li>
        <img src="/uploads/content/11.jpg" alt="" uk-cover>
        <div class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small" uk-slideshow-parallax="x: 600,-600">
          <h3 class="uk-margin-remove">Меланжеры Kadzama</h3>
          <p class="uk-margin-remove"> 
Универсальные машины для получения однородных масс из твердых ингредиентов, точнее шоколада.
GraviKB участвовало в модернизации, а так же разработке новых типоразмеров.
Сайт изготовителя - <a href="http://kadzama.com">kadzama.com</a></p>
        </div>
      </li>

      <li>
        <img src="/uploads/content/6.jpg" alt="" uk-cover>
        <div class="uk-overlay uk-overlay-default uk-position-bottom-left uk-position-small" uk-slideshow-parallax="x: 600,-600">
          <h3 class="uk-margin-remove">Линия механизированной лепки кирпича</h3>
          <p class="uk-margin-remove">Проектирование</p>
        </div>
      </li>

    </ul>

    <a class="uk-slidenav-large uk-position-center-left uk-position-small uk-hidden-hover uk-slidenav-previous uk-icon uk-slidenav"
      href="#" uk-slidenav-previous="" uk-slideshow-item="previous">
    </a>
    <a class="uk-slidenav-large uk-position-center-right uk-position-small uk-hidden-hover uk-slidenav-next uk-icon uk-slidenav"
      href="#" uk-slidenav-next="" uk-slideshow-item="next">
    </a>

    <div class="uk-position-top-center uk-position-medium">
      <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin">
        <li uk-slideshow-item="0"><a href="#"></a></li>
        <li uk-slideshow-item="1"><a href="#"></a></li>
        <li uk-slideshow-item="2"><a href="#"></a></li>
        <li uk-slideshow-item="3"><a href="#"></a></li>
      </ul>
    </div>

  </div>
</section>