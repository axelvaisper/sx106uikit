<section id="vk-articles" class="uk-container" style="padding-bottom: 4em; padding-top: 4em">

  <h2 class="uk-text-center uk-text-muted">Новые статьи</h2>

  <div class="uk-child-width-1-3@s uk-grid-small" uk-grid>

  <div>
    <div id="vk_article_-169134012_6454"></div>
  </div>

  <div>
    <div id="vk_article_-169134012_6506"></div>
  </div>

  <div>
    <div id="vk_article_-169134012_6617"></div>
  </div>

  <div>
    <div id="vk_article_-169134012_6594"></div>
  </div>
  
  <div>
    <div id="vk_article_-169134012_6670"></div>
  </div>

  <div>
    <div id="vk_article_-169134012_7230"></div>
  </div>

  <div>
    <div id="vk_article_-169134012_7609"></div>
  </div>


    {* <div uk-lightbox>
      <a href="/index.php?p=articles&action=displayarticle&id=2" data-lightbox-type="iframe">
        <figure class="uk-inline">
          <img src="/uploads/content/4t.jpg" alt="">
          <div class="uk-position-cover uk-overlay-primary uk-border-rounded uk-padding-small">
            <figcaption class="uk-position-center uk-text-center">
              <h4 class="uk-text-bold">Как развернуть поверхность в SolidWorks</h4>
              <small></small>
              <button class="uk-button uk-button-small uk-text-bold uk-border-rounded uk-button-secondary"><i uk-icon="icon:flash;ratio:.7"></i>
                Читать</button>
            </figcaption>
          </div>
        </figure>
      </a>
    </div> *}

  </div>

</section>

<script type="text/javascript">
  VK.Widgets.Article("vk_article_-169134012_6454", "@gravikb-bystrye-privyazki-solidworks");
  VK.Widgets.Article("vk_article_-169134012_6506", "@gravikb-kak-otvyazat-detal-ot-toolbox");
  VK.Widgets.Article("vk_article_-169134012_6594", "@gravikb-zachem-konstruktoru-igrovaya-mysh");
  VK.Widgets.Article("vk_article_-169134012_6617", "@gravikb-zhesty-myshi-solidworks");
  VK.Widgets.Article("vk_article_-169134012_6670", "@gravikb-tolschina-detali-v-shtampe-po-ssylke-s-modeli-solidworks");
  VK.Widgets.Article("vk_article_-169134012_7230", "@gravikb-kak-razvernut-poverhnost-solidworks");
  VK.Widgets.Article("vk_article_-169134012_7609", "@gravikb-polki-v-listovom-metalle-solisworks");
</script>