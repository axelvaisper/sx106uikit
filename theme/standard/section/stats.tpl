<section id="stats" class="bg-primary">
<div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-3">

            <div class="stats-info">
                <i class="icon-handshake"></i>
                <p><span class="odometer-block" id="handshake"></span></p>
                <h2>Заключено сделок</h2>
            </div>
        </div>
        <div class="col-md-3 col-sm-3">
            <div class="stats-info">
                <i class="icon-calendar"></i>
                <p><span class="odometer-block" id="icon-calendar"></span></p>
                <h2>Среднее время сделки (мес.)</h2>
            </div>
        </div>
        <div class="col-md-3 col-sm-3">
            <div class="stats-info">
                <i class="icon-group"></i>
                <p><span id="icon-group" class="odometer-block"></span></p>
                <h2>Штат сотрудников</h2>
            </div>
        </div>
        <div class="col-md-3 col-sm-3">
            <div class="stats-info">
                <i class="icon-th"></i>
                <p><span id="icon-th" class="odometer-block"></span></p>
                <h2>Объектов в базе</h2>
            </div>
        </div>
    </div>
</div>
</section>