<section id="portfolio" uk-filter="target: .js-filter" class="uk-container" style="padding-bottom: 4em; padding-top: 4em">
  <h2 class="uk-text-center uk-text-muted">Портфолио</h2>

  <div class="uk-grid-collapse uk-child-width-auto uk-flex-center" uk-grid>
    <div>
      <ul class="uk-tab">
        <li class="uk-active" uk-filter-control><a href="#">Сбросить</a></li>
      </ul>
    </div>
    <div>
      <ul class="uk-tab uk-flex-center">
        <li uk-filter-control="filter: [data-type='develop']; group: data-type"><a href="#">Проектирование</a></li>
        <li uk-filter-control="filter: [data-type='revers']; group: data-type"><a href="#">Реверс-инжиниринг</a></li>
        <li uk-filter-control="filter: [data-type='auto']; group: data-type"><a href="#">Автоматизация</a></li>
        <li uk-filter-control="filter: [data-type='vector']; group: data-type"><a href="#">Векторизация</a></li>
      </ul>
    </div>
    <div>
      <ul class="uk-tab">
        <li uk-filter-control="filter: [data-job='3d']; group: job"><a href="#">3D Моделирование</a></li>
        {* <li uk-filter-control="filter: [data-job='medium']; group: job"><a href="#">Матирование</a></li>
        <li uk-filter-control="filter: [data-job='large']; group: job"><a href="#">Анодирование</a></li> *}
      </ul>
    </div>
  </div>

  {* миниатюры *}
  <ul class="js-filter uk-child-width-1-2 uk-child-width-1-4@m uk-grid-small" uk-grid uk-lightbox="animation: slide">

    <li data-type="develop" data-job="3d" >
      <a href="{$baseurl}/uploads/content/1.jpg" data-caption="Станок виброформовочный для изготовления железобетонных колец и крышек. Гидравлический привод для подъёма и переворота матрицы, управляемый бункер для заполнения и выравнивания раствора. виброплатформа и сменные матрицы.">
        <div class="uk-inline-clip uk-transition-toggle uk-background-cover uk-border-rounded uk-height-small uk-width-1-1" style="background-image: url(/uploads/content/1t.jpg);">
          <div class="uk-transition-slide-bottom-medium uk-padding-small-left uk-overlay-primary uk-top-small uk-position-relative">
            <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand uk-text-bold">Станок виброформовочный</div>
              <div class="uk-width-1-6 uk-text-right uk-text-bold">2</div>
              <p class="uk-margin-small-top">Служит для изготовления железобетонных колец и крышек</p>
            </div>
          </div>
        </div>
      </a>
      <a href="{$baseurl}/uploads/content/1-1.jpg" data-caption="Станок виброформовочный для изготовления железобетонных колец и крышек. Гидравлический привод для подъёма и переворота матрицы, управляемый бункер для заполнения и выравнивания раствора. виброплатформа и сменные матрицы."></a>
    </li>


    <li data-type="develop" data-job="3d">
      <a href="{$baseurl}/uploads/content/2.jpg" data-caption="Портальный фрезер">
        <div class="uk-inline-clip uk-transition-toggle uk-background-cover uk-border-rounded uk-height-small uk-width-1-1" style="background-image: url(/uploads/content/2t.jpg);">
          <div class="uk-transition-slide-bottom-medium uk-padding-small-left uk-top-small uk-overlay-primary uk-position-relative">
            <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand uk-text-bold">Портальный фрезер</div>
              <div class="uk-width-1-6 uk-text-right uk-text-bold">2</div>
            </div>
          </div>
        </div>
      </a>
      <a href="{$baseurl}/uploads/content/2-1.jpg" data-caption="Портальный фрезер"></a>
    </li>

    <li data-type="auto" data-job="3d">
      <a href="{$baseurl}/uploads/content/4.jpg" data-caption="Стенд для измерения клиновых ремней">
        <div class="uk-inline-clip uk-transition-toggle uk-background-cover uk-border-rounded uk-height-small uk-width-1-1" style="background-image: url(/uploads/content/4t.jpg);">
          <div class="uk-transition-slide-bottom-medium uk-padding-small-left uk-top-small uk-overlay-primary uk-position-relative">
            <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand uk-text-bold">Стенд для измерения клиновых ремней</div>
              <div class="uk-width-1-6 uk-text-right uk-text-bold">2</div>
            </div>
          </div>
        </div>
      </a>
      <a href="{$baseurl}/uploads/content/4-1.jpg" data-caption="Стенд для измерения клиновых ремней"></a>
    </li>

    <li data-type="auto" data-job="3d">
      <a href="{$baseurl}/uploads/content/6.jpg" data-caption="Линия механизированной лепки кирпича">
        <div class="uk-inline-clip uk-transition-toggle uk-background-cover uk-border-rounded uk-height-small uk-width-1-1" style="background-image: url(/uploads/content/6t.jpg);">
          <div class="uk-transition-slide-bottom-medium uk-padding-small-left uk-top-small uk-overlay-primary uk-position-relative">
            <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand uk-text-bold">Линия механизированной лепки кирпича</div>
              <div class="uk-width-1-6 uk-text-right uk-text-bold">1</div>
            </div>
          </div>
        </div>
      </a>
    </li>

    <li data-type="revers" data-job="3d">
      <a href="{$baseurl}/uploads/content/7.jpg" data-caption="Светильники">
        <div class="uk-inline-clip uk-transition-toggle uk-background-cover uk-border-rounded uk-height-small uk-width-1-1" style="background-image: url(/uploads/content/7t.jpg);">
          <div class="uk-transition-slide-bottom-medium uk-padding-small-left uk-top-small uk-overlay-primary uk-position-relative">
            <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand uk-text-bold">Светильники</div>
              <div class="uk-width-1-6 uk-text-right uk-text-bold">1</div>
            </div>
          </div>
        </div>
      </a>
    </li>

    <li data-type="revers" data-job="3d">
      <a href="{$baseurl}/uploads/content/8.jpg" data-caption="Станина для фрезера Makita">
        <div class="uk-inline-clip uk-transition-toggle uk-background-cover uk-border-rounded uk-height-small uk-width-1-1" style="background-image: url(/uploads/content/8t.jpg);">
          <div class="uk-transition-slide-bottom-medium uk-padding-small-left uk-top-small uk-overlay-primary uk-position-relative">
            <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand uk-text-bold">Станина для фрезера Makita</div>
              <div class="uk-width-1-6 uk-text-right uk-text-bold">1</div>
            </div>
          </div>
        </div>
      </a>
    </li>

    <li data-type="develop" data-job="3d">
      <a href="{$baseurl}/uploads/content/12.jpg" data-caption="Планировка склада">
        <div class="uk-inline-clip uk-transition-toggle uk-background-cover uk-border-rounded uk-height-small uk-width-1-1" style="background-image: url(/uploads/content/12t.jpg);">
          <div class="uk-transition-slide-bottom-medium uk-padding-small-left uk-top-small uk-overlay-primary uk-position-relative">
            <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand uk-text-bold">Планировка склада</div>
              <div class="uk-width-1-6 uk-text-right uk-text-bold">1</div>
            </div>
          </div>
        </div>
      </a>
    </li>

    <li data-type="develop" data-job="3d">
      <a href="{$baseurl}/uploads/content/10.jpg" data-caption="Гидрозахват">
        <div class="uk-inline-clip uk-transition-toggle uk-background-cover uk-border-rounded uk-height-small uk-width-1-1" style="background-image: url(/uploads/content/10t.jpg);">
          <div class="uk-transition-slide-bottom-medium uk-padding-small-left uk-top-small uk-overlay-primary uk-position-relative">
            <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand uk-text-bold">Гидрозахват</div>
              <div class="uk-width-1-6 uk-text-right uk-text-bold">2</div>
            </div>
          </div>
        </div>
      </a>
      <a href="{$baseurl}/uploads/content/10-1.jpg" data-caption="Гидрозахват"></a>
    </li>

    <li data-type="develop" data-job="3d" >
      <a href="{$baseurl}/uploads/content/11.jpg" data-caption="Универсальные машины для получения однородных масс из твердых ингредиентов, точнее шоколада. GraviKB участвовало в модернизации, а так же разработке новых типоразмеров.">
        <div class="uk-inline-clip uk-transition-toggle uk-background-cover uk-border-rounded uk-height-small uk-width-1-1" style="background-image: url(/uploads/content/11t.jpg);">
          <div class="uk-transition-slide-bottom-medium uk-padding-small-left uk-top-small uk-overlay-primary uk-position-relative">
            <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand uk-text-bold">Меланжеры Kadzama <br> сайт изготовителя kadzama.com</div>
              <div class="uk-width-1-6 uk-text-right uk-text-bold">1</div>
            </div>
          </div>
        </div>
      </a>
    </li>

    <li data-type="develop" data-job="3d">
      <a href="{$baseurl}/uploads/content/13.jpg" data-caption="Микропогрузчик с гидроприводом и управлением с ПЛК. Разработан совместно со специалистами компании imhydro.com">
        <div class="uk-inline-clip uk-transition-toggle uk-background-cover uk-border-rounded uk-height-small uk-width-1-1" style="background-image: url(/uploads/content/13t.jpg);">
          <div class="uk-transition-slide-bottom-medium uk-padding-small-left uk-top-small uk-overlay-primary uk-position-relative">
            <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand uk-text-bold">Микропогрузчик с гидроприводом и управлением с ПЛК. Разработан совместно со специалистами компании imhydro.com</div>
              <div class="uk-width-1-6 uk-text-right uk-text-bold">2</div>
            </div>
          </div>
        </div>
      </a>
      <a href="{$baseurl}/uploads/content/13-1.jpg" data-caption="Микропогрузчик с гидроприводом и управлением с ПЛК. Разработан совместно со специалистами компании imhydro.com"></a>
    </li>

    <li data-type="develop" data-job="3d">
      <a href="{$baseurl}/uploads/content/14.jpg" data-caption="Верстак слесарный. Разработан по аналогии с продукцией Inforce.">
        <div class="uk-inline-clip uk-transition-toggle uk-background-cover uk-border-rounded uk-height-small uk-width-1-1" style="background-image: url(/uploads/content/14t.jpg);">
          <div class="uk-transition-slide-bottom-medium uk-padding-small-left uk-top-small uk-overlay-primary uk-position-relative">
            <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand uk-text-bold">Верстак слесарный. Разработан по аналогии с продукцией Inforce.</div>
              <div class="uk-width-1-6 uk-text-right uk-text-bold">1</div>
            </div>
          </div>
        </div>
      </a>
    </li>

    <li data-type="vector" data-job="">
      <a href="{$baseurl}/uploads/content/15.jpg" data-caption="Заглушка транспортировочная">
        <div class="uk-inline-clip uk-transition-toggle uk-background-cover uk-border-rounded uk-height-small uk-width-1-1" style="background-image: url(/uploads/content/15t.jpg);">
          <div class="uk-transition-slide-bottom-medium uk-padding-small-left uk-top-small uk-overlay-primary uk-position-relative">
            <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand uk-text-bold">Заглушка транспортировочная</div>
              <div class="uk-width-1-6 uk-text-right uk-text-bold">1</div>
            </div>
          </div>
        </div>
      </a>
    </li>

  </ul>

  {* {include file="$incpath/section/filter-modal.tpl"} *}

</section>