<script>
<!-- //
function check_searchform() {
    if(document.sf.qc.value == '') {
        return false;
    }
}
//-->
</script>

<br>

<div class="box_innerhead">{#Calendar_search#}</div>
<form name="sf" action="index.php" method="get" onsubmit="return check_searchform();">
  <div class="form-group">
  <input type="text" name="qc" value="{$smarty.request.qc|sanitize}" class="form-control" />
  </div>
  <input class="btn btn-primary btn-block" type="submit" value="{#Calendar_search#}" />
  <input name="area" type="hidden" value="{$area}" />
  <input name="p" type="hidden" value="calendar" />
  <input name="action" type="hidden" value="search" />
</form>
