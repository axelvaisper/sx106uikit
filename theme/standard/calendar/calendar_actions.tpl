{if permission('calendar_event') || permission('calendar_event_new')}
<a href="index.php?p=calendar&amp;area={$area}&amp;action=newevent&amp;month={$currentmonth|default:$smarty.now|date_format: '%m'}&amp;year={$Year}&amp;area={$area}&amp;show={$showtype}" class="btn btn-sm btn-primary"><i class="icon-plus"></i> {#Calendar_newEvent#}</a>
<a href="index.php?p=calendar&amp;area={$area}&amp;action=myevents" class="btn btn-sm btn-secondary">{#Calendar_MyEvents#}</a>
{/if}
