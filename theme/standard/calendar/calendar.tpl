<section class="container">
<div class="row">
  <div class="col-md-9">
    <h1>{$header} <small class="text-muted">{if $smarty.request.show == 'private'}{#Calendar_private#}{else}{#Calendar_public#}{/if}</small></h1>
  </div>
  <div class="col-md-3 text-center">
    {include file="$incpath/calendar/calendar_actions.tpl"}
  </div>
</div>
<hr>

<div class="row">

  <div class="col-md-9">
    {$calendar}
    <br>
    <table width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"> {$calendar_prev} </td>
        <td valign="top">&nbsp;</td>
        <td valign="top">{$calendar_next}</td>
      </tr>
    </table>
  </div>

  <div class="col-md-3">
    {include file="$incpath/calendar/calendar_jumpform.tpl"}
    {include file="$incpath/calendar/calendar_searchform.tpl"}
      <br>
      <div class="card" class="text-center">
        <div class="card-body">
          <i class="icon-emo-happy"> {#Birthdays_Today#}</i> <br>
          <i class="icon-warning-empty"> {#Calendar_leg_import#}</i> <br>
          <i class="icon-lightbulb"> {#Calendar_period#}</i>
        </div>
      </div>
  </div>

</div>
</section>