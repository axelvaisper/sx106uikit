<!doctype html>
<html lang="{$langcode}" prefix="og: http://ogp.me/ns#">
{include file="$incpath/head/head-home.tpl"}
<body>
{result type='script' format='file' position='body_start'}
{result type='script' format='code' position='body_start'}
{result type='code'   format='code' position='body_start'}

<div id="preloader"><div uk-spinner="ratio: 3"></div></div>

<div class="uk-offcanvas-content">

  <header class="uk-background-fixed uk-background-center-center uk-background-cover" style="background-image: url({$imgpath}/page/cover.jpg)">
    <div class="uk-overlay-cover uk-overlay-primary">
      
      <div class="uk-inline uk-height-large uk-width-1-1">

        <div class="uk-position-top zindex-2">
          {navigation id=1 tpl='navi-home.tpl'}
          <hr class="uk-margin-remove" style="border-color:#0000001a">
        </div>

        <div class="uk-position-center uk-width-1-3@m slogan">
          <h4 class="uk-heading-divider uk-margin-remove" uk-scrollspy="cls: uk-animation-slide-top-small;delay:1000">
          {$settings.Firma|sanitize}</h4>
          <h1 class="uk-margin-remove" uk-scrollspy="cls: uk-animation-slide-bottom-small;delay:1000">{$settings.Seitenname|sanitize}</h1>
        </div>

      </div>

    </div>
  </header>

  {include file="$incpath/section/about.tpl"}
  {* {include file="$incpath/section/slides.tpl"} *}
  {include file="$incpath/section/why.tpl"}
  {include file="$incpath/section/service.tpl"}

  <div class="uk-container">
    <div id="yandex_rtb_R-A-294686-1"></div>
    <script type="text/javascript">
        (function(w, d, n, s, t) {
            w[n] = w[n] || [];
            w[n].push(function() {
                Ya.Context.AdvManager.render({
                    blockId: "R-A-294686-1",
                    renderTo: "yandex_rtb_R-A-294686-1",
                    async: true
                });
            });
            t = d.getElementsByTagName("script")[0];
            s = d.createElement("script");
            s.type = "text/javascript";
            s.src = "//an.yandex.ru/system/context.js";
            s.async = true;
            t.parentNode.insertBefore(s, t);
        })(this, this.document, "yandexContextAsyncCallbacks");
    </script>
  </div>

  {include file="$incpath/section/filter.tpl"}
  {* {include file="$incpath/section/step4.tpl"}
  {include file="$incpath/section/stats.tpl"}
  {$PartnerDisplay} *}
  {include file="$incpath/section/order.tpl"}
  {$content}
 
  {include file="$incpath/section/contact-info.tpl"}
  {* {include file="$incpath/section/map-yandex.tpl"} *}
  {* {include file="$incpath/footer/footer-home.tpl"} *}
  {$Newsletter}

</div><!--offcanvas-->

{include file="$incpath/other/vk-chat.tpl"}
{include file="$incpath/js/jsend-home.tpl"}

{result type='code'   format='code' position='body_end'}
{result type='script' format='file' position='body_end'}
{result type='script' format='code' position='body_end'}
</body>
</html>
