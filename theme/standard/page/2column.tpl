<!doctype html>
<html lang="{$langcode}">
  {include file="$incpath/other/head.tpl"}
  <body>
    {result type='script' format='file' position='body_start'} {* вывод файлов скриптов *}
    {result type='script' format='code' position='body_start'} {* вывод кода скриптов *}
    {result type='code' format='code' position='body_start'}   {* вывод кода *}

    {include file="$incpath/other/header.tpl"}

    <main class="main">
      <div class="container">
        <div class="row">

          <div class="col-md-3">
            <div class="d-none d-md-block">
              {navigation id=2 tpl='sidebar.tpl'}
              {include file="$incpath/shop/basket_saved_small.tpl"}
              {$user_login}
              {bookmarks}
              {$SmallCalendar}
              {$SmallCalendarNewEvents}
              {$NewUsers}
              {include file="$incpath/other/outlinks.tpl"}
            </div>
          </div>

          <div class="col-md-9">
            {$content}
          </div>
        </div>
      </div>
    </main>

    {include file="$incpath/other/footer.tpl"}
    {include file="$incpath/other/google.tpl"}

    {result type='code' format='code' position='body_end'}   {* вывод кода *}
    {result type='script' format='file' position='body_end'} {* вывод файлов скриптов *}
    {result type='script' format='code' position='body_end'} {* вывод кода скриптов *}
  </body>
</html>
