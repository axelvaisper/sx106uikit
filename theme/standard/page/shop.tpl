<!doctype html>
<html lang="{$langcode}" prefix="og: http://ogp.me/ns#">
{include file="$incpath/head/head-shop.tpl"}
<body class="shop">
{result type='script' format='file' position='body_start'}
{result type='script' format='code' position='body_start'}
{result type='code'   format='code' position='body_start'}

{* аним.загрузки *}
<div id="preloader"><div uk-spinner></div></div>
<div>
<div class="vkhack">

<div class="uk-offcanvas-content">
  <noscript>You need to enable JavaScript to run this app.</noscript>
  {* {include file="$incpath/shop/shop-header.tpl"} *}

  <div class="uk-inline uk-box-shadow-large uk-margin-small-bottom">
    <img src="{$imgpath}/page/bg-main.jpg" style="min-height:120px" alt="cover image">
    <div class="uk-position-cover uk-overlay-default"></div>

    <div class="uk-position-top zindex-2">
      {navigation id=2 tpl='navi-topbar.tpl'}{* navi/ *}
      <hr class="uk-margin-remove" style="border-color:#0000001a">
      {navigation id=1 tpl='shopnav.tpl'}
    </div>

  </div><!--uk-inline-->


  {* ВЫВОДИМ НА СТРАНИЦЕ ПРОДУКТА *}
  {if strpos($smarty.server.REQUEST_URI,"/show-product/") !== false}

    {if !empty($breadcrumb)}
      <nav aria-label="breadcrumb" class="uk-container uk-container-large">
        <ol class="uk-breadcrumb">
          {foreach from=$breadcrumb item=item}
              <li>
                {$item}
              </li>
          {/foreach}
        </ol>
      </nav>
    {/if}

    {$content}

  {* ВЫВОДИМ НА ГЛАВНОЙ *}
  {elseif strpos($smarty.server.REQUEST_URI,"/base/") !== false}
    <div class="uk-container">
    <div class="uk-grid-small uk-margin-small-bottom" uk-grid>

        <div class="uk-width-1-4@m uk-flex-last uk-flex-first@m">
          <div class="zindex-1" uk-sticky="offset:90; bottom:true; media:640">
          {* {include file="$incpath/shop/search_extended.tpl"} *}
            {$ShopInfoPanel}
            {* {include file="$incpath/shop/basket_saved_small.tpl"} *}
            {$curreny_selector}
            {$small_topseller}

            {include file="$incpath/shop/small_shipper.tpl"}
            {include file="$incpath/other/outlinks.tpl"}
          </div>
        </div>

        <div class="uk-width-expand@m" uk-height-viewport="expand:true">

        {if !empty($breadcrumb)}
        <nav aria-label="breadcrumb" class="uk-container">
          <ol class="uk-breadcrumb">
            {foreach from=$breadcrumb item=item}
                <li>
                  {$item}
                </li>
            {/foreach}
          </ol>
        </nav>
        {/if}

        {$content}
        </div>

    </div>
    </div>

  {* ВЫВОДИМ НА СТРАНИЦЕ ПРОДУКТОВ *}
  {elseif strpos($smarty.server.REQUEST_URI,"/show-products/") !== false}
    <div class="uk-container">
    <div class="uk-grid-small uk-margin-small-bottom" uk-grid>

        <div class="uk-width-1-4@m">
          <div class="zindex-1" uk-sticky="offset:90; bottom:true; media:640">
          {* {include file="$incpath/shop/search_extended.tpl"} *}
            {$ShopInfoPanel}
            {* {include file="$incpath/shop/basket_saved_small.tpl"} *}
            {$curreny_selector}
            {$small_topseller}

            {include file="$incpath/shop/small_shipper.tpl"}
            {include file="$incpath/other/outlinks.tpl"}
          </div>
        </div>

        <div class="uk-width-expand@m" uk-height-viewport="expand:true">

        {if !empty($breadcrumb)}
        <nav aria-label="breadcrumb" class="uk-container">
          <ol class="uk-breadcrumb">
            {foreach from=$breadcrumb item=item}
                <li>
                  {$item}
                </li>
            {/foreach}
          </ol>
        </nav>
        {/if}

        {$content}
        </div>

    </div>
    </div>
  {/if}

  {* {include file="$incpath/section/order.tpl"} *}
  {* {include file="$incpath/section/contact-info.tpl"} *}

  {if !empty($ShopInfo)}
      {$ShopInfo}
  {/if}

  {* {include file="$incpath/footer/footer-shop.tpl"} *}

</div><!--offcanvas-->
</div>
</div>
{* {include file="$incpath/other/vk-chat.tpl"} *}

{include file="$incpath/js/jsend-shop.tpl"}

{* {include file="$incpath/other/yandex-metrika.tpl"} *}
{include file="$incpath/other/google.tpl"}


{result type='code' format='code' position='body_end'}
{result type='script' format='file' position='body_end'}
{result type='script' format='code' position='body_end'}
</body>
</html>
