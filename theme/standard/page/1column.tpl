<!doctype html>
<html lang="{$langcode}">
  {include file="$incpath/other/head.tpl"}
  <body>
    {result type='script' format='file' position='body_start'} {* вывод файлов скриптов *}
    {result type='script' format='code' position='body_start'} {* вывод кода скриптов *}
    {result type='code' format='code' position='body_start'}   {* вывод кода *}

    {include file="$incpath/other/header.tpl"}

    <main class="main">
      <div class="container">
        {$content}
      </div>
    </main>

    {include file="$incpath/other/google.tpl"}

    {result type='code' format='code' position='body_end'}   {* вывод кода *}
    {result type='script' format='file' position='body_end'} {* вывод файлов скриптов *}
    {result type='script' format='code' position='body_end'} {* вывод кода скриптов *}
  </body>
</html>
