<!doctype html>
<html lang="{$langcode}" prefix="og: http://ogp.me/ns#">
{include file="$incpath/head/head-home.tpl"}
<body>
{result type='script' format='file' position='body_start'}
{result type='script' format='code' position='body_start'}
{result type='code'   format='code' position='body_start'}

<div id="preloader"><div uk-spinner="ratio: 3"></div></div>

<div class="uk-offcanvas-content">

  <header class="uk-background-fixed uk-background-center-center uk-background-cover" style="background-image: url({$imgpath}/page/cover.jpg)">
    <div class="uk-overlay-cover uk-overlay-primary">
      
      <div class="uk-inline uk-height-large uk-width-1-1">

        <div class="uk-position-top zindex-2">
          {navigation id=1 tpl='navi-home.tpl'}
          <hr class="uk-margin-remove" style="border-color:#0000001a">
        </div>

        <div class="uk-position-center uk-width-1-3@m slogan">
          <h4 class="uk-heading-divider uk-margin-remove" uk-scrollspy="cls: uk-animation-slide-top-small;delay:1000">
          {$settings.Firma|sanitize}</h4>
          <h1 class="uk-margin-remove" uk-scrollspy="cls: uk-animation-slide-bottom-small;delay:1000">{$settings.Seitenname|sanitize}</h1>
        </div>

      </div>

    </div>
  </header>

  {$content}

  {* {include file="$incpath/section/contact-info.tpl"} *}
  {* {include file="$incpath/section/map-yandex.tpl"} *}
  {* {include file="$incpath/footer/footer-main.tpl"} *}
  {$Newsletter}

</div><!--offcanvas-->

{* {include file="$incpath/other/vk-chat.tpl"} *}
{* {include file="$incpath/js/jsend-home.tpl"} *}

{result type='code'   format='code' position='body_end'}
{result type='script' format='file' position='body_end'}
{result type='script' format='code' position='body_end'}
</body>
</html>
