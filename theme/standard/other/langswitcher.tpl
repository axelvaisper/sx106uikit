<div class="flex-line">
  {foreach from=$languages item=lang}
      {if $langcode != $lang}
          <a class="mx-1" href="index.php?lang={$lang}&amp;lredirect={page_link|base64encode}&amp;rand={$smarty.now}">
            <img data-toggle="tooltip" title="{$lang}" src="{$imgpath}/flags/{$lang}.png" alt="" />
          </a>
      {/if}
  {/foreach}
</div>
