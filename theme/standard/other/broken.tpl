{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function () {
    $('#brokenlink').validate({
        rules: {
            'name': { required: true },
            'email': { required: true, email: true }
        },
        messages: { },
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                target: '#broken-target',
                url: '{$brokenlink}',
                success: function() {
                    document.getElementById('broken-form').style.display = 'none';
                },
                clearForm: false,
                resetForm: true
            });
        }
    });
});
//-->
</script>

<div id="broken-click" style="display: none">
  <div class="wrapper">
  <div class="h3 title">{#Links_ErrorSendBroken#}</div>
    <div class="box-content">
      <div id="broken-target"></div>
      <form id="brokenlink" method="post" action="">
        <div id="broken-form">
          <div class="row">
            <div class="col-md-3 col-form-label">
              {#Title#}
            </div>
            <div class="col-md-9 form-group font-weight-bold">
              {$link_res->Name|sanitize}
            </div>
          </div>

          <div class="row">
            <div class="col-md-3 col-form-label">
              {#Links_ErrorM#}
            </div>
            <div class="col-md-9 form-group">
              <div class="form-check">
                <input id="b-reason" type="radio" name="BrokenReason" value="Links_Broken_dnserror" checked="checked" />
                <label for="b-reason">{#Links_Broken_dnserror#}</label>
              </div>
              <div class="form-check">
                <input id="b-connect" type="radio" name="BrokenReason" value="Links_Broken_noconnection" />
                <label for="b-connect">{#Links_Broken_noconnection#}</label>
              </div>
              <div class="form-check">
                <input id="b-auth" type="radio" name="BrokenReason" value="Links_Broken_auth" />
                <label for="b-auth">{#Links_Broken_auth#}</label>
              </div>
              <div class="form-check">
                <input id="b-notfound" type="radio" name="BrokenReason" value="Links_Broken_notfound" />
                <label for="b-notfound">{#Links_Broken_notfound#}</label>
              </div>
              <div class="form-check">
                <input id="b-server" type="radio" name="BrokenReason" value="Links_Broken_servererror" />
                <label for="b-server">{#Links_Broken_servererror#}</label>
              </div>
              <div class="form-check">
                <input id="b-other" type="radio" name="BrokenReason" value="ActionOther" />
                <label for="b-other">{#ActionOther#}</label>
              </div>
            </div>
          </div>

          {if !$loggedin}
              <div class="row form-group">
                <div class="col-md-3">
                  <label class="col-form-label" for="b-name">{#Contact_myName#}</label>
                </div>
                <div class="col-md-9">
                  <input class="form-control" id="b-name" type="text" name="name" value="" required="required" />
                </div>
              </div>
              <div class="row form-group">
                <div class="col-md-3">
                  <label class="col-form-label" for="b-mail">{#SendEmail_Email#}</label>
                </div>
                <div class="col-md-9">
                  <input class="form-control" id="b-mail" type="email" name="email" value="" required="required" />
                </div>
              </div>
          {else}
              <input name="name" type="hidden" value="{$smarty.session.user_name}" id="c_a" />
              <input name="email" type="hidden" value="{$smarty.session.login_email}" id="c_e" />
          {/if}

          <div class="text-center">
            <input class="btn btn-primary btn-block-sm" type="submit" value="{#Links_ErrorSendBroken#}" />
          </div>
          <input type="hidden" name="dpage" value="{page_link|base64_encode}" />
        </div>
      </form>
    </div>
  </div>
</div>
