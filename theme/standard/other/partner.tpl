{if $small_partners}
<script>
<!-- //
$(function() {
    $('.partner-click').on('click', function () {
        var options = {
            target: '#plick',
            url: 'index.php?p=partners&action=updatehitcount&id=' + $(this).data('id'),
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
});
//-->
</script>

<section id="partner">
<div class="uk-container">
<div class="uk-child-width-expand@m" uk-grid uk-scrollspy="target: > div; cls:uk-animation-slide-right; delay: 500">
  <h2 class="uk-text-center">{* {#Partners#} *} Нам доверяют</h2>
    <div id="partner-click"></div>
    {foreach from=$small_partners item=sp}
        <div class="uk-text-center">
          <a class="partner-click" data-id="{$sp->Id}" {if $sp->Nofollow == 1}rel="nofollow" {/if}title="{$sp->PartnerName|sanitize}" href="{$sp->PartnerUrl}" target="_blank">
            {if $sp->Bild}
                <img src="{$sp->Bild}" alt="{$sp->PartnerName|sanitize}" uk-tooltip="{$sp->PartnerName|sanitize}"/>
            {else}
                <span >{$sp->PartnerName|sanitize}</span>
            {/if}
          </a>
        </div>
    {/foreach}
</div>
</div>
</section>
{/if}
