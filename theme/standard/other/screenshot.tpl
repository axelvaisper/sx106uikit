<script>
<!-- //
$(function() {
    $('.screenshot').colorbox({
        photo: true,
        transition: "elastic",
        maxHeight: "98%",
        maxWidth: "98%",
        slideshow: true,
        slideshowAuto: false,
        slideshowSpeed: 2500,
        current: "{#GlobalImage#} {ldelim}current{rdelim} {#PageNavi_From#} {ldelim}total{rdelim}",
        slideshowStart: "{#GlobalStart#}",
        slideshowStop: "{#GlobalStop#}",
        previous: "{#GlobalBack#}",
        next: "{#GlobalNext#}",
        close: "{#GlobalGlose#}"
    });
});
//-->
</script>

<div class="box-content">
  <h4>%%title%%</h4>
  <div class="clearfix">
    <a class="screenshot" rel="screenshot" href="uploads/screenshots/%%id%%">
      <img class="img-left img-thumbnail" src="%%src%%" alt="%%title%%" />
    </a>
    %%text%%
  </div>
</div>
