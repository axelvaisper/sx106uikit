<footer class="footer bg-primary">
  <div class="footer-top">

    <div class="container">
      <div class="row">

        <div class="col-md-6 col-sm-6">
            <h4><a href="/">
              <img class="img-fluid" src="{$imgpath}/page/logo.png" alt="" /> {$settings.Seitenname|sanitize}
            </a></h4>
          <div class="spacer">{$insert.footer_text}</div>
        </div>

        <div class="col-md-3 col-sm-6">
          {navigation id=3 tpl='footer.tpl'}
        </div>

        <div class="col-md-3 col-sm-6">
          <h4 class="footer-title">{#Imprint#}</h4>
          <ul class="footer-list-menu">
            {if !empty({$settings.Firma})}
                <li class="footer-list-item w-100">
                  {$settings.Firma}
                </li>
            {/if}
            {if !empty({$settings.Zip})}
                <li class="footer-list-item w-100" data-toggle="tooltip" title="{#Zip#}">
                  {$settings.Zip}
                </li>
            {/if}
            {if !empty({$settings.Strasse})}
                <li class="footer-list-item w-100" style="color:#fff" data-toggle="tooltip" title="{#Strasse#}">
                  <a href="/index.php?p=imprint">{$settings.Strasse} </a>
                </li>
            {/if}
            {if !empty({$settings.Stadt})}
                <li class="footer-list-item w-100" data-toggle="tooltip" title="{#Town#}">
                  {$settings.Stadt}
                </li>
            {/if}
            {if !empty({$settings.Telefon})}
                <li class="footer-list-item w-100" data-toggle="tooltip" title="{#Phone#}">
                  <a href="tel:{$settings.Telefon|regex_replace:'/[^\d\+]/':''}">{$settings.Telefon}</a>
                </li>
            {/if}
            {if !empty({$settings.Fax})}
                <li class="footer-list-item w-100" data-toggle="tooltip" title="{#Fax#}">
                  {$settings.Fax}
                </li>
            {/if}
            {if !empty({$settings.Mail_Absender})}
                <li class="footer-list-item w-100" data-toggle="tooltip" title="{#Email#}">
                  <a href="mailto:{$settings.Mail_Absender}">{$settings.Mail_Absender}</a>
                </li>
            {/if}
          </ul>
        </div>

      </div>

    </div>
  </div>

  <div class="footer-bottom">
    <div class="container">
      <div class="col-md-12">
        <div class="copyright">{#copyright_text#}</div>
      </div>
    </div>
    <div class="version">
      {version}
    </div>
  </div>

</footer>
