<div class="h3 title">{#UserActivity#}</div>
<div class="box-content">
  {if !empty($posts)}
      {foreach from=$posts item=p}
          <div class="d-flex justify-content-between">
            {if !empty($p.forum_title)}
                <div class="mr-2 text-truncate">
                  {#GlobalMessage#} / <a href="index.php?p=showforums">{#Forums_Title#}</a> / <a href="{$p.link}">{$p.forum_title|sanitize}</a>
                </div>
            {else}
                <div class="text-truncate mr-2">
                  {#Global_Comment#} {if !empty($p.sec_link)}/ <a href="{$p.sec_link}">{$p.sec_name|sanitize}</a> {/if}/ <a href="{$p.link}">{$p.sec_title|sanitize}</a>
                </div>
            {/if}
            <div class="text-nowrap">
              <i class="icon-calendar"></i>{$p.date|date_format:$lang.DateFormat}
            </div>
          </div>
      {/foreach}
  {else}
      {#NoAktivity#}
  {/if}
</div>
