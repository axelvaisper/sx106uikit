{if $user.Gaestebuch == 1}
    <div class="wrapper">
      <div class="h3 title clearfix" id="eintraege">
        {#Profile_GuestbookUser#}
        <div class="float-right">
          <a href="#eintrag_neu">
            <i class="icon-chat" data-toggle="tooltip" title="{#Profile_Guestbook_EntryNow1#} {$user.Benutzername}"></i>
          </a>
          {if $smarty.session.benutzer_id == $smarty.request.id && $eintrag}
              <a onclick="return confirm('{#Profile_Guestbook_DeleteAll#}');" href="index.php?p=user&amp;action=guestbook&amp;do=delete_all&amp;id={$smarty.request.id}&amp;area={$area}&amp;page={$smarty.request.page}">
                <i class="icon-cancel" data-toggle="tooltip" title="{#Profile_Guestbook_DeleteAll#}"></i>
              </a>
          {/if}
        </div>
      </div>

      {if $eintrag}
          {if $smarty.session.benutzer_id == $smarty.request.id}
<script>
<!-- //
function edit_entry(ID) {
{foreach from=$eintrag item=e}
    document.getElementById('eintrag_edit_' + {$e.Id}).style.display = 'none';
    document.getElementById('eintrag_' + {$e.Id}).style.display = '';
{/foreach}
    document.getElementById('eintrag_' + ID).style.display = 'none';
    document.getElementById('eintrag_edit_' + ID).style.display = '';
}
//-->
</script>
      {/if}
      {foreach from=$eintrag item=e}
          <div class="box-content{if $e.Aktiv != 1} border-error{/if}" id="entry{$e.Id}">
            <div class="h5">{$e.Titel|sanitize|default:'-'}</div>

            <div class="listing-head flex-line">
              <div class="mr-3">
                <span class="font-weight-bold">{#Date#}:</span> {$e.Datum|date_format: $lang.DateFormatExtended}
              </div>
              <div class="mr-3">
                <span class="font-weight-bold">{#GlobalAutor#}:</span> {$e.Autor|sanitize}
              </div>
              {if $e.Autor_Herkunft}
                  <div class="mr-3">
                    <span class="font-weight-bold">{#Town#}:</span> {$e.Autor_Herkunft|sanitize}
                  </div>
              {/if}
              {if $e.Autor_Web}
                  <a rel="nofollow" target="_blank" href="{$e.Autor_Web|sanitize}">
                    <i class="icon-link" data-toggle="tooltip" title="{$e.Autor_Web|sanitize}"></i>
                  </a>
              {/if}
            </div>

            <div id="eintrag_{$e.Id}">
              <!--START_NO_REWRITE-->
              {$e.Eintrag}
              <!--END_NO_REWRITE-->
            </div>

            {if $smarty.session.benutzer_id == $smarty.request.id}
                <div id="eintrag_edit_{$e.Id}" style="display: none">
                  <form method="post" action="index.php?p=user&amp;action=guestbook">
                    <div class="row form-group">
                      <div class="col-md-3 col-sm-12">
                        <label class="col-form-label" for="gb-autor-{$e.Id}">{#GlobalName#}</label>
                      </div>
                      <div class="col-md-9 col-sm-12">
                        <input class="form-control" id="gb-autor-{$e.Id}" name="E_Autor" type="text" value="{$e.Autor|sanitize}" />
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col-md-3 col-sm-12">
                        <label class="col-form-label" for="gb-site-{$e.Id}">{#Web#}</label>
                      </div>
                      <div class="col-md-9 col-sm-12">
                        <input class="form-control" id="gb-site-{$e.Id}" name="E_Webseite" type="text" value="{$e.Autor_Web|sanitize}" />
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col-md-3 col-sm-12">
                        <label class="col-form-label" for="gb-town-{$e.Id}">{#Town#}</label>
                      </div>
                      <div class="col-md-9 col-sm-12">
                        <input class="form-control" id="gb-town-{$e.Id}" name="E_Herkunft" type="text" value="{$e.Autor_Herkunft|sanitize}" />
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col-md-3 col-sm-12">
                        <label class="col-form-label" for="gb-title-{$e.Id}">{#Title#}</label>
                      </div>
                      <div class="col-md-9 col-sm-12">
                        <input class="form-control" id="gb-title-{$e.Id}" name="E_Titel" type="text" value="{$e.Titel|sanitize}" />
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col-md-3 col-sm-12">
                        <label class="col-form-label" for="gb-text-{$e.Id}">{#GlobalMessage#}</label>
                      </div>
                      <div class="col-md-9 col-sm-12">
                        <textarea class="form-control" id="gb-text-{$e.Id}" name="E_Eintrag" rows="3">{$e.Eintrag_Raw|sanitize}</textarea>
                      </div>
                    </div>
                    <input class="btn btn-primary btn-block-sm" type="submit" value="{#Save#}" />
                    <input type="hidden" name="id" value="{$smarty.request.id}" />
                    <input type="hidden" name="do" value="edit" />
                    <input type="hidden" name="gb_entry" value="{$e.Id}" />
                    <input type="hidden" name="page" value="{$smarty.request.page|default:1}" />
                  </form>
                </div>

                <div class="text-right">
                  {if $e.Aktiv != 1}
                      <a href="index.php?p=user&amp;action=guestbook&amp;do=set_active&amp;id={$smarty.request.id}&amp;page={$smarty.request.page}&amp;gb_entry={$e.Id}&amp;area={$area}">
                        <i class="icon-ok" data-toggle="tooltip" title="{#Profile_Guestbook_SetActive#}"></i>
                      </a>
                  {/if}
                  <a href="javascript:void(0);" onclick="edit_entry('{$e.Id}');return false;">
                      <i class="icon-pencil" data-toggle="tooltip" title="{#GlobalEdit#}"></i>
                    </a>
                    <a onclick="return confirm('{#Profile_Guestbook_DeleteC#}');" href="index.php?p=user&amp;action=guestbook&amp;do=delete&amp;d=true&amp;id={$smarty.request.id}&amp;page={$smarty.request.page}&amp;gb_entry={$e.Id}&amp;area={$area}">
                      <i class="icon-cancel" data-toggle="tooltip" title="{#Delete#}"></i>
                    </a>
                </div>
            {/if}
          </div>
      {/foreach}
      {if !empty($pages)}
          <div class="wrapper">
            {$pages}
          </div>
      {/if}
    {else}
        <div class="box-content">{#NotMessages#}</div>
    {/if}
</div>

<div class="wrapper" id="eintrag_neu">
  {if isset($KeineGaeste) && $KeineGaeste == 1}
      <div class="box-content">{#Profile_Guestbook_NoGuests#}</div>
  {else}

{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
{include file="$incpath/other/jsform.tpl"}
$(function () {
{if !empty($error)}
    location.href = '#error';
{/if}
    $('#fe').validate({
        rules: {
            Autor: { required: true },
            Titel: { required: true },
            text: { required: true, minlength: 10, maxlength: {$user.Gaestebuch_Zeichen} }
        },
        submitHandler: function() {
            document.forms['f'].submit();
        }
    });
});
//-->
</script>

        <div class="h3 title">{#Profile_Guestbook_EntryNow1#} {$user.Benutzername}</div>
        <div class="box-content">
          {if !empty($error)}
              <div class="box-error" id="error">
                {foreach from=$error item=err}
                    <div>{$err}</div>
                {/foreach}
              </div>
          {/if}
          <form method="post" action="index.php?p=user&amp;area={$area}" name="f" id="fe">
            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label" for="gb-autor">{#Contact_myName#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                <input class="form-control" id="gb-autor" name="Autor" type="text" value="{$smarty.post.Autor|sanitize}" />
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label" for="gb-site">{#Web#}{#GlobalOption#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                <input class="form-control" id="gb-site" name="Webseite" type="text" value="{$smarty.post.Webseite|sanitize}" />
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label" for="gb-town">{#Town#}{#GlobalOption#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                <input class="form-control" id="gb-town" name="Herkunft" type="text" value="{$smarty.post.Herkunft|sanitize}" />
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label" for="gb-title">{#Title#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                <input class="form-control" id="gb-title" name="Titel" type="text" value="{$smarty.post.Titel|sanitize}" />
              </div>
            </div>
            {if $settings.KommentarFormat == 1}
                <div class="row">
                  <div class="col-md-12">
                    {if $settings.SysCode_Smilies == 1}
                        {$listemos}
                    {/if}
                    {include file="$incpath/comments/format.tpl"}
                  </div>
                </div>
            {/if}
            <div class="row form-group">
              <div class="col-md-12">
                <label class="sr-only" for="msgform">{#GlobalMessage#}</label>
                <textarea class="form-control" id="msgform" name="text" rows="5">{$smarty.post.text|escape:html}</textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                {include file="$incpath/other/captcha.tpl"}
              </div>
            </div>
            <input class="btn btn-primary btn-block-sm" type="submit" onclick="closeCodes();" value="{#ButtonSend#}" />
            <input type="hidden" name="Redir" value="{page_link}" />
            <input type="hidden" name="Eintrag" value="1" />
            <input type="hidden" name="id" value="{$smarty.request.id}" />
          </form>
        </div>
      {/if}
  </div>
{/if}
