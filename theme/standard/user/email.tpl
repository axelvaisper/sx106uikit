<h1 class="title text-truncate">{$sname}</h1>
{if isset($not_logged) && $not_logged == 1}
    <div class="wrapper">
      {#Profile_Email_Error#}
    </div>
    <div class="text-center">
      <input class="btn btn-primary" type="button" onclick="closeWindow();" value="{#WinClose#}" />
    </div>
{else}

    {if (isset($smarty.request.mailsend) && $smarty.request.mailsend == 1) && !$error}
        <div class="text-center">
          <div class="wrapper">
            {#SendEmail_Ok#}
          </div>
          <input class="btn btn-primary" type="button" onclick="closeWindow();" value="{#WinClose#}" />
        </div>
    {else}

{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function() {
    $('#cf').validate({
        rules: {
            {if !$loggedin}
            email: { required: true, email: true },
            {/if}
            subject: { required: true },
            body: { required: true, minlength: 10 }
        },
        submitHandler: function() {
            document.forms['fc'].submit();
        },
        success: function(label) {
            label.html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;').addClass('checked');
        }
    });
});
//-->
</script>

        {if !empty($error)}
            <div class="box-error">
              {foreach from=$error item=err}
                  <div>{$err}</div>
              {/foreach}
            </div>
        {/if}

        <form name="fc" id="cf" action="index.php" method="post">
          <div class="form-group">
            <label for="send-mail">{#SendEmail_Email#}</label>
            <input class="form-control" id="send-mail" name="email" type="email" value="{$smarty.request.email|default:$smarty.session.login_email|escape:html}" maxlength="35" />
          </div>
          <div class="form-group">
            <label for="send-title">{#SendEmail_Title#}</label>
            <input class="form-control" id="send-title" name="subject" type="text" id="subject" value="{$smarty.request.subject|escape:html}" />
          </div>
          <div class="form-group">
            <label for="send-body">{#GlobalMessage#}</label>
            <textarea class="form-control" id="send-body" name="body" rows="5">{$smarty.request.body|escape:html}</textarea>
          </div>

          {include file="$incpath/other/captcha.tpl"}
          <div class="text-center">
            <input class="btn btn-primary" value="{#SendEmail_Send#}" type="submit">
            <input class="btn btn-secondary" onclick="closeWindow();" value="{#WinClose#}" type="button">
          </div>

          <input name="uid" type="hidden" value="{$smarty.request.uid}" />
          <input name="p" type="hidden" value="misc" />
          <input name="do" type="hidden" value="email" />
          <input name="mailsend" type="hidden" value="1" />
        </form>
    {/if}
{/if}
