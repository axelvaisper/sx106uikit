<div class="d-flex flex-column">
  <div>
    {$welcome}, {$smarty.session.user_name}!
  </div>
  {if get_active('shop')}
      <div class="wrapper">
        {#LoginExternCustomerNr#}: {$smarty.session.benutzer_id}
      </div>
  {/if}
  {if get_active('pn')}
      <a href="index.php?p=pn">
        {#PN_inbox#} {newpn}
      </a>
  {/if}

  {if get_active('shop')}
      <a href="index.php?p=shop&amp;action=myorders">
        {#Shop_go_myorders#}
      </a>
      <a href="index.php?p=shop&amp;action=mydownloads">
        {#LoginExternVd#}
      </a>
      <a href="index.php?p=shop&amp;action=mylist">
        {#Shop_mylist#}
      </a>
      <a class="colorbox" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1">
        {#Shop_mergeListsMy#}
      </a>
  {/if}
  {if get_active('calendar')}
      <a href="index.php?p=calendar&amp;month={$smarty.now|date_format:'m'}&amp;year={$smarty.now|date_format:'Y'}&amp;area={$area}&amp;show=private">
        {#UserCalendar#}
      </a>
  {/if}
  <a href="index.php?p=user&amp;id={$smarty.session.benutzer_id}&amp;area={$area}">
    {#LoginExternVp#}
  </a>
  <a href="index.php?p=useraction&amp;action=profile">
    {#LoginExternPc#}
  </a>
  <div class="wrapper">
    <a href="index.php?p=useraction&amp;action=changepass">
      {#LoginExternCp#}
    </a>
  </div>

  {if permission('adminpanel')}
      <a href="javascript:void(0);" onclick="openWindow('{$baseurl}/admin', 'admin', '', '', 1);">
        {#AdminLink#}
      </a>
  {/if}
  {if permission('deleteaccount') && $smarty.session.benutzer_id != 1}
      <a href="index.php?p=useraction&amp;action=deleteaccount">
        {#AccountDel#}
      </a>
  {/if}

  <form method="post" name="logout_form" action="index.php">
    <input type="hidden" name="p" value="userlogin" />
    <input type="hidden" name="action" value="logout" />
    <input type="hidden" name="area" value="{$area}" />
    <input type="hidden" name="backurl" value="{'index.php'|base64encode}" />
    <a onclick="return confirm('{#Confirm_Logout#}');" href="javascript: document.forms['logout_form'].submit();">
      {#Logout#}
    </a>
  </form>
</div>
