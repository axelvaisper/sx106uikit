<h1 class="title text-truncate">{$title_html}</h1>
{if $icq}
    <div class="wrapper h4 text-center">
      {$icq|sanitize}
    </div>
    <div class="wrapper d-flex flex-column">
      <a rel="nofollow" target="_blank" href="http://people.icq.com/people/cmd.php?uin={$icq}&amp;action=message">
        {#Arrow#}{#Profile_ICQContactWith#}
      </a>
      <a rel="nofollow" target="_blank" href="http://www.icq.com/people/webmsg.php?to={$icq}">
        {#Arrow#}{#Profile_ICQContactWithout#}
      </a>
      <a rel="nofollow" target="_blank" href="http://people.icq.com/people/cmd.php?uin={$icq}&amp;action=add">
        {#Arrow#}{#Profile_ICQContactAdd#}
      </a>
      <a rel="nofollow" target="_blank" href="http://people.icq.com/people/about_me.php?uin={$icq}">
        {#Arrow#}{#Profile_ICQSHowProfile#}
      </a>
    </div>
    <div class="wrapper">
      {#Profile_ICQInf#}
    </div>
{/if}
<div class="text-center">
  <input class="btn btn-primary" type="button" onclick="closeWindow();" value="{#WinClose#}" />
</div>
