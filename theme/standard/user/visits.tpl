<div class="h3 title">{#UserVisits#}</div>
<div class="box-content">
  {if !empty($items)}
      {assign var='lcc' value=0}
      {foreach from=$items item=item name=avatar}
          <div class="col d-flex justify-content-center align-self-end">
            <div class="text-center">
              <div>
                <a title="{$item.Name}" href="index.php?p=user&amp;id={$item.Id}&amp;area={$area}">{$item.Avatar}</a>
              </div>
              <div class="small">
                <a href="index.php?p=user&amp;id={$item.Id}&amp;area={$area}">{$item.Name}</a>
              </div>
            </div>
          </div>
          {assign var='lcc' value=$lcc+1}
          {if $lcc % $avatar_line == 0 && !$smarty.foreach.avatar.last}
          </div>
          <div class="row">
          {/if}
      {/foreach}
  </div>
{else}
    {#NoAktivity#}
{/if}
</div>
