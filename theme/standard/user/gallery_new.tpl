{script file="$jspath/jupload.js" position='head'}
<script>
<!-- //
function fileUpload(divid) {
    $(document).ajaxStart(function() {
        $('UpInf_' + divid).hide();
        $('#loading_' + divid).show();
        $('#buttonUpload_' + divid).val('{#Global_Wait#}').prop('disabled', true);
    }).ajaxComplete(function() {
        $('#loading_' + divid).hide();
        $('#buttonUpload_' + divid).val('{#UploadButton#}').prop('disabled', false);
    });
    $.ajaxFileUpload( {
	url: 'index.php?p=user&action=upload&divid=' + divid,
	secureuri: false,
	fileElementId: 'fileToUpload_' + divid,
	dataType: 'json',
	success: function (data) {
	    if(typeof(data.result) !== 'undefined') {
                document.getElementById('UpInf_' + divid).innerHTML = data.result;
                if(data.filename !== '') {
                    document.getElementById('newFile_' + divid).value = data.filename;
                    var nextid = eval(divid + '+' + 1);
                    $('#tab_' + nextid).show();
                }
	    }
	},
	error: function (data, status, e) {
	    document.getElementById('UpInf_' + divid).innerHTML = e;
	}
    });
    return false;
}
//-->
</script>

<div class="h1 title">{#NewAlbum#}</div>

{if !empty($error)}
    <div class="box-error">
      {foreach from=$error item=err}
          <div>{$err}</div>
      {/foreach}
    </div>
    <div class="text-center">
      <button class="btn btn-secondary" onclick="closeWindow();">{#WinClose#}</button>
    </div>
{else}
    <form action="" method="post" enctype="multipart/form-data">
      <div class="row wrapper">
        <div class="col-md-2 col-sm-12">
          <label class="col-form-label" for="new-name">{#AlbumTitle#}</label>
        </div>

        <div class="col-md col-sm-12">
          <input class="form-control" id="new-name" name="title" type="text" value="{$smarty.post.title|escape:html}" />
        </div>
      </div>

      {section name='i' start=0 loop=$loop step=1}
          <div id="tab_{$smarty.section.i.index}" {if $smarty.section.i.index != 1 && $smarty.section.i.index != $next && (empty($title[$smarty.section.i.index]) || $file[$smarty.section.i.index] == '')}style="display:none;"{/if}>
            <div class="row align-items-center">
              <div class="col-md-2 col-sm-12 form-group">
                <label class="col-form-label" for="new-image{$smarty.section.i.index}">{#GlobalTitle#}</label>
              </div>
              <div class="col-md-auto col-sm-12 form-group">
                <input class="form-control" id="new-image{$smarty.section.i.index}" type="text" name="feld_title[{$smarty.section.i.index}]" value="{$title[$smarty.section.i.index]}" />
              </div>

              <div class="col-md col-sm-12 form-group">
                <div id="UpInf_{$smarty.section.i.index}">{$pic[$smarty.section.i.index]}</div>
                <div id="loading_{$smarty.section.i.index}" style="display:none;">
                  <img src="{$imgpath_page}ajaxbar.gif" alt="" />
                </div>
                <div class="input-group">
                  <input class="form-control" id="fileToUpload_{$smarty.section.i.index}" type="file" name="fileToUpload_{$smarty.section.i.index}" />
                  <span class="input-group-append">
                    <input class="btn btn-primary" type="button" id="buttonUpload_{$smarty.section.i.index}" onclick="fileUpload('{$smarty.section.i.index}');" value="{#UploadButton#}" />
                  </span>
                </div>
                <input type="hidden" name="feld_file[{$smarty.section.i.index}]" id="newFile_{$smarty.section.i.index}" value="{$file[$smarty.section.i.index]}" />
              </div>
            </div>
          </div>
      {/section}

      <div class="text-center">
        <input class="btn btn-primary" type="submit" value="{#Save#}" />
        <input class="btn btn-secondary" type="button" onclick="closeWindow();" value="{#WinClose#}" />
      </div>

      <input type="hidden" name="save" value="1" />
    </form>
{/if}
