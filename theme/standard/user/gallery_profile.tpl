{if $smarty.request.id == $smarty.session.benutzer_id}

    <div class="box-content">
      <div class="wrapper">
        {#GalleryInfo#}
      </div>

      <div class="row text-center">
        <div class="col-md col-sm-12 spacer">
          <a class="colorbox-sm btn btn-primary btn-block-sm" href="index.php?p=user&amp;action=gal&amp;do=new&amp;area={$area}">
            {#NewAlbum#}
          </a>
        </div>
        <div class="col-md col-sm-12 spacer">
          <a class="colorbox-sm btn btn-primary btn-block-sm" href="index.php?p=user&amp;action=gal&amp;do=edit&amp;area={$area}">
            {#EditAlbum#}
          </a>
        </div>
        <div class="col-md col-sm-12 spacer">
          <a class="colorbox-sm btn btn-primary btn-block-sm" href="index.php?p=user&amp;action=gal&amp;do=del&amp;area={$area}">
            {#DelAlbum#}
          </a>
        </div>
      </div>
    </div>
{/if}
