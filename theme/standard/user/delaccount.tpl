<h1 class="title">{#AccountDel#}</h1>
<div class="box-content">
  {if $ugroup == 1}
      {#AccountDelAdmin#}
  {else}
      {if isset($CurrPassWrong) && $CurrPassWrong == 1}
          <div class="box-error">{#AccountDelPassWrong#}</div>
      {/if}
      <div class="wrapper">{#AccountDelInf#}</div>

      <form method="post" action="index.php?p=useraction&amp;action=deleteaccount">
        <div class="row form-group">
          <div class="col-md-4 col-sm-12">
            <label class="col-form-label" for="form-reason">{#AccountDelReason#}</label>
          </div>
          <div class="col-md-8 col-sm-12">
            <select class="form-control" name="DelReason" id="form-reason">
              {foreach from=$DelReasons item=reason}
                  <option value="{$reason}">{$reason}</option>
              {/foreach}
            </select>
          </div>
        </div>

        <div class="row form-group">
          <div class="col-md-4 col-sm-12">
            <label class="col-form-label" for="form-message">{#AccountDelMessage#}</label>
          </div>
          <div class="col-md-8 col-sm-12">
            <textarea class="form-control" id="form-message" name="ReasonMessage" rows="3">{if isset($smarty.post.ReasonMessage)}{$smarty.post.ReasonMessage|sanitize}{/if}</textarea>
          </div>
        </div>

        <div class="row form-group">
          <div class="col-md-4 col-sm-12">
            <label class="col-form-label" for="form-pass">{#AccountDelPass#}</label>
          </div>
          <div class="col-md-8 col-sm-12">
            <input class="form-control" id="form-pass" name="PassCurr" type="text" />
          </div>
        </div>

        <div class="text-center">
          <input class="btn btn-primary btn-block-sm" type="submit" value="{#AccountDelFinalButton#}" />
        </div>

        <input type="hidden" name="subaction" value="delfinal" />
      </form>
  {/if}
</div>
