<div class="h3 title" id="gallery">{#Gallery_Name#}</div>
<div class="box-content">

  {if !empty($gallery)}
      <div class="wrapper d-flex">
        <div class="mr-3">{#Albums#} - {$gal}</div>
        <div class="mr-3">{#Images#} - {$num_img}</div>
      </div>

      <div class="row">
        {assign var='lcc' value=0}
        {foreach from=$gallery item=g name=gall}
            {if !empty($g.Datei)}
                <div class="col d-flex justify-content-center">
                  <div class="text-center">
                    <div>
                      <a class="colorbox-sm" href="index.php?p=user&amp;action=gallery&amp;do=images&amp;id={$g.Id}&amp;area={$area}&amp;image={$g.Bild}" onclick="return galBrowser(this.href);">
                        <img class="img-fluid height-4" src="{$g.Image}" alt="{$g.Name}" />
                      </a>
                    </div>
                    <div class="small">
                      <a class="colorbox-sm" href="index.php?p=user&amp;action=gallery&amp;do=images&amp;id={$g.Id}&amp;area={$area}&amp;image={$g.Bild}" onclick="return galBrowser(this.href);">
                        {$g.Name}
                      </a>
                    </div>
                  </div>
                </div>
                {assign var='lcc' value=$lcc+1}
                {if $lcc % $NLines == 0 && !$smarty.foreach.gall.last}
                </div>
                <div class="row">
                {/if}
            {/if}
        {/foreach}
      </div>
  {else}
      {#NoGallery#}
  {/if}
</div>
