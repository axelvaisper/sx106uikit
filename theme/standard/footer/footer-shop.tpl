<footer>
  <div class="uk-container uk-padding-remove">
    <div class="uk-child-width-1-3@s uk-grid-collapse" uk-grid>


      <div itemscope itemtype="http://schema.org/LocalBusiness">

        <a href="/" class="uk-logo uk-flex uk-light">
          <img itemprop="image" src="{$imgpath}/page/logo.svg" alt="" width="55" height="55">
          <div itemprop="name" class="uk-margin-small-left uk-dark uk-margin-small-bottom">
            <small>{$settings.Firma|sanitize}</small>
            <h6 class="uk-margin-remove uk-text-bold uk-text-uppercase">
            {$settings.Seitenname|sanitize}</h6>
          </div>
        </a>

        <ul class="footer-list-menu">
          {if !empty({$settings.Zip})}
              <li>
                {$settings.Zip}
              </li>
          {/if}
          {if !empty({$settings.Strasse})}
              <li style="color:#fff">
                <a itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" href="/index.php?p=imprint">{$settings.Strasse} </a>
              </li>
          {/if}
          {if !empty({$settings.Stadt})}
              <li>
                {$settings.Stadt}
              </li>
          {/if}
          {if !empty({$settings.Telefon})}
              <li>
                <a href="tel:{$settings.Telefon|regex_replace:'/[^\d\+]/':''}">
                  <span itemprop="telephone">{$settings.Telefon}</span></a>
              </li>
          {/if}
          {if !empty({$settings.Fax})}
              <li>
                <a href="https://vk.com/public{$settings.Fax}">
                https://vk.com/public{$settings.Fax}</a>
              </li>
          {/if}
          {if !empty({$settings.Mail_Absender})}
              <li >
                <a href="mailto:{$settings.Mail_Absender}" rel="nofollow">
                  <span itemprop="email">{$settings.Mail_Absender}</span></a>
              </li>
          {/if}
        </ul>

      </div>


      <div>
        {navigation id=3 tpl='navi-footer-shop.tpl'}
      </div>


       <div >
        {* <div id="feedback_vk"></div>
        <script src="https://feedback.kupiapp.ru/widget/widget.js" type="text/javascript"></script>
        <script type="text/javascript">document.addEventListener("DOMContentLoaded", feedback_vk.init({ id:'feedback_vk', gid:{$settings.Fax} }));</script> *}

        <div id="vk_poll"></div>
        <script type="text/javascript">
        VK.Widgets.Poll("vk_poll", {}, "297010422_cc6da8fdc8bfbc028a");
        </script>
      </div>


    </div>

  </div>

  <hr>

  <div class="uk-container uk-text-center">
      {version} {#copyright_text#}<br>
      <a href="https://vk.com/he6oru">Дизайн студия He6oru</a>
  </div>

</footer>