<footer class="uk-padding-small">
  <div class="uk-container uk-padding-remove">
    <div class="uk-child-width-1-3@s uk-grid-collapse" uk-grid>


      <div itemscope itemtype="http://schema.org/LocalBusiness">

        <a href="/" class="uk-logo uk-flex">
          <img itemprop="image" src="{$imgpath}/page/logo.svg" alt="" width="55" height="55">
          <div itemprop="name" class="uk-margin-small-left uk-margin-small-bottom">
            <small>{$settings.Firma|sanitize}</small>
            <h6 class="uk-margin-remove uk-text-bold uk-text-uppercase">
            {$settings.Seitenname|sanitize}</h6>
          </div>
        </a>

        <ul class="footer-list-menu">
          {if !empty({$settings.Zip})}
              <li>
                {$settings.Zip|sanitize}
              </li>
          {/if}
          {if !empty({$settings.Strasse})}
              <li>
                <a itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" href="/index.php?p=imprint">{$settings.Strasse|sanitize} </a>
              </li>
          {/if}
          {if !empty({$settings.Stadt})}
              <li>
                {$settings.Stadt|sanitize}
              </li>
          {/if}
          {if !empty({$settings.Telefon})}
              <li>
                <a href="tel:{$settings.Telefon|regex_replace:'/[^\d\+]/':''}">
                  <span itemprop="telephone">{$settings.Telefon|sanitize}</span></a>
              </li>
          {/if}
          {if !empty({$settings.Fax})}
              <li>
                <a href="https://vk.com/public{$settings.Fax}">
                https://vk.com/public{$settings.Buh|sanitize}</a>
              </li>
          {/if}
          {if !empty({$settings.Mail_Absender})}
              <li >
                <a href="mailto:{$settings.Mail_Absender}" rel="nofollow">
                  <span itemprop="email">{$settings.Mail_Absender|sanitize}</span></a>
              </li>
          {/if}
        </ul>

      </div>


      <div>
        {navigation id=3 tpl='navi-footer-home.tpl'}
      </div>


      <div >
        {* vk группа *}
        <div id="vk_groups"></div>
        <script type="text/javascript">
        VK.Widgets.Group("vk_groups", { mode: 3, color1: 'F9F9F6', color2: '1C1C1C' }, {$settings.Fax|sanitize});
        </script>

        {* vk отзывы *}
        {* <div id="feedback_vk"></div>
        <script src="https://feedback.kupiapp.ru/widget/widget.js" type="text/javascript"></script>
        <script type="text/javascript">document.addEventListener("DOMContentLoaded", feedback_vk.init({ id:'feedback_vk', gid:{$settings.Fax|sanitize} }));</script> *}

        {* vk опросы *}
        {* <div id="vk_poll"></div>
        <script type="text/javascript">
        VK.Widgets.Poll("vk_poll", {}, "297010422_cc6da8fdc8bfbc028a");
        </script> *}
      </div>


    </div>

  </div>

  <hr>

  <div class="uk-container uk-text-center">
      {version} {#copyright_text#}<br>
      <a href="https://vk.com/he6oru">Дизайн студия He6oru</a>
  </div>

</footer>
