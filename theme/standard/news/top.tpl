{if $topnewsitems}
    <div>
      {foreach from=$topnewsitems item=content}
          <div class="box-content clearfix">
            {if !empty($content.TopnewsBild)}
                <a href="index.php?p=news&amp;area={$content.Sektion}&amp;newsid={$content.Id}&amp;name={$content.LinkTitle|translit}">
                  <img class="img-fluid img-left" src="uploads/news/{$content.TopnewsBild}" alt="{$content.Titel|sanitize}" />
                </a>
            {elseif !empty($content.Bild)}
                <a href="index.php?p=news&amp;area={$content.Sektion}&amp;newsid={$content.Id}&amp;name={$content.LinkTitle|translit}">
                  <img class="img-fluid {if $content.Bildausrichtung == 'right'}img-right{else}img-left{/if}" src="uploads/news/{$content.Bild}" alt="{$content.Titel|sanitize}" />
                </a>
            {/if}
            <h3>
              <a href="index.php?p=news&amp;area={$content.Sektion}&amp;newsid={$content.Id}&amp;name={$content.LinkTitle|translit}">{$content.Titel|sanitize}</a>
            </h3>
            {$content.News|html_truncate:400}
          </div>
      {/foreach}
    </div>
{/if}
