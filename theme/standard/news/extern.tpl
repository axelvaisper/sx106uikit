{if !empty($externNews)}
    <div class="wrapper">
      <div class="h3 title">{#News_extern#}</div>
      <div class="box-content">
        {foreach from=$externNews item=ne}
            <div data-toggle="tooltip" title="{$ne.News|tooltip:200}" class="text-truncate">
              <a href="index.php?p=news&amp;area={$area}&amp;newsid={$ne.Id}&amp;name={$ne.Titel|translit}">
                {$ne.Titel|sanitize}
              </a>
            </div>
        {/foreach}
      </div>
    </div>
{/if}
