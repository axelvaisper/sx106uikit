{include file="$incpath/forums/user_panel.tpl"}
<div> {$navigation} </div>
<br />
{if $error}
  <div class="box-error">{$error}</div>
{/if}
<table width="100%" cellpadding="4" cellspacing="1" class="forum_tableborder">
  <tr>
    <td class="forum_header"><strong>{#Forums_ForumTitle_login#}</strong></td>
  </tr>
  <tr>
    <td class="forum_info_main">{#Forums_TitleLockedByPass#}</td>
  </tr>
  <tr>
    <td class="forum_info_main">
      <form action="index.php?p=forum&amp;action=login" method="post">
        <input type="hidden" name="fid" value="{$fid}" />
        <input type="password" name="pass" />
        <input class="btn btn-primary" type="submit" value="{#Forums_ButtonLogin#}" />
      </form>
    </td>
  </tr>
</table>
{include file="$incpath/forums/forums_footer.tpl"}
