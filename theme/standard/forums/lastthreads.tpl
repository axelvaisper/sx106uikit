{if $last_thread_array}
    <div>
      <div class="h3 title">{#Forums_lastTopics#}</div>
      <div class="box-content">
        {foreach from=$last_thread_array item=x}
            <div class="d-flex justify-content-between">
              <h5 class="text-truncate mr-2">
                <a href="{$x->tlink}"><i class="icon-chat"></i>{$x->title|truncate:200|sanitize}</a>
              </h5>
              <div class="text-nowrap">
                <i class="icon-calendar"></i>{$x->datum|date_format:$lang.DateFormat}
              </div>
            </div>
        {/foreach}
      </div>
    </div>
{/if}
