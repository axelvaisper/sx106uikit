<div class="row text-nowrap">
  <div class="col-auto">
    <form method="post" name="logout_form_up" action="index.php">
      <input type="hidden" name="p" value="userlogin" />
      <input type="hidden" name="action" value="logout" />
      <input type="hidden" name="backurl" value="{page_link|base64encode}" />
    </form>
    {if get_active("pn")}
        <a class="d-block" href="index.php?p=pn"><img src="{$imgpath_forums}mailbox_small.png" alt="" /> {#PN_inbox#} {newpn}</a>
    {/if}
    {if get_active("ignorelist")}
        <a class="d-block" href="index.php?p=forum&amp;action=ignorelist"><img src="{$imgpath_forums}ignore_small.png" alt="" /> {#Ignorelist#}</a>
    {/if}
    {if get_active('calendar')}
        <a class="d-block" href="index.php?p=calendar&amp;month={$smarty.now|date_format:'m'}&amp;year={$smarty.now|date_format:'Y'}&amp;area={$area}&amp;show=private"><img src="{$imgpath_forums}event.png" alt="" /> {#UserCalendar#}</a>
    {/if}
  </div>

  <div class="col-auto">
    <a class="d-block" onclick="return confirm('{#Confirm_Logout#}');" href="javascript: document.forms['logout_form_up'].submit();"><img src="{$imgpath_forums}logout_small.png" alt="" /> {#Logout#}</a>
    <a class="d-block" href="index.php?p=useraction&amp;action=changepass"><img src="{$imgpath_forums}password_small.png" alt="" /> {#ChangePass#}</a>
    <a class="d-block" href="index.php?p=useraction&amp;action=profile"><img src="{$imgpath_forums}profile_small.png" alt="" /> {#Profile#}</a>
    {if permission('adminpanel')}
        <a class="d-block" href="admin/" target="_blank"><img src="{$imgpath_forums}admin_small.png" alt="" /> {#AdminLink#}</a>
    {/if}
  </div>
</div>
