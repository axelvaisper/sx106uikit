<form action="index.php?p=forum&amp;action=rating" method="post">
  <input type="hidden" name="t_id" value="{$topic->id}" />
  <table>
    <tr>
      <td><strong>{#Forums_Label_rating#}</strong>&nbsp;</td>
      <td>
        <div class="rating-star" data-rating="4" data-rating-active="true" data-rating-input="rating"></div>
      </td>
      <td>
        <input class="btn btn-primary" type="submit" value="{#RateThis#}" /></td>
    </tr>
  </table>
</form>
