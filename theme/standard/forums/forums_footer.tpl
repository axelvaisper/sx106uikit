{if get_active('whosonline')}
    <div class="box-content">
      <div class="h3">{#Forums_WhosOnline#}</div>
      <div>
        {useronline}
      </div>
    </div>
{/if}
<div class="box-content">
  <div class="h3">{#Forums_Stats#}</div>
  <div>
    {forumstats}
  </div>
</div>
<div class="box-content">
  <div class="h3">{#Birthdays_Today#}</div>
  <div>
    {birthdays}
  </div>
</div>
