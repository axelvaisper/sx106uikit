<div class="forum_container">
  <fieldset>
    <legend>{#GlobalTheme#}</legend>
    <input type="text" name="topic" value="{$topic|escape: 'html'}" maxlength="200" size="50" />
  </fieldset>
  <br />
  <fieldset>
    <legend>{#Forums_PostIconTitle#}</legend>
    {foreach from=$posticons item=posticon}
      {$posticon}
    {/foreach}
  </fieldset>
  <br />
</div>
