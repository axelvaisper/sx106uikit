<table border="0" cellspacing="2" cellpadding="0">
  <tr>
    <td width="100">&nbsp;</td>
    <td width="120" align="center"><strong>{#Forums_Header_default#}</strong></td>
    <td width="120" align="center"><strong>{#Forums_Showforums_posts#}</strong></td>
    <td width="120" align="center"><strong>{#Users#}</strong></td>
  </tr>
  <tr>
    <td><strong>{#StatsToday#}</strong></td>
    <td align="center" class="text-link">{$num_threads_day}</td>
    <td align="center" class="text-link">{$num_posts_day}</td>
    <td align="center" class="text-link">{$num_members_day}</td>
  </tr>
  <tr>
    <td><strong>{#StatsWeek#}</strong></td>
    <td align="center" class="text-link">{$num_threads_week}</td>
    <td align="center" class="text-link">{$num_posts_week}</td>
    <td align="center" class="text-link">{$num_members_week}</td>
  </tr>
  <tr>
    <td><strong>{#StatsMonth#}</strong></td>
    <td align="center" class="text-link">{$num_threads_month}</td>
    <td align="center" class="text-link">{$num_posts_month}</td>
    <td align="center" class="text-link">{$num_members_month}</td>
  </tr>
  <tr>
    <td><strong>{#StatsAll#}</strong></td>
    <td align="center" class="text-link">{$num_threads}</td>
    <td align="center" class="text-link">{$num_posts}</td>
    <td align="center" class="text-link">{$num_members}</td>
  </tr>
  <tr>
    <td colspan="4"><strong>{#WelcomeNew#}</strong> <a class="forumlinks" href="index.php?p=user&amp;id={$uid}&amp;area={$area}">{$newestmember}</a></td>
  </tr>
</table>
