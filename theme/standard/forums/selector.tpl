<form action="index.php?p=showforum" method="post" name="fp" id="fp">
  {strip}
    <select name="fid" style="width: 330px">
      {foreach from=$categories_dropdown item=category}
        <optgroup label="{$category->title}">
          {foreach from=$category->forums item=forum_dropdown}
            {if $forum_dropdown->category_id == 0}
              <option style="color: #000; font-weight: bold; font-style: italic" value="{$forum_dropdown->id}" disabled="disabled">{$forum_dropdown->visible_title} </option>
            {else}
              <option value="{$forum_dropdown->id}" {if isset($smarty.request.fid) && $smarty.request.fid == $forum_dropdown->id} selected="selected" {/if}>{$forum_dropdown->visible_title} </option>
            {/if}
          {/foreach}
        </optgroup>
      {/foreach}
    </select>
  {/strip}

  <input class="btn btn-primary" type="submit" value="{#GotoButton#}" />
</form>
