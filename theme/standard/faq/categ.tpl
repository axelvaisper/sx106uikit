<script>
<!-- //
    $(function () {
        $('.user_pop').colorbox({
            height: "600px",
            width: "550px",
            iframe: true
        });
    });
//-->
</script>

<div class="wrapper">
  <h1 class="title clearfix">
    {#Faq#}
    {if permission('faq_sent')}
        <div class="float-right">
          <a class="user_pop" href="index.php?p=faq&amp;action=mail&amp;faq_id=0&amp;area={$area}">
            <i class="icon-help-circled" data-toggle="tooltip" title="{#New_Guest#}"></i>
          </a>
        </div>
    {/if}
  </h1>
  <div class="box-content">
    {if !empty($categs)}
        {foreach from=$categs item=item}
            <a class="d-block{if $item->check} mt-3{/if}" href="index.php?p=faq&amp;action=display&amp;faq_id={$item->Id}&amp;area={$area}&amp;name={$item->Name|translit}">
              {$item->visible_image}
            </a>
        {/foreach}
    {else}
        {#Faq_nothing#}
    {/if}
  </div>
</div>
