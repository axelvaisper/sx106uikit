<script>
<!-- //
$(function () {
    $('.spoiler_open').on('click', function () {
        var id = $(this).attr('id');
        $('#div_' + id).toggle();
    });
    $('.user_pop').colorbox({
        height: "600px",
        width: "550px",
        iframe: true
    });
});
//-->
</script>

<h1 class="title clearfix">
  {#Faq#}
  {if permission('faq_sent')}
      <div class="float-right">
        <a class="user_pop" href="index.php?p=faq&amp;action=mail&amp;faq_id=0&amp;area={$area}">
          <i class="icon-help-circled" data-toggle="tooltip" title="{#New_Guest#}"></i>
        </a>
      </div>
  {/if}
</h1>

{if $categs}
    <div class="form-group">
      <label class="sr-only" for="articles-jump">{#GotoArchive#}</label>
      <select class="form-control" id="articles-jump" onchange="eval(this.options[this.selectedIndex].value);selectedIndex = 0;">
        <option value="location.href = 'index.php?p=faq&amp;area={$area}'" {if empty($smarty.request.faq_id)}selected="selected"{/if}>{#AllCategs#}</option>
        {foreach from=$categs item=fc}
            <option value="location.href = 'index.php?p=faq&amp;action=display&amp;faq_id={$fc->Id}&amp;area={$area}&amp;name={$fc->Name|translit}'" {if $fc->Id == $smarty.request.faq_id}selected="selected"{/if}>{$fc->visible_title|sanitize}</option>
        {/foreach}
      </select>
    </div>
{/if}

<h3 class="title">{$cat->Name|sanitize}</h3>
<div class="box-content">
  {foreach from=$categs item=fcc}
      {if $cat->Id == $fcc->Parent_Id}
          <a class="d-block font-weight-bold spacer" href="index.php?p=faq&amp;action=display&amp;faq_id={$fcc->Id}&amp;area={$area}&amp;name={$fcc->Name|translit}">
            <i class="icon-help-circled"></i>{$fcc->Name|translit}
          </a>
      {/if}
  {/foreach}
</div>

<div class="box-content">
  {assign var=nofaq value=0}
  {foreach from=$faq item=item}
      {if $cat->Id == $item->Kategorie}
          {assign var=nofaq value=1}
          <a class="d-block spacer" href="#faq{$item->Id}">
            <i class="icon-info-circled"></i>{$item->Faq|sanitize}
          </a>
      {/if}
  {/foreach}
</div>

{if $nofaq == 1}
    {foreach from=$faq item=item}
        {if $cat->Id == $item->Kategorie}
            <div class="box-content">
              <div class="wrapper font-weight-bold">
                <a id="faq{$item->Id}" href="index.php?p=faq&amp;action=faq&amp;fid={$item->Id}&amp;area={$area}&amp;name={$item->Faq|translit}">{$item->Faq|sanitize}</a>
              </div>
              <div class="wrapper" id="div_faq_{$item->Id}" style="display: none">{$item->text}</div>

              <div class="flex-line justify-content-between">
                <a class="mx-2 spoiler_open" id="faq_{$item->Id}" href="index.php?p=faq&amp;action=faq&amp;fid={$item->Id}&amp;area={$area}&amp;name={$item->Faq|translit}" onclick="return false;">
                  {#FaqReply#}
                </a>
                {if permission('faq_sent')}
                    <a class="mx-2 user_pop" href="index.php?p=faq&amp;action=mail&amp;faq_id={$cat->Id}&amp;area={$area}">
                      {#New_Guest#}
                    </a>
                {/if}
                <a class="mx-2" href="#">{#GlobalTop#}</a>
              </div>
            </div>
        {/if}
    {/foreach}
{/if}
