<script>
<!-- //
$(function() {
    $('#linkextern').on('click', function() {
        var options = {
            target: '#plick',
            url: 'index.php?action=updatehitcount&p=links&id={$link_res->Id}',
            timeout: 3000
        };
	$(this).ajaxSubmit(options);
	return true;
    });
    $('.user_pop').colorbox({ height: "600px", width: "700px", iframe: true });
});
//-->
</script>

<div class="wrapper">
  <h1 class="title">
   <a href="{$link_res->Url}" id="linkextern" target="_blank">{$link_res->Name|sanitize}</a>
  </h1>
  <div class="wrapper clearfix">
    {if !empty($link_res->Bild)}
        <img class="img-fluid img-right" src="uploads/links/{$link_res->Bild}" alt="{$link_res->Name|sanitize}" />
    {/if}
    {$link_res->Beschreibung}
  </div>
  <div class="flex-line">
    <span class="mr-3">{#GlobalAutor#}: <a href="index.php?p=user&amp;id={$link_res->Autor}&amp;area={$area}">{$link_res->UserName}</a></span>
    <span class="mr-3">{#Date#}: {$link_res->Datum|date_format:$lang.DateFormatSimple}</span>
  </div>
</div>

<div class="wrapper">
  <div class="h3 title">{#Info#}</div>
  <div class="box-content">
    <div class="row">
      <div class="col-3">{#Added#}</div>
      <div class="col-9">
        {$link_res->Datum|date_format:$lang.DateFormatSimple}
      </div>
    </div>
    <div class="row">
      <div class="col-3">{#GlobalAutor#}</div>
      <div class="col-9">
        <a href="index.php?p=user&amp;id={$link_res->Autor}&amp;area={$area}">{$link_res->UserName}</a>
      </div>
    </div>
    <div class="row">
      <div class="col-3">{#Global_Hits#}</div>
      <div class="col-9">{$link_res->Hits}</div>
    </div>
    {if !empty($link_res->Sprache) && $Linksettings->Flaggen == 1}
        <div class="row">
          <div class="col-3">{#Country#}</div>
          <div class="col-9"><img src="{$imgpath}/flags/{$link_res->Sprache}.png" alt="" /></div>
        </div>
    {/if}
    {if $Linksettings->Wertung == 1}
        <div class="row">
          <div class="col-3">{#Rating_Rating#}</div>
          <div class="col-9 flex-line">
            <div class="rating-star" data-rating="{$link_res->Wertung}"></div>
          </div>
        </div>
    {/if}
  </div>
</div>

{if $Linksettings->Kommentare == 1 || $Linksettings->DefektMelden == 1}
    <div class="wrapper">
      <div class="h3 title">{#GlobalActions#}</div>
      <div class="box-content">
        {if permission('links_sent')}
            <a class="d-block user_pop" href="index.php?p=links&amp;area={$area}&amp;action=links_sent">
              <i class="icon-link"></i>{#LinksSent#}
            </a>
        {/if}
        {if $Linksettings->Kommentare == 1}
            <a class="d-block" href="#comments">
              <i class="icon-chat"></i>{#Comments#}
            </a>
        {/if}
        {if $Linksettings->DefektMelden == 1}
            {if !empty($link_res->DefektGemeldet)}
                <div class="text-danger">
                  <i class="icon-warning-empty"></i>{#Links_ErrorSendBrokenImpos#}
                </div>
            {else}
                <a class="d-block" onclick="document.getElementById('broken-click').style.display = '';" href="javascript:void(0);">
                  <i class="icon-warning-empty"></i>{#Links_ErrorSendBroken#}
                </a>
            {/if}
        {/if}
      </div>
    </div>
{/if}

{if $Linksettings->DefektMelden == 1}
    {include file="$incpath/other/broken.tpl"}
{/if}

{$RatingForm}
{$GetComments}
