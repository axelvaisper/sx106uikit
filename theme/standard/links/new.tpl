{if !empty($NewLinksEntries)}
<section id="links">
  <div class="container">
    <div class="row">
      <h3 class="col-12">{* {#Links_New#} *}Нам доверяют</h3>
      {foreach from=$NewLinksEntries item=e}
          <div class="col-md-auto">
    {*         <h3 class="listing-head text-truncate">
              <a title="{$e->Name|sanitize}" href="{$e->Link_Details}">{$e->Name|sanitize}</a>
            </h3> *}
              {if !empty($e->Bild)}
                  <a href="{$e->Link_Details}" title="{$e->Name|sanitize}">
                    <img class="img-fluid listing-img-right" src="uploads/links/{$e->Bild}" alt="{$e->Name|sanitize}" />
                  </a>
              {/if}
              <div class="text-justify">
                {$e->Beschreibung|truncate:500}
              </div>
          </div>
      {/foreach}
    </div>
  </div>
</section>
{/if}
