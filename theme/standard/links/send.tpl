<h1 class="title text-truncate">{$sname}</h1>
{if isset($smarty.request.sentlinks) && $smarty.request.sentlinks == 1 && empty($error)}
    <div class="text-center">
      <div class="wrapper">
        {#GlobalSend#}
      </div>
      <button class="btn btn-primary" onclick="closeWindow();">{#WinClose#}</button>
    </div>
{else}

{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function() {
    $('#cf').validate({
        rules: {
            Beschreibung: { required: true, minlength: 10 },
            Name: { required: true },
            Url: { required: true }
        },
        submitHandler: function() {
            document.forms['fc'].submit();
        }
    });
});
//-->
</script>

{if $loggedin}
{script file="$jspath/jupload.js" position='head'}
<script>
<!-- //
function fileUpload(sub, divid) {
    $(document).ajaxStart(function() {
        $('UpInf_' + divid).hide();
        $('#loading_' + divid).show();
        $('#buttonUpload_' + divid).val('{#Global_Wait#}').prop('disabled', true);
    }).ajaxComplete(function() {
        $('#loading_' + divid).hide();
        $('#buttonUpload_' + divid).val('{#UploadButton#}').prop('disabled', false);
    });
    $.ajaxFileUpload({
        url: 'index.php?p=links&action=' + sub + '&divid=' + divid,
        secureuri: false,
        fileElementId: 'fileToUpload_' + divid,
        dataType: 'json',
        success: function (data) {
	    if(typeof(data.result) !== 'undefined') {
                document.getElementById('UpInf_' + divid).innerHTML = data.result;
                if(data.filename !== '') {
                    document.getElementById('newFile_' + divid).value = data.filename;
                }
	    }
        },
        error: function (data, status, e) {
            document.getElementById('UpInf_' + divid).innerHTML = e;
        }
    });
    return false;
}
//-->
</script>
{/if}

{if !empty($error)}
    <div class="box-error">
      {foreach from=$error item=err}
          <div>{$err}</div>
      {/foreach}
    </div>
{/if}
<form name="fc" id="cf" action="index.php?p=links&amp;area={$area}&amp;action=links_sent" method="post">
  {if !empty($categs)}
      <div class="form-group">
        <label for="send-categ">{#Global_Categ#}</label>
        <select class="form-control" id="send-categ" name="Kategorie">
          <option value="0">{#Global_Select_Categ#}</option>
          {foreach from=$categs item=dd}
              <option {if isset($smarty.request.Kategorie) && $smarty.request.Kategorie == $dd->Id}selected="selected"{/if} value="{$dd->Id}">{$dd->visible_title} </option>
          {/foreach}
        </select>
      </div>
  {/if}
  <div class="form-group">
    <label for="send-name">{#GlobalTitle#}</label>
    <input class="form-control" id="send-name" name="Name" type="text" value="{$smarty.request.Name|sanitize}" />
  </div>
  <div class="form-group">
    <label for="send-link">{#GlobalLinks#}</label>
    <input class="form-control" id="send-link" name="Url" type="text" value="{$smarty.request.Url|escape:html}" />
  </div>
  <div class="form-group">
    <label for="send-body">{#Description#}</label>
    <textarea class="form-control" id="send-body" name="Beschreibung" rows="5">{$smarty.request.Beschreibung|escape:html}</textarea>
  </div>
  {if $loggedin}
      <div class="form-group">
        <label for="fileToUpload_1">{#GlobalImage#}</label>
        <div id="UpInf_1"></div>
        <div id="loading_1" style="display: none;">
          <img src="{$imgpath_page}ajaxbar.gif" alt="" />
        </div>
        <div class="input-group">
          <input class="form-control" id="fileToUpload_1" type="file" name="fileToUpload_1" />
          <span class="input-group-append">
            <input type="button" class="btn btn-primary" id="buttonUpload_1" onclick="fileUpload('uploadicon', 1);" value="{#UploadButton#}" />
          </span>
        </div>
        <input type="hidden" name="newImg_1" id="newFile_1" />
      </div>
  {/if}
  {include file="$incpath/other/captcha.tpl"}
  <div class="text-center">
    <input class="btn btn-primary" value="{#SendEmail_Send#}" type="submit">
    <input class="btn btn-secondary" onclick="closeWindow();" value="{#WinClose#}" type="button">
  </div>
  <input name="sentlinks" type="hidden" value="1" />
</form>
{/if}
