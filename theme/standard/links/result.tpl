<div class="wrapper">
  <h1 class="title">{#Links#}</h1>
  {include file="$incpath/links/search.tpl"}
  {if empty($Entries)}
      <div class="box-content">{#Links_SNotFound#}</div>
  {/if}
  {$Results}
</div>
