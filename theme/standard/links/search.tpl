<form method="post" action="index.php?p=links&amp;area={$area}&amp;action=search">
  <div class="input-group">
    <input class="form-control" name="ql" value="{$smarty.request.ql|sanitize|replace:'-':''}" type="text" />
    <span class="input-group-append">
      <input class="btn btn-primary" type="submit" value="{#Search#}" />
    </span>
  </div>
</form>
