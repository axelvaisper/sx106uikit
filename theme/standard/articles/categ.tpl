<div class="form-group">
  <label class="sr-only" for="articles-jump">{#GotoArchive#}</label>
  <select class="form-control" id="articles-jump" name="select" onchange="eval(this.options[this.selectedIndex].value);selectedIndex = 0;">
    <option value="location.href = 'index.php?p=articles&amp;area={$area}{$TypArchive}'" {if empty($smarty.request.catid)}selected="selected"{/if}>{#AllCategs#}</option>
    {foreach from=$dropdown item=dd}
        <option value="location.href = '{$dd->HLink}'" {if isset($smarty.request.catid) && $smarty.request.catid == $dd->Id}selected="selected"{/if}>{$dd->visible_title}</option>
    {/foreach}
  </select>
</div>

{if !empty($Categs)}
    <div class="box-content">
      <div class="row">
        {assign var=lcc value=0}
        {foreach from=$Categs item=c name=categ}
            {if $c->Parent_Id == $smarty.request.catid|default:0}
                <div class="col-md-6 my-1">
                  <a class="font-weight-bold" href="{$c->HLink}">
                    <i class="icon-folder-open"></i>{$c->Name|sanitize}{if $c->LinkCount >= 1} ({$c->LinkCount}){/if}
                  </a>
                </div>
                {assign var=lcc value=$lcc+1}
                {if $lcc % 2 == 0 && !$smarty.foreach.categ.last}
                </div>
                <div class="row">
                {/if}
            {/if}
        {/foreach}
      </div>
    </div>
{/if}
