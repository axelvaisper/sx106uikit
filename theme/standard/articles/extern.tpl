{if !empty($externArticles)}
    <div class="wrapper">
      <div class="h3 title">{#ParentArticles#}</div>
      <div class="box-content">
        {foreach from=$externArticles item=ne}
            <div data-toggle="tooltip" title="{$ne.News|tooltip:200}" class="text-truncate">
              <a href="index.php?p=articles&amp;area={$area}&amp;action=displayarticle&amp;id={$ne.Id}&amp;name={$ne.Titel|translit}">
                {$ne.Titel|sanitize}
              </a>
            </div>
        {/foreach}
      </div>
    </div>
{/if}
