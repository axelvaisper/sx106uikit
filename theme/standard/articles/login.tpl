<div class="wrapper">
  <h1 class="title">{#Content_passwordProtected#}</h1>
  <div class="box-content">
    <div class="h4">{#PassLogin#}</div>
    <p>{#Content_LoginText#}</p>
    <form class="form-inline" name="form1" method="post" action="">
      <div class="form-group mr-sm-3">
        <label for="input-pass" class="sr-only">{#Pass#}</label>
        <input class="form-control" type="password" id="input-pass" name="Artikel_Kennwort_{$res.Id}" placeholder="{#Pass#}" required="required" />
      </div>
      <input class="btn btn-primary" type="submit" name="button" value="{#ButtonSend#}" />
    </form>
  </div>
</div>
