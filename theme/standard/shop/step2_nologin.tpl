{if $smarty.request.inf == 'regcode'}
    <div class="box-content">
      {#RegE_infEmailCode#}
    </div>
{/if}

<div class="row align-items-stretch">
  <div class="col">
    <div class="h4 title text-truncate">{#Shop_v_opt2_t#}</div>
    <div class="box-content">
      <div class="wrapper">
        {#Shop_v_opt2#}
      </div>
      {if $login_false == 1}
          <div class="box-error">{#Shop_v_opt2_e#}</div>
      {/if}

      <form method="post" action="index.php">
        <div class="form-group">
          <label class="" for="l_s_login_email">{#Email#}</label>
          <input class="form-control" id="l_s_login_email" name="s_login_email" type="text" value="" />
        </div>

        <div class="form-group">
          <label class="" for="l_s_login_pass">{#Pass#}</label>
          <input class="form-control" id="l_s_login_pass" name="s_login_pass" type="password" value="" />
        </div>

        <div class="form-group">
          <div class="form-check">
            <input class="form-check-input" id="l_s_logged" name="s_staylogged" value="1" type="checkbox" />
            <label class="form-check-label" for="l_s_logged">{#PassCookieHelp#}</label>
          </div>
        </div>

        <form method="get" action="index.php">
          <div class="text-center">
            <input class="btn btn-primary btn-block-sm" type="submit" value="{#Login_Button#}" />
          </div>

          <input type="hidden" name="p" value="shop" />
          <input type="hidden" name="area" value="{$area}" />
          <input type="hidden" name="action" value="shoporder" />
          <input type="hidden" name="subaction" value="step2" />
          <input type="hidden" name="s_login" value="1" />
          {if isset($smarty.request.inf) && $smarty.request.inf == 'regcode'}
              <input type="hidden" name="inf" value="regcode" />
          {/if}
        </form>
    </div>
  </div>

  <div class="col">
    <div class="h4 title text-truncate">{#Shop_v_opt3_t#}</div>
    <div class="box-content">
      <div class="wrapper">
        {#Shop_v_opt3#}
      </div>
      <form method="post" action="index.php">
        <div class="text-center">
          <input class="btn btn-primary btn-block-sm" type="submit" value="{#GlobalNext#}" />
        </div>
        <input type="hidden" name="p" value="shop" />
        <input type="hidden" name="area" value="{$area}" />
        <input type="hidden" name="action" value="shoporder" />
        <input type="hidden" name="subaction" value="step2" />
        <input type="hidden" name="register" value="new" />
      </form>
    </div>
  </div>

  {if $guest_order == 1}
      <div class="col">
        <div class="h4 title text-truncate">{#Shop_v_opt1_t#}</div>
        <div class="box-content">
          <div class="wrapper">
            {#Shop_v_opt_2#}
          </div>
          <form method="get" action="index.php">
            <div class="text-center">
              <input class="btn btn-primary btn-block-sm" type="submit" value="{#GlobalNext#}" />
            </div>
            <input type="hidden" name="p" value="shop" />
            <input type="hidden" name="area" value="{$area}" />
            <input type="hidden" name="action" value="shoporder" />
            <input type="hidden" name="subaction" value="step2" />
            <input type="hidden" name="order" value="guest" />
          </form>
        </div>
      </div>
  {/if}
</div>
