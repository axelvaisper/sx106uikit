<div class="uk-card-default" id="vote">
    <div class="h3 title">{#Shop_prod_vote_votesall#}</div>
    {if $votes}
      {foreach from=$votes item=v}
          <div class="box-content">
            <div class="row">
              <div class="col-2">{#Date#}:</div>
              <div class="col">{$v->Datum|date_format:$lang_settings.Zeitformat}</div>
            </div>

            <div class="row">
              <div class="col-2">{#GlobalAutor#}:</div>
              <div class="col">{$v->Benutzer}</div>
            </div>

            <div class="row">
              <div class="col-2">{#Shop_prod_vote_auttext#}</div>
              <div class="col">{$v->Bewertung}</div>
            </div>

            <div class="row">
              <div class="col-2">{#Shop_prod_vote_points#}</div>
              <div class="col">
                <div class="rating-star" data-rating="{$v->Bewertung_Punkte}"></div>
              </div>
            </div>
          </div>
      {/foreach}
    {else}
      <div class="box-content">
        {#Shop_prod_vote_novotes#}
      </div>
    {/if}

    <div class="box-content">
    {if !permission('shop_vote')}
        {#Shop_prod_vote_login#}
    {else}

      {script file="$jspath/jvalidate.js" position='head'}
      <script>
      <!-- //
      {include file="$incpath/other/jsvalidate.tpl"}
      $(function () {
          $('#prod_vote_form').validate({
              rules: {
                  prod_vote_text: { required: true, minlength: 10 }
              },
              submitHandler: function () {
                  document.forms['prod_vote_form'].submit();
              }
          });
      });
      //-->
      </script>

        {assign var=secure_uniqid value="two"}
        <a name="vote_form"></a>
        {if !empty($error{$secure_uniqid})}
            <div class="box-error">
              {foreach from=$error{$secure_uniqid} item=err}
                  <div>{$err}</div>
              {/foreach}
            </div>
        {/if}
        <form name="prod_vote_form" id="prod_vote_form" method="post" action="{page_link}#vote_form">
          <input type="hidden" name="id" value="{$smarty.request.id}" />
          <input type="hidden" name="red" value="{$red}" />
          <input type="hidden" name="sub" value="prod_vote" />
          <input type="hidden" name="prod_name" value="{$p.Titel|sanitize}" />

          <div class="row form-group">
            <div class="col-md-3 col-sm-12">
              <label class="col-form-label" for="prod-vote-text">{#GlobalMessage#}</label>
            </div>
            <div class="col-md-9 col-sm-12">
              <textarea class="form-control" id="prod-vote-text" name="prod_vote_text"rows="3">{$smarty.request.prod_vote_text|sanitize}</textarea>
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-3 col-sm-12">{#Shop_prod_vote_points#}</div>
            <div class="col-md-9 col-sm-12">
              <div class="rating-star" data-rating="4" data-rating-active="true" data-rating-input="prod_vote_points"></div>
            </div>
          </div>

          {include file="$incpath/other/captcha.tpl"}

          <div class="text-center">
            <input class="btn btn-primary btn-block-sm" type="submit" value="{#RateThis#}" />
          </div>
        </form>
    {/if}
    </div>
</div>