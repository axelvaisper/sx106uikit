<div class="box-content">
  <div class="wrapper">
    {$p.Beschreibung|autowords}
  </div>
  {if !empty($p.BeschreibungLang)}
      <div class="wrapper">
        {$p.BeschreibungLang|autowords}
      </div>
  {/if}
  {if !empty($article_pages)}
      <div class="wrapper">
        {$article_pages}
      </div>
  {/if}
</div>