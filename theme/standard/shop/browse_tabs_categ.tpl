<div class="box-content">
  <div class="row spacer">
    {assign var=break_count value=0}
    {foreach from=$sub_categs item=subcateg name=categ}
        <div class="col-4 text-center">
          {if $subcateg->Bild_Kategorie}
              <a href="index.php?p=shop&amp;action=showproducts&amp;cid={$subcateg->catid}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$subcateg->Name|translit}">
                <img class="img-fluid height-6" data-toggle="tooltip" title="{$subcateg->Name|tooltip}" src="uploads/shop/icons_categs/{$subcateg->Bild_Kategorie}" alt="{$subcateg->Name}" />
              </a>
          {else}
              <h4>
                <a href="index.php?p=shop&amp;action=showproducts&amp;cid={$subcateg->catid}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$subcateg->Name|translit}">
                  {$subcateg->Name}
                </a>
              </h4>
          {/if}
        </div>
        {assign var=break_count value=$break_count+1}
        {if $break_count % 3 == 0 && !$smarty.foreach.categ.last}
        </div>
        <div class="row spacer">
        {/if}
    {/foreach}
  </div>
</div>
