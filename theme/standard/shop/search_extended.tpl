<form method="post" action="">
<div class="wrapper panel">
<div class="panel-head">{#ExtendedSearch#}</div>
<div class="panel-main">

    <div class="form-group">
      <input class="form-control" id="ext-search" name="shop_q" type="text" value="{if $smarty.request.shop_q != 'empty'}{$smarty.request.shop_q|escape:html}{/if}" placeholder="{#Shop_search_e_keyword#}.." />
    </div>

    <div class="form-group">
      <label for="ext-offers">{#Shop_Offers#}</label>
      <div class="form-check">
        <input class="form-check-input" id="ext-offers" name="offers" value="1"{if $smarty.request.offers == 1} checked="checked"{/if} type="checkbox" />
        <label class="form-check-label" for="ext-offers">{#Shop_searchJustOffers#}</label>
      </div>
    </div>

    <div class="form-group">
      <label for="ext-cid">{#Global_Categ#}</label>
      <select class="custom-select" id="ext-cid" name="cid">
        <option value="0">- {#AllCategs#}</option>
        {foreach from=$MySearchCategs item=mysc}
            <option value="{$mysc->Id}" {if isset($smarty.request.cid) && $smarty.request.cid == $mysc->Id}selected="selected" {/if}>{$mysc->Entry|sanitize}</option>
            {if $mysc->Sub1}
                {foreach from=$mysc->Sub1 item=sub1}
                    <option value="{$sub1->Id}" {if isset($smarty.request.cid) && $smarty.request.cid == $sub1->Id}selected="selected" {/if}>-&nbsp;{$sub1->Entry|sanitize}</option>
                    {if $sub1->Sub2}
                        {foreach from=$sub1->Sub2 item=sub2}
                            <option value="{$sub2->Id}" {if isset($smarty.request.cid) && $smarty.request.cid == $sub2->Id}selected="selected" {/if}>-&nbsp;-&nbsp;{$sub2->Name|sanitize}</option>
                            {if $sub2->Sub3}
                                {foreach from=$sub2->Sub3 item=sub3}
                                    <option value="{$sub3->Id}" {if isset($smarty.request.cid) && $smarty.request.cid == $sub3->Id}selected="selected" {/if}>-&nbsp;-&nbsp;-&nbsp;{$sub3->Name|sanitize}</option>
                                    {if $sub3->Sub4}
                                        {foreach from=$sub3->Sub4 item=sub4}
                                            <option value="{$sub4->Id}" {if isset($smarty.request.cid) && $smarty.request.cid == $sub4->Id}selected="selected" {/if}>-&nbsp;-&nbsp;-&nbsp;-&nbsp;{$sub4->Name|sanitize}</option>
                                        {/foreach}
                                    {/if}
                                {/foreach}
                            {/if}
                        {/foreach}
                    {/if}
                {/foreach}
            {/if}
        {/foreach}
      </select>
    </div>

    <div class="form-group">
      <label for="ext-manu">{#Manufacturer#}</label>
      <select class="custom-select" id="ext-manu" name="man">
        <option value="0"> - {#Shop_allManuf#}</option>
        {foreach from=$shop_manufaturers item=man}
            <option value="{$man.Id}" {if $smarty.request.man == $man.Id}selected="selected" {/if}>{$man.Name|sanitize}</option>
        {/foreach}
      </select>
    </div>

    <div class="form-group">
      <label for="ext-pfrom">{#Shop_search_e_pfrom#}</label>
      <input class="form-control" id="ext-pfrom" name="pf" type="text" value="{$smarty.request.pf|default:'1.00'}" maxlength="15" />
    </div>

    <div class="form-group">
      <label for="ext-ptill">{#Shop_search_e_ptill#}</label>
      <input class="form-control" id="ext-ptill" name="pt" type="text" value="{$smarty.request.pt|default:'1000.00'}" maxlength="15" />
    </div>

  <div class="text-center">
    <input class="btn btn-primary btn-block-sm" type="submit" value="{#StartSearch#}" />
  </div>

  <input type="hidden" name="p" value="shop" />
  <input type="hidden" name="action" value="showproducts" />
  <input type="hidden" name="list" value="{$smarty.request.list|default:$shopsettings->Sortable_Produkte}" />
  <input type="hidden" name="limit" value="{$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}" />
  <input type="hidden" name="s" value="1" />
  <input type="hidden" name="page" value="{$smarty.request.page|default:'1'}" />
  <input type="hidden" name="avail" value="{$smarty.request.avail|default:'0'}" />
</div>
</div>
</form>
