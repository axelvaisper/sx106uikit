{if $p.Fsk18 == 1 && $fsk_user != 1}
    {assign var="not_possible_to_buy" value=1}
{/if}

{if isset($notfound) && $notfound == 1}
    <div class="box-content">
        <div class="uk-alert-warning" uk-alert>
            {#Shop_errorProduct#}
        </div>
    </div>
{else}

    {script file="$jspath/jvalidate.js" position='head'}
    <script>
    <!-- //
    {include file = "$incpath/other/jsvalidate.tpl"}
    //  $(function() {
    //     $('.img-popup').colorbox({
    //         photo: true,
    //         maxHeight: "98%",
    //         maxWidth: "98%",
    //         slideshow: true,
    //         slideshowAuto: false,
    //         slideshowSpeed: 2500,
    //         current: "{#GlobalImage#} {ldelim}current{rdelim} {#PageNavi_From#} {ldelim}total{rdelim}",
    //         slideshowStart: "{#GlobalStart#}",
    //         slideshowStop: "{#GlobalStop#}",
    //         previous: "{#GlobalBack#}",
    //         next: "{#GlobalNext#}",
    //         close: "{#GlobalGlose#}"
    //     });
    //     $('.img-popup-more').colorbox({ height: '95%', width: '80%', iframe: true });
    //     $('#tobasket').validate({
    //         rules: {
    //         {if !empty($p.Frei_1) && $p.Frei_1_Pflicht == 1}
    //         free_1: { required: true },
    //         {/if}
    //         {if !empty($p.Frei_2) && $p.Frei_2_Pflicht == 1}
    //         free_2: { required: true },
    //         {/if}
    //         {if !empty($p.Frei_3) && $p.Frei_3_Pflicht == 1}
    //         free_3: { required: true },
    //         {/if}
    //         amount: { required: true, number: true }
    //         },
    //         messages: { },
    //         submitHandler: function(form) {
    //             $(form).ajaxSubmit({
    //                 {if empty($smarty.request.blanc)}
    //                 target: '#ajaxbasket',
    //                 {/if}
    //                 timeout: 6000,
    //                 success: showResponse,
    //                 clearForm: false,
    //                 resetForm: false
    //             });
    //         }
    //     });
    // });

    $(function(){
        $("#to_mylist").on("click", function(){
             UIkit.notification({
                    message: '<i uk-icon="icon:check"></i> {#Shop_ProdAddedToList#}<br><hr><a href="index.php?p=shop&amp;action=mylist" class="uk-button uk-button-link uk-button-small uk-width-1-1">{#Shop_mylist#}</a>',
                    status: 'default',
                    pos: 'top-center',
                    timeout: 4000
                });
        });
    });

    // $('#to_mylist').val(0)
    //-->
    </script>

    {include file="$incpath/shop/notification.tpl" dialog='product'}

    <form method="post" name="product_request_form" action="{page_link}#product_request"><input type="hidden" name="subaction" value="product_request" />
    </form>

        <div class="uk-container uk-container-large">
        <h1>{$p.Titel|sanitize}</h1>
        <div class="uk-grid-collapse uk-card-default" uk-grid>

            {*<div class="col-md-2 col-sm-12 wrapper">
            <div class="row justify-content-center">

              <div class="col-auto">
                <div class="wrapper">
                  {if empty($p.NoBild)}
                      <a class="img-popup" rel="poppim" href="{$p.BildPopLink}">
                        <img class="img-fluid" src="{$p.Bild}" alt="{$p.Titel|sanitize}" />
                      </a>
                  {else}
                      <img src="{$p.Bild}" alt="{$p.Titel|sanitize}" />
                  {/if}
                </div>
              </div>

             <div class="col-auto">
                {if $images}
                    <div class="row">
                      {assign var=icount value=0}
                      {assign var=showlink value=0}
                      {foreach from=$images item=im name=categ}
                          {assign var=icount value=$icount+1}
                          {if $icount < 5}
                              <div class="col spacer">
                                <a class="img-popup" rel="poppim" href="{$im.Bild_GrossLink}">
                                  <img class="img-fluid" src="{$im.Bild}" alt="" />
                                </a>
                              </div>
                              {if $icount % 2 == 0 && !$smarty.foreach.categ.last}
                              </div>
                              <div class="row">
                              {/if}
                          {else}
                              {assign var=showlink value=1}
                              <div style="display:none">
                                <a class="img-popup" rel="poppim" href="{$im.Bild_GrossLink}">
                                  <img class="img-fluid" src="{$im.Bild}" alt="" />
                                </a>
                              </div>
                          {/if}
                      {/foreach}
                    </div>

                    {if $showlink == 1}
                        <div class="spacer text-center">
                          <a class="img-popup-more" href="index.php?p=misc&do=shopimgages&prodid={$p.Id}">
                            <div class="small">{#Shop_moreImages#}</div>
                          </a>
                        </div>
                    {/if}
                {/if}
              </div>

              {if $p.Fsk18 == 1}
                  <div class="col-auto">
                    <div class="spacer text-center">
                      <img class="img-fluid" src="{$imgpath_page}special.png" alt="{#Shop_isFSKWarning#}" />
                      <div class="small">{#Shop_isFSKWarning#}</div>
                    </div>
                  </div>
              {/if}

            </div>
            </div>*}

            {*КАРТИНКИ И ТП*}
            <div class="uk-width-expand@s uk-card-default uk-padding-small">


              {* {#Shop_ArticleNumber#}: {$p.Artikelnummer} *}

              {* {include file="$incpath/shop/product_map.tpl"} *}
              {* {include file="$incpath/shop/product_kadmap.tpl"} *}

              {* {#Shop_Availablility#}: {$p.VIcon} *}
              {* <div hidden="">
                {if $not_on_store == 1}
                    {$p.VMsg|sanitize}
                {else}
                    <div>
                      {#Shop_shipping_timeinf#}:
                      {if $order_for_you == 1}
                          {$available_array.3->Name|sanitize}
                      {else}
                          {$p.Lieferzeit|sanitize}
                      {/if}
                    </div>

                    {if $p.Lagerbestand > 0}
                        {if $low_amount == 1}
                            <div class="text-danger">
                              {#Shop_lowAmount#}
                              <div>
                                {$lang.ShopLowWarnInf|replace:'__COUNT__':$p.Lagerbestand}
                              </div>
                            </div>
                        {elseif $shopsettings->Zeige_Lagerbestand == 1}
                            {#Shop_av_storeAv#}: {$p.Lagerbestand}
                        {/if}
                    {/if}
                {/if}
              </div> *}

            </div>{*/КАРТИНКИ И ТП*}

            {* ЦЕНА и ДЕТАЛИ *}
            <div class="uk-width-1-4@s uk-padding-small">

                <form method="post" id="tobasket" action="index.php?p=shop&amp;area={$area}">
                    <input type="hidden" value="{$p.Preis|jsnum}" id="price_hidden" name="price_h" />

                    {include file="$incpath/shop/product_price.tpl"}

                    <ul class="uk-subnav uk-subnav-divider" uk-margin>

                        {if $shipping_free == 1}
                          <li class="shop-sticker">
                            {#Shop_freeshipping#}
                          </li>
                        {/if}
                        {if $p.diffpro > 0}
                          <li class="shop-sticker">
                            {#Shop_Billiger#}{$p.diffpro|numformat}%
                          </li>
                        {/if}
                        {if get_active('shop_merge')}
                          <li uk-lightbox>
                            <a data-type="iframe" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1&amp;prodid={$p.Id}&amp;cid={$p.Kategorie}">
                              <i uk-icon="list"></i> {#Merge#}
                            </a>
                          </li>
                        {/if}
                        {*<li>
                        <a href="#product_request" onclick="document.forms['product_request_form'].submit();
                                return false;">
                          <i class="icon-chat"></i> {#Shop_prod_request_link#}
                        </a>
                        </li> *}

                        {if $shop_bewertung == 1}
                          <li>
                            <a href="#vote">
                              <i class="icon-thumbs-up"></i>{#Shop_prod_votes_link#}
                            </a>
                          </li>
                        {/if}

                        {if isset($shop_cheaper)}{*Хочу дешевле*}
                            <li><a id="cheaper_link" href="#" title="{#cheaper_name#}">
                              <i class="icon-warning-empty"></i>{#cheaper_name#}
                            </a></li>
                            {$shop_cheaper}
                        {/if}
                    </ul>


                    {if $not_possible_to_buy == 1}
                          {$shopsettings->Fsk18}
                    {/if}

                    {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                        {* {include file="$incpath/shop/product_vars.tpl"} *}
                        {include file="$incpath/shop/product_config.tpl"}
                       {include file="$incpath/shop/product_amount_submit.tpl"}
                    {/if}

                </form>

                <ul uk-accordion>

                  {if $smarty.request.subaction == 'product_request' || $shopsettings->AnfrageForm == 1}
                    <li>
                      <a class="uk-accordion-title" href="#">{#Shop_prod_request#}</a>
                      <div class="uk-accordion-content">
                        {include file="$incpath/shop/product_request.tpl"}
                      </div>
                    </li>
                  {/if}

                  {if get_active('shop_preisalarm') && ($shopsettings->PreiseGaeste == 1 || $loggedin)}
                    <li>
                      <a class="uk-accordion-title" href="#">{#Shop_priceAlert#}</a>
                      <div class="uk-accordion-content">
                        {$price_alert}
                      </div>
                    </li>
                  {/if}

                </ul>

            </div>{*/ЦЕНА и ДЕТАЛИ *}

        </div>
        </div>



<div class="uk-container">

    <div class="uk-card-default uk-margin-small-top">
        {*табы меню*}
        <ul class="uk-child-width-expand" uk-tab="connect: #tabproduct; animation: uk-animation-slide-left-small, uk-animation-slide-right-small">
          <li class="uk-active"><a>{#buttonDetails#}</a></li>
          {if $Zub_a_products_array}
          <li><a>{$tabs->TAB1|sanitize}</a></li>
          {/if}
          {if $Zub_b_products_array}
          <li><a>{$tabs->TAB2|sanitize}</a></li>
          {/if}
          {if $Zub_c_products_array}
          <li><a>{$tabs->TAB3|sanitize}</a></li>
          {/if}
          {if $shopsettings->similar_product == 1 && $Zub_d_products_array}
          <li><a>{#Shop_detailSimilar#}</a></li>
          {/if}
          {if $prod_downloads}
          <li><a>{#Shop_Downloads#}</a></li>
          {/if}
        </ul>

        {* табы контент *}
        <ul id="tabproduct" class="uk-switcher">
          <li >
            <div class="uk-grid-small uk-grid-divider" uk-grid>
                <div class="uk-width-expand@s">
                    {include file="$incpath/shop/products_description.tpl"}
                </div>
                <div class="uk-width-1-3@s">
                    {include file="$incpath/shop/products_details.tpl"}
                </div>
            </div>
          </li>
          {if $Zub_a_products_array}
              <li >
                {$Zub_a_products}
              </li>
          {/if}
          {if $Zub_b_products_array}
              <li >
                {$Zub_b_products}
              </li>
          {/if}
          {if $Zub_c_products_array}
              <li >
                {$Zub_c_products}
              </li>
          {/if}
          {if $shopsettings->similar_product == 1 && $Zub_d_products_array}
              <li >
                {$Zub_d_products}
              </li>
          {/if}
          {if $prod_downloads}
              <li>
                <div class="box-content">
                  {foreach from=$prod_downloads item=pdd}
                      <div>
                        <div >
                          <a href="{$baseurl}/uploads/shop/product_downloads/{$pdd->Datei}">
                            <img src="{$imgpath}/filetypes/{$pdd->Icon}" alt="" />
                          </a>
                        </div>

                        <div >
                          <a href="{$baseurl}/uploads/shop/product_downloads/{$pdd->Datei}">
                            {$pdd->DlName}
                          </a>
                          {$pdd->Beschreibung}
                        </div>

                        <div>{$pdd->Size}</div>
                      </div>
                  {/foreach}
                </div>
              </li>
          {/if}
        </ul>
    </div>

    {if $shop_bewertung == 1}{* рейтинг *}
        {include file="$incpath/shop/shop_bewertung.tpl"}
    {/if}

</div>

{/if}

<div class="uk-container">
    <div class="uk-padding-small uk-card-default uk-margin-small-top uk-margin-small-bottom">
      <div class="h3 title">{#Shop_detailLastSeen#}</div>
      {$small_seen_products}
    </div>

    {if $shopsettings->vat_info_product == 1}
        {include file="$incpath/shop/vat_info.tpl"}
    {/if}
</div>