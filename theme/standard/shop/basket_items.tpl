<script>
<!-- //
$(function () {
    $('.click-refres').on('click', function () {
        $('#form-refres-' + $(this).data('id')).submit();
    });
    $('.click-remove').on('click', function () {
        $('#form-remove-' + $(this).data('id')).submit();
    });
});
//-->
</script>

<div class="row text-center">
  <div class="col-2">{#GlobalImage#}</div>
  <div class="col-4">{#Products#}</div>
  <div class="col-2">{#Shop_amount#}</div>
  <div class="col-2">{#Products_price#}</div>
  <div class="col-2">{#Shop_basket_ovall#}</div>
</div>
  {foreach from=$product_array item=p}
      <div class="spacer border-top">
        <div class="row">
          <div class="col-2 spacer border-right text-center">
            <a href="{$p->ProdLink}">
              <img class="img-fluid" src="{$p->Bild}" alt="{$p->Titel|sanitize}" />
            </a>
          </div>

          <div class="col-4 spacer border-right">
            <a href="{$p->ProdLink}">{$p->Titel|sanitize}</a>
          <div class="small">{#Shop_f_artNr#}: {$p->Artikelnummer}</div>
          {foreach from=$p->Vars item=v}
              <div class="small">{$v->KatName}: {$v->Name} ({$v->Wert|numformat} {$currency_symbol})</div>
          {/foreach}
          {if $p->FreeFields}
              <div class="small">{#Konfiguration#}</div>
              <div class="small">{$p->FreeFields}</div>
          {/if}
          <div class="small">{$lang.Shop_shipping_timeinf}: {$p->Lieferzeit}</div>
        </div>

        <div class="col-2 spacer border-right text-center">
          {if $basket_display == 'step4'}
              {$p->Anzahl}
          {else}
              <form id="form-refres-{$p->Id}" method="post" action="index.php">

                <div class="d-flex">
                  <input class="form-control w-50" name="amount" type="text" value="{$p->Anzahl}" />
                  <a class="click-refres" data-id="{$p->Id}" href="javascript:void(0);">
                    <i class="icon-cw size-xl" data-toggle="tooltip" title="{$lang.Shop_refreshItem}"></i>
                  </a>
                  <a class="click-remove" data-id="{$p->Id}" href="javascript:void(0);">
                    <i class="icon-cancel size-xl" data-toggle="tooltip" title="{$lang.Shop_delItem}"></i>
                  </a>
                </div>

                {foreach from=$p->Varianten item=x}
                    <input type="hidden" name="mod[]" value="{$x}" />
                {/foreach}
                <input type="hidden" name="basket_refresh" value="1" />
                <input type="hidden" name="p" value="shop" />
                <input type="hidden" name="area" value="{$area}" />
                <input type="hidden" name="action" value="to_cart" />
                <input type="hidden" name="redir" value="{page_link}" />
                <input type="hidden" name="product_id" value="{$p->Id}" />
              </form>
              <form id="form-remove-{$p->Id}" method="post" action="index.php">
                {foreach from=$p->Varianten item=x}
                    <input type="hidden" name="mod[]" value="{$x}" />
                {/foreach}
                <input type="hidden" name="p" value="shop" />
                <input type="hidden" name="area" value="{$area}" />
                <input type="hidden" name="action" value="delitem" />
                <input type="hidden" name="redir" value="{page_link}" />
                <input type="hidden" name="product_id" value="{$p->Id}" />
              </form>
          {/if}
        </div>

        <div class="col-2 spacer border-right text-right">
          {if $show_vat_table == 1 && $shopsettings->NettoPreise == 1}
              {if $show_vat_table == 1}
                  {#Shop_netto#}
              {/if}
              <span class="shop-price">{$p->Preis|numformat} {$currency_symbol}</span>
              {if $show_vat_table == 1 && $shopsettings->NettoPreise != 1}
                  <div class="small">{#Shop_brutto#} {$p->Preis_b|numformat} {$currency_symbol}</div>
              {/if}
          {else}
              {if $show_vat_table != 1}
                  <span class="shop-price">{$p->Preis|numformat} {$currency_symbol}</span>
              {else}
                  <span class="shop-price">{$p->Preis_b|numformat} {$currency_symbol}</span>
                  {if $show_vat_table == 1}
                      <div class="small">{#Shop_netto#} {$p->Preis|numformat} {$currency_symbol}</div>
                  {/if}
              {/if}
          {/if}
        </div>

        <div class="col-2 spacer text-right">
          {if $show_vat_table == 1 && $shopsettings->NettoPreise == 1}
              {if $show_vat_table == 1}
                  {#Shop_netto#}
              {/if}
              <span class="shop-price">{$p->Endpreis|numformat} {$currency_symbol}</span>
              {if $show_vat_table == 1  && $shopsettings->NettoPreise != 1}
                  <div class="small">{#Shop_brutto#} {$p->Preis_bs|numformat} {$currency_symbol}</div>
              {/if}
          {else}
              {if $show_vat_table != 1}
                  <span class="shop-price">{$p->Endpreis|numformat} {$currency_symbol}</span>
              {else}
                  <span class="shop-price">{$p->Preis_bs|numformat} {$currency_symbol}</span>
                  {if $show_vat_table == 1}
                      <div class="small">{#Shop_netto#} {$p->Endpreis|numformat} {$currency_symbol}</div>
                  {/if}
              {/if}
          {/if}
        </div>

      </div>
    </div>
{/foreach}

