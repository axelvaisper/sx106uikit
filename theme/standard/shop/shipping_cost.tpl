<!--shiping_cost.tpl-->
<h1 class="title">{$title_html}</h1>
{if $popup != 1}
    <div class="box-content">
    {/if}

    <div class="wrapper">
      {#Shop_shipping_cost_inf1#}
    </div>
    {if $freeshipping > 0}
        <div class="wrapper">
          {$freeshipping_inf2}
        </div>
    {/if}

    <div class="row text-center font-weight-bold">
      <div class="col-3">
        {#Shop_shipping_method#}
      </div>
      <div class="col-3">
        {#Shop_shipping_countries#}
      </div>
      <div class="col-2">
        {#Shop_shipping_during#}
      </div>
      <div class="col-4 text-center">
        {#Shop_shipping_cost#}
      </div>
    </div>

    {foreach from=$shipper item=s}
        <div class="spacer border-top">
          <div class="row">
            <div class="col-3 spacer border-right">
              <div class="wrapper">
                {$s->Name|sanitize}
              </div>
              {if $s->Icon}
                  <div class="wrapper">
                    <img class="img-fluid" src="uploads/shop/shipper_icons/{$s->Icon}" alt="{$s->Name|sanitize}" />
                  </div>
              {/if}
              {$s->Beschreibung}
            </div>

            <div class="col-3 spacer border-right">
              {foreach from=$s->Laenders item=laender}
                  <div>
                    {$laender}
                  </div>
              {/foreach}
            </div>

            <div class="col-2 spacer border-right">
              {if !empty($s->Versanddauer)}
                  {$s->Versanddauer} {#Shop_shipping_days#}
              {/if}
            </div>

            <div class="col-4 spacer">
              {if $s->Gebuehr_Pauschal > 0}
                  <div class="d-flex justify-content-between">
                    <div class="text-right">
                      {#Shop_shipping_pausch#}
                    </div>
                    <div class="font-weight-bold required">
                      {$s->Gebuehr_Pauschal|numformat} {$currency_symbol}</strong>
                    </div>
                  </div>
              {else}
                  {foreach from=$s->Volumes item=volumes}
                      <div class="d-flex justify-content-between">
                        <div>
                          {if $volumes->Von > 0}
                              {$volumes->Von}
                          {/if}
                          {if $volumes->Von > 0}
                              -
                          {/if}
                          {$volumes->Bis} кв.м.
                        </div>
                        <div class="font-weight-bold required">
                          {$volumes->Gebuehr|numformat} {$currency_symbol}
                        </div>
                      </div>
                  {/foreach}
              {/if}
            </div>
          </div>
        </div>
    {/foreach}

    <div class="wrapper">
      {$Inf_Footer}
    </div>

    {if $popup != 1}
    </div>
{else}
    <div class="text-center">
      <input class="btn btn-primary" onclick="window.print();" type="button" value="{#PrintNow#}" />
      <input class="btn btn-secondary" onclick="closeWindow();" type="button" value="{#WinClose#}" />
    </div>
{/if}
