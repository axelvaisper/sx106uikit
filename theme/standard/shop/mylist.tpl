<div class="h3 title">{#Shop_mylist#}</div>
<div class="box-content">
  {#Shop_mylist_inf#}
</div>
{if !$product_array}
    <div class="box-content">
      <div class="box-error">{#Shop_mylist_false#}</div>
    </div>
{else}
    <div class="box-content">
      <div class="row row-header">
        <div class="col">{#Products#}</div>
        <div class="col"></div>
        <div class="col text-center">{#Shop_basket_amount2#}</div>
        <div class="col text-center">{#Products_price#}</div>
        <div class="col text-center">{#Shop_basket_ovall#}</div>
        <div class="col text-right">{#GlobalActions#}</div>
      </div>

      <div class="wrapper">
        {foreach from=$product_array item=p}
            <div class="row {cycle name='list' values='row-primary,row-secondary'}">

              <div class="col">
                <a href="{$p->ProdLink}">
                  <img class="img-fluid" src="{$p->Bild}" alt="" />
                </a>
              </div>

              <div class="col">
                <a href="{$p->ProdLink}">{$p->Titel|sanitize}</a>
                <div class="small">{#Shop_f_artNr#}: {$p->Artikelnummer}</div>
                {foreach from=$p->Vars item=v}
                    <div class="small">{$v->KatName}: {$v->Name} ({$v->Wert|numformat} {$currency_symbol})</div>
                {/foreach}
              </div>

              <div class="col text-center">
                {$p->Anzahl}
              </div>

              <div class="col">
                {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                    {if $p->Preis > 0}
                        <div class="small">{#Shop_netto#} {$p->Preis|numformat} {$currency_symbol}</div>
                        {if $show_vat_table == 1}
                            <div class="small">{#Shop_brutto#} {$p->Preis_b|numformat} {$currency_symbol}</div>
                        {/if}
                    {else}
                        {#Zvonite#}
                    {/if}
                {else}
                    {#Shop_prices_justforUsers#}
                {/if}
              </div>

              <div class="col">
                {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                    {if $p->Endpreis > 0}
                        <div class="small">{#Shop_netto#} {$p->Endpreis|numformat} {$currency_symbol}</div>
                        {if $show_vat_table == 1}
                            <div class="small">{#Shop_brutto#} {$p->Preis_bs|numformat} {$currency_symbol}</div>
                        {/if}
                    {else}
                        {#Zvonite#}
                    {/if}
                {else}
                    {#Shop_prices_justforUsers#}
                {/if}
              </div>

              <div class="col text-right">
                <form class="d-inline" name="to_basket_{$p->Id}" method="post" action="index.php">
                  <input name="amount" type="hidden" value="{$p->Anzahl}" />
                  {foreach from=$p->Varianten item=x}
                      <input type="hidden" name="mod[]" value="{$x}" />
                  {/foreach}
                  <input type="hidden" name="p" value="shop" />
                  <input type="hidden" name="area" value="{$area}" />
                  <input type="hidden" name="action" value="to_cart" />
                  <input type="hidden" name="redir" value="{page_link}" />
                  <input type="hidden" name="product_id" value="{$p->Id}" />
                  {if $p->Endpreis > 0}
                      <a href="javascript: document.forms['to_basket_{$p->Id}'].submit();">
                        <i class="icon-basket size-xl" data-toggle="tooltip" title="{#Shop_toBasket#}"></i>
                      </a>
                  {/if}
                </form>
                <form class="d-inline" name="delete_item_{$p->Id}" method="post" action="index.php">
                  <input name="amount" type="hidden" value="{$p->Anzahl}" />
                  {foreach from=$p->Varianten item=x}
                      <input type="hidden" name="mod[]" value="{$x}" />
                  {/foreach}
                  <input type="hidden" name="p" value="shop" />
                  <input type="hidden" name="area" value="{$area}" />
                  <input type="hidden" name="action" value="delitem_mylist" />
                  <input type="hidden" name="redir" value="{page_link}" />
                  <input type="hidden" name="product_id" value="{$p->Id}" />
                  <a href="javascript: document.forms['delete_item_{$p->Id}'].submit();">
                    <i class="icon-cancel size-xl" data-toggle="tooltip" title="{#Delete#}"></i>
                  </a>
                </form>
              </div>
            </div>
        {/foreach}
      </div>

      <form method="post" action="index.php">
        <input type="hidden" name="p" value="shop" />
        <input type="hidden" name="area" value="{$area}" />
        <input type="hidden" name="action" value="new_list" />

        <div class="text-center">
          <input class="btn btn-primary btn-block-sm" type="submit" value="{#Shop_mylist_new#}" />
        </div>
      </form>
    </div>

    <div class="box-content">
      {if !$loggedin}
          {#Shop_mylist_save_notlogged#}
      {else}
          <form method="post" action="index.php">
            <input type="hidden" name="p" value="shop" />
            <input type="hidden" name="area" value="{$area}" />
            <input type="hidden" name="action" value="mylist" />
            <input type="hidden" name="subaction" value="save_list" />

            <div class="input-group">
              <input class="form-control" name="Name_Merkzettel" value="{#GlobalTitle#}" type="text" />
              <span class="input-group-append">
                <input class="btn btn-primary" value="{#Shop_mylist_save#}" type="submit" />
              </span>
            </div>
          </form>
      {/if}
    </div>
{/if}

<div class="h3 title">{#Shop_mylist_load#}</div>
<div class="box-content">
  {if !$loggedin}
      {#Shop_mylist_load_notlogged#}
  {else}
      {if $myLists}
          <div class="row row-header">
            <div class="col">{#Shop_mylist_list#}</div>
            <div class="col">{#Date#}</div>
            <div class="col">{#GlobalActions#}</div>
          </div>

          {foreach from=$myLists item=ml}
              <div class="row {cycle name='mylist' values='row-primary,row-secondary'}">
                <div class="col">
                  <a href="index.php?p=shop&amp;action=mylist&amp;subaction=load_list&amp;id={$ml->Id}">{$ml->Name|sanitize}</a>
                </div>
                <div class="col">{$ml->Datum|date_format:$lang.DateFormat}</div>
                <div class="col">
                  <a href="index.php?p=shop&amp;action=mylist&amp;subaction=del_list&amp;id={$ml->Id}">
                    <i class="icon-cancel" data-toggle="tooltip" title="{#Shop_mylist_dellist#}"></i>
                  </a>
                </div>
              </div>
          {/foreach}
      {else}
          {#Shop_mylist_nothing#}
      {/if}
  {/if}
</div>