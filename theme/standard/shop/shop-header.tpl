<section class="d-none d-sm-block p-2" style="background:#2e2f46cc;color:#999">
  <div class="container">
    <div class="row justify-content-center align-content-center">

{*         <div class="col-sm-3">
              <a href="/index.php?p=imprint" class="text-light"><h6 class="font-weight-bold"><i class="icon-location"></i>{$settings.Strasse}</h6></a> <span class="text-muted">{$settings.Stadt}</span>
        </div> *}

        <div class="col-sm-3">
              <span><i class="icon-mail"></i>
              написать</span><br>
              <a href="mailto:{$settings.Mail_Absender}" class="text-light"><b class="font-weight-bold">{$settings.Mail_Absender}</b></a>
        </div>

        <div class="col-sm-3 border-right border-secondary">
              <b>{$settings.Telefon}</b><br>
              <a href="#zvonok" data-toggle="modal" data-target="#zvonokModal" class="text-light"><i class="icon-phone"></i>Заказать звонок</a>
        </div>

        <div class="col-sm-3">
              <a class="text-light" href="{$baseurl}/index.php?p=shop&amp;action=mylist">{#Shop_mylist#}</a><br>
        {if $loggedin}
        <a class="text-light colorbox" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1">{#Shop_mergeListsMy#}</a>
        {/if}
        </div>

        <div class="col-sm-3">
          {if $loggedin}
           <a href="#" class="text-light" data-toggle="modal" data-target="#loginModal"><i class="icon-user"></i>{#Login#}</a>
          {else}
           <a href="#" class="text-light" data-toggle="modal" data-target="#loginModal"><i class="icon-user"></i> Вход</a>
          <span class="">|</span >
           <a class="text-light" href="/index.php?p=register&lang=ru&area=1" class="">Регистрация</a>
          {/if}
        </div>

  </div>
  </div>
</section>