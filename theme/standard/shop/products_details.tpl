<div class="box-content">

  {if intval($p.GewichtF) > 0}
      <div class="row spacer">
        <div class="col-3">{#Shop_ProdWeight#}</div>
        <div class="col-9">
          {$p.GewichtF} кв.м.
          {if !empty($p.GewichtRaw)}
              ({$p.GewichtRaw} {#Shop_ProdWeightRaw#})
          {/if}
        </div>
      </div>
  {/if}
  {if $p.Abmessungen}
      <div class="row spacer">
        <div class="col-3">{#Shop_ProdHBL#}</div>
        <div class="col-9">{$p.Abmessungen|sanitize}{#Shop_ProdHBLC#}</div>
      </div>
  {/if}
  {if !empty($p.EAN_Nr)}
      <div class="row spacer">
        <div class="col-3">EAN</div>
        <div class="col-9">{$p.EAN_Nr|sanitize}</div>
      </div>
  {/if}
  {if !empty($p.ISBN_Nr)}
      <div class="row spacer">
        <div class="col-3">{#ISBN_Code#}</div>
        <div class="col-9">{$p.ISBN_Nr|sanitize}</div>
      </div>
  {/if}
  {if $p.man->Id}
      <div class="row spacer">
        <div class="col-3">{#Manufacturer#}</div>
        <div class="col-9"><a href="index.php?p=shop&amp;action=showproducts&amp;man={$p.man->Id}">{$p.man->Name}</a></div>
      </div>
  {/if}
  {if !empty($p.PrCountry)}
      <div class="row spacer">
        <div class="col-3">{#PrCountry#}</div>
        <div class="col-9">{$p.PrCountry|sanitize}</div>
      </div>
  {/if}
  {if $det_spez.Spez_1 && $p.Spez_1}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_1|specialchars}</div>
        <div class="col-9">{$p.Spez_1|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_2 && $p.Spez_2}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_2|specialchars}</div>
        <div class="col-9">{$p.Spez_2|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_3 && $p.Spez_3}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_3|specialchars}</div>
        <div class="col-9">{$p.Spez_3|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_4 && $p.Spez_4}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_4|specialchars}</div>
        <div class="col-9">{$p.Spez_4|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_5 && $p.Spez_5}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_5|specialchars}</div>
        <div class="col-9">{$p.Spez_5|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_6 && $p.Spez_6}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_6|specialchars}</div>
        <div class="col-9">{$p.Spez_6|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_7 && $p.Spez_7}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_7|specialchars}</div>
        <div class="col-9">{$p.Spez_7|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_8 && $p.Spez_8}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_8|specialchars}</div>
        <div class="col-9">{$p.Spez_8|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_9 && $p.Spez_9}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_9|specialchars}</div>
        <div class="col-9">{$p.Spez_9|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_10 && $p.Spez_10}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_10|specialchars}</div>
        <div class="col-9">{$p.Spez_10|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_11 && $p.Spez_11}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_11|specialchars}</div>
        <div class="col-9">{$p.Spez_11|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_12 && $p.Spez_12}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_12|specialchars}</div>
        <div class="col-9">{$p.Spez_12|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_13 && $p.Spez_13}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_13|specialchars}</div>
        <div class="col-9">{$p.Spez_13|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_14 && $p.Spez_14}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_14|specialchars}</div>
        <div class="col-9">{$p.Spez_14|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $det_spez.Spez_15 && $p.Spez_15}
      <div class="row spacer">
        <div class="col-3">{$det_spez.Spez_15|specialchars}</div>
        <div class="col-9">{$p.Spez_15|cleantoentities|default:'-'|autowords}</div>
      </div>
  {/if}
  {if $shopsettings->Zeige_ErschienAm == 1}
      <div class="row spacer">
        <div class="col-3">{#Shop_releasedProductDetail#}</div>
        <div class="col-9">{$p.Erstellt|date_format: $lang.DateFormatSimple}</div>
      </div>
  {/if}
</div>
