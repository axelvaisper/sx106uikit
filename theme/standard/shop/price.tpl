<div class="h1 title">{#Shop_Preis#}</div>
<div class="box-content">

  <div class="row text-center font-weight-bold">
    <div class="col-2">{#Shop_ArticleNumber#}</div>
    <div class="col-8">{#GlobalTitle#}</div>
    <div class="col-2">{#Products_price#}</div>
  </div>

  {foreach from=$prais key=categ item=products}
      <div class="row row-header">
        <div class="col">{$categ}</div>
      </div>
      {foreach from=$products item=tovar}
          <div class="row {cycle values='row-primary,row-secondary'}">
            <div class="col-2">
              {$tovar->Artikelnummer|sanitize}
            </div>

            <div class="col-8">
              <a href="index.php?p=shop&amp;action=showproduct&amp;id={$tovar->Id}&amp;cid={$tovar->Kategorie}&amp;pname={$tovar->Titel|translit}">
                {$tovar->Titel|sanitize}
              </a>
              <div class="small text-truncate">
                {$tovar->Beschreibung|striptags|sanitize}
              </div>
            </div>

            <div class="col-2 text-right">
              {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                  {if $tovar->Preis_Liste > 0}
                      <span class="shop-price">{$tovar->Preis_Liste|numformat} {$currency_symbol}</span>
                  {else}
                      <span class="shop-price">{#Zvonite#}</span>
                  {/if}
              {else}
                  {#Shop_prices_justforUsers#}
              {/if}
            </div>
          </div>
      {/foreach}
  {/foreach}
</div>

{if !empty($pages)}
    <div class="wrapper">
      {$pages}
    </div>
{/if}
