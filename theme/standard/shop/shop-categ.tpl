<section class="header_area sticky" style="background:#2e2f46cc">
    <div class="main_header_area animated">
        <div class="container">
            <nav id="navigation2" class="navigation">

                <div class="nav-header">
                  <a href="/" class="nav-brand">
                  <div class="media">
                    <img class="mr-2" src="{$imgpath}/page/logo.png" alt="logo">
                    <div class="media-body text-center d-none d-sm-block">
                      <small>{$settings.Firma|sanitize}</small><br>
                      <b>{$settings.Seitenname|sanitize}</b>
                    </div>
                  </div>
                  </a>
                  <div class="nav-toggle"></div>
                </div>

                {include file="$incpath/shop/search_small.tpl"}

<div class="nav-menus-wrapper">
<ul class="nav-menu ">


<li class="ml-2 bg-info"><a class="" href="#">База недвижимости</a>
<div class="megamenu-panel nav-submenu">
    <div class="megamenu-lists">

  {assign var=cid value=$smarty.request.cid}
  {foreach from=$MyShopNavi item=sn}
  <ul class="megamenu-list list-col-5">
      <li class="megamenu-list-title">
        <a class="{if $cid == $sn->Id || in_array($sn->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sn->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sn->Entry|translit}">
          {$sn->Icon} {$sn->Entry|sanitize}
        </a></li>
        {if !empty($sn->Sub1) && count($sn->Sub1)}
               {foreach from=$sn->Sub1 item=sub1}
                    <li >
                      <a class="{if $cid == $sub1->Id || in_array($sub1->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub1->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub1->Entry|translit}">
                        {$sub1->Entry|sanitize}
                      </a></li>
                      {if !empty($sub1->Sub2) && count($sub1->Sub2)}
                              {foreach from=$sub1->Sub2 item=sub2}
                                  <li>
                                    <a class="{if $cid == $sub2->Id || in_array($sub2->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub2->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub2->Name|translit}">
                                      {$sub2->Name|sanitize}
                                    </a></li>
                                    {if !empty($sub2->Sub3) && count($sub2->Sub3)}
                                            {foreach from=$sub2->Sub3 item=sub3}
                                                <li>
                                                  <a class="{if $cid == $sub3->Id || in_array($sub3->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub3->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub3->Name|translit}">
                                                    {$sub3->Name|sanitize}
                                                  </a></li>

                                                  {if !empty($sub3->Sub4) && count($sub3->Sub4)}

                                                          {foreach from=$sub3->Sub4 item=sub4}
                                                              <li>
                                                                <a class="{if $cid == $sub4->Id || in_array($sub4->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub4->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub4->Name|translit}">
                                                                  {$sub4->Name|sanitize}
                                                                </a>
                                                              </li>
                                                          {/foreach}
                                                  {/if}
                                            {/foreach}
                                    {/if}
                              {/foreach}
                      {/if}
                {/foreach}
        {/if}
  </ul>
  {/foreach}

      </div>
  </div>
</li>

        {foreach from=$MyShopNavi item=sn}
            <li class="">
              <a class="{if $cid == $sn->Id || in_array($sn->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sn->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sn->Entry|translit}">
                {$sn->Icon} {$sn->Entry|sanitize}
              </a>

              {if !empty($sn->Sub1) && count($sn->Sub1)}
                    <ul class="nav-dropdown">
                      {foreach from=$sn->Sub1 item=sub1}
                          <li class="">
                            <a class="{if $cid == $sub1->Id || in_array($sub1->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub1->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub1->Entry|translit}">
                              {$sub1->Entry|sanitize}
                            </a>

                            {if !empty($sub1->Sub2) && count($sub1->Sub2)}
                                  <ul class="nav-dropdown">
                                    {foreach from=$sub1->Sub2 item=sub2}
                                        <li class="">
                                          <a class="{if $cid == $sub2->Id || in_array($sub2->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub2->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub2->Name|translit}">
                                            {$sub2->Name|sanitize}
                                          </a>

                                          {if !empty($sub2->Sub3) && count($sub2->Sub3)}
                                                <ul class="nav-dropdown">
                                                  {foreach from=$sub2->Sub3 item=sub3}
                                                      <li class="">
                                                        <a class="{if $cid == $sub3->Id || in_array($sub3->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub3->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub3->Name|translit}">
                                                          {$sub3->Name|sanitize}
                                                        </a>

                                                        {if !empty($sub3->Sub4) && count($sub3->Sub4)}
                                                              <ul class="nav-dropdown">
                                                                {foreach from=$sub3->Sub4 item=sub4}
                                                                    <li class="">
                                                                      <a class="{if $cid == $sub4->Id || in_array($sub4->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub4->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub4->Name|translit}">
                                                                        {$sub4->Name|sanitize}
                                                                      </a>
                                                                    </li>
                                                                {/foreach}
                                                              </ul>
                                                        {/if}
                                                      </li>
                                                  {/foreach}
                                                </ul>
                                          {/if}
                                        </li>
                                    {/foreach}
                                  </ul>
                            {/if}
                          </li>
                      {/foreach}
                    </ul>
              {/if}
            </li>
        {/foreach}


</ul>
                </div>
            </nav>
        </div>
    </div>
</section>