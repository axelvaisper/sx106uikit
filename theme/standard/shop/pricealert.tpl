<div class="wrapper">
    {#Shop_priceAlertInf#}
</div>

{if !empty($error)}
    <div class="uk-alert-warning">
        {foreach from=$error item=err}
            <div>{$err}</div>
        {/foreach}
    </div>
{/if}

<form method="post" action="#pricealert" class="uk-grid-small" uk-grid>

    <div class="uk-width-1-2@s">
        <input class="uk-input" id="alert-mail" name="pricealert_email" type="email" value="{$smarty.post.pricealert_email|sanitize}" required="required" placeholder="{#Email#}.." />
    </div>

    <div class="uk-width-1-2@s">
        <input class="uk-input" id="alert-price" name="pricealert_newprice" type="text" value="{$smarty.post.pricealert_newprice|sanitize}" required="required" placeholder="{#Shop_priceAlertYPrice#}.."/>
    </div>

    <div class="uk-width-1-1">
        <input class="uk-button uk-button-default uk-width-1-1" type="submit" value="{#ButtonSend#}" />
    </div>

    <input type="hidden" name="pricealert_send" value="1" />
    <input type="hidden" name="red" value="{$red}" />
</form>
