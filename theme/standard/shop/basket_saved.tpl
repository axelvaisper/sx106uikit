<script>
<!-- //
$(function () {
    $('#click-delall').on('click', function () {
        if (confirm('{#Shop_basket_delallC#}')) {
            $('#send-delall').submit();
        }
    });
});
//-->
</script>

<form id="send-delall" method="post" action="index.php?p=shop">
  <input type="hidden" name="action" value="delsavedbasket_all" />
  <input type="hidden" name="bid" value="{$sb->Id}" />
</form>

<div class="h1 title clearfix">
  {#Shop_savedBasketLink1#}
  <div class="float-right">
    <a id="click-delall" href="javascript:void(0);">
      <i class="icon-cancel" data-toggle="tooltip" title="{#Shop_basket_delall#}"></i>
    </a>
  </div>
</div>

{if $saved_found}
    {foreach from=$saved item=sb}
        <div class="box-content">
          <div class="h5 wrapper">
            {#Shop_savedBasketFrom#} {$sb->ZeitBis|date_format:"%d.%m.%y, %H:%M"}
          </div>

          <div class="row text-center">
            <div class="col-2">{#GlobalImage#}</div>
            <div class="col-2">{#Shop_f_artNr#}</div>
            <div class="col-3">{#Shop_variants_d#}</div>
            <div class="col-3">{#Konfiguration#}</div>
            <div class="col-2">{#Shop_amount#}</div>
          </div>

          <div class="wrapper">
            {foreach from=$sb->Positions item=p name=posit}
                <div class="spacer border-top">
                  <div class="row">
                    <div class="col-2 spacer border-right text-center">
                      <a href="{$p->ProdLink}">
                        <img class="img-fluid" src="{$p->Bild}" alt="" />
                      </a>
                    </div>

                    <div class="col-2 spacer border-right text-center">
                      <a class="small" href="{$p->ProdLink}">{$p->Titel|sanitize}</a>
                      <div>
                        {$p->Artikelnummer}
                      </div>
                    </div>

                    <div class="col-3 spacer border-right">
                      {if $p->Vars}
                          {foreach from=$p->Vars item=v}
                              <div>
                                {$v->KatName}: {$v->Name} ({$v->Wert|numformat} {$currency_symbol})
                              </div>
                          {/foreach}
                      {/if}
                    </div>

                    <div class="col-3 spacer border-right">{$p->FreeFields}</div>
                    <div class="col-2 spacer text-center">{$p->Anzahl}</div>
                  </div>
                </div>
            {/foreach}
          </div>

          <div class="form-inline justify-content-center">
            <form class="mx-1" method="post" action="index.php?p=shop"{if $basket_products_price > 0 || $basket_products_all >= 1} onsubmit="return confirm('{#Shop_savedBasketLoadC#}');"{/if}>
              <input type="hidden" name="action" value="loadsavedbasket" />
              <input type="hidden" name="bid" value="{$sb->Id}" />
              <input class="btn btn-primary" type="submit" value="{#Shop_savedBasketLoad#}" />
            </form>
            <form class="mx-1" method="post" action="index.php?p=shop" onsubmit="return confirm('{#Shop_savedBasketDel#}');">
              <input type="hidden" name="action" value="delsavedbasket" />
              <input type="hidden" name="bid" value="{$sb->Id}" />
              <input class="btn btn-secondary" type="submit" value="{#Delete#}" />
            </form>
          </div>
        </div>
    {/foreach}
{else}
    <div class="box-content">{#Shop_savedBasketNo#}</div>
{/if}
