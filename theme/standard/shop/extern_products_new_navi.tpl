{if $extern_products_array}
    <div class="wrapper panel">
      <div class="panel-head">{#Shop_newin#}</div>
      <div class="panel-main">
        {foreach from=$extern_products_array item=p name=pro}
            <div class="text-center">
              <div data-toggle="tooltip" title="{$p.Beschreibung|tooltip}">

                <a href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                  <img class="img-fluid" src="{$p.Bild_Mittel}" alt="" />
                </a>

                <div class="spacer text-truncate">
                  <a href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                    {$p.Titel|sanitize}
                  </a>
                </div>
              </div>

              <div class="wrapper">
                {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                    {if $p.Preis_Liste != $p.Preis}
                        <div>
                          {#Shop_instead#}
                          <span class="shop-price-old">{$p.Preis_Liste|numformat} {$currency_symbol}</span>
                        </div>
                    {/if}

                    {if !empty($p.Vars)}
                        {#Shop_priceFrom#}
                    {/if}

                    {if $p.Preis > 0}
                        <span class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
                        {if $no_nettodisplay != 1}
                            {if $price_onlynetto != 1}
                                <div class="small">
                                  {if $shopsettings->NettoKlein == 1}
                                      {#Shop_netto#} {$p.netto_price|numformat} {$currency_symbol}
                                  {/if}
                                </div>
                            {/if}
                            {if $price_onlynetto == 1 && !empty($p.price_ust_ex)}
                                <div class="small">
                                  {include file="$incpath/shop/tax_inf_small.tpl"}
                                </div>
                            {/if}
                        {/if}
                    {else}
                        <span class="shop-price">{#Zvonite#}</span>
                    {/if}
                {else}
                    {#Shop_prices_justforUsers#}
                {/if}
              </div>
            </div>
        {/foreach}
      </div>
    </div>
{/if}
