<div class="h1 title">{#Shop_personalDownloads#}</div>
{if !$exists}
    <div class="h4">{#Error#}</div>
    {#Shop_personalDownloadsE#}
{else}
    <div class="row font-weight-bold">
      <div class="col-1"><i class="icon-download"></i></div>
      <div class="col">{#Shop_mydownloads_filename#}</div>
      <div class="col">{#Description#}</div>
      <div class="col">{#GlobalSize#}</div>
      <div class="col">{#AddedOn#}</div>
    </div>
    {foreach from=$downloads item=d}
        <div class="row spacer">
          <div class="col-1">
            {if $d->not_exists != 1}
                <a href="index.php?p=misc&amp;do=mypersonaldownloads&amp;oid={$smarty.get.oid}&amp;dl=1&amp;id={$d->Id}">
                {/if}
                <i class="icon-download"></i>
                {if $d->not_exists != 1}
                </a>
            {/if}
          </div>
          <div class="col">
            {if $d->not_exists == 1}
                <span>{$d->Datei|slice:50:'...'}</span>
            {else}
                <a href="index.php?p=misc&amp;do=mypersonaldownloads&amp;oid={$smarty.get.oid}&amp;dl=1&amp;id={$d->Id}">{$d->Datei|slice:45:'...'}</a>
            {/if}
          </div>
          <div class="col">{$d->Beschreibung}</div>
          <div class="col">{$d->size}</div>
          <div class="col">{$d->Datum|date_format:'%d.%m.%Y'}</div>
        </div>
    {/foreach}
{/if}
