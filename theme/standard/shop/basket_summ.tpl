<div class="row text-nowrap">
  <div class="col">
    {if !$smarty.session.shopstep && $product_array}
        <div class="spacer">
          <button class="btn btn-primary" onclick="if (confirm('{#Shop_del_basketc#}'))location.href = 'index.php?d=1&amp;p=shop&amp;action=delbasket&amp;key=' + Math.random();">{#Shop_del_basket#}</button>
        </div>
    {/if}
  </div>

  <div class="col-auto">
    {if $show_vat_table == 1 && $shopsettings->NettoPreise != 1}
        <div class="row">
          <div class="col-3">{#Shop_f_summ_brutto#}:</div>
          <div class="col text-right">
            {$basket_products_brutto|numformat} {$currency_symbol}
          </div>
        </div>
    {/if}
    <div class="row">
      <div class="col">
        {if $show_vat_table == 1}
            {#Shop_f_summ_netto#}:
        {else}
            {#Shop_f_summ_brutto#}:
        {/if}
      </div>
      <div class="col text-right">
        {$basket_products_price_netto|numformat} {$currency_symbol}
      </div>
    </div>
    {if $smarty.session.print_coupon_price == 1 && $smarty.session.coupon_val>0}
        {if $smarty.session.coupon_typ == 'pro'}
            <div class="row">
              <div class="col">
                {#Shop_o_coupon#}: {$smarty.session.coupon_code}(- {$smarty.session.coupon_val|numformat}%)
              </div>
              <div class="col text-right">
                - {$smarty.session.price_netto_coupon|numformat} {$currency_symbol}
              </div>
            </div>
        {elseif $smarty.session.coupon_typ == 'wert'}
            <div class="row">
              <div class="col">
                {#Shop_o_coupon#}: {$smarty.session.coupon_code}
              </div>
              <div class="col text-right">
                - {$smarty.session.price_netto_coupon|numformat} {$currency_symbol}
              </div>
            </div>
        {/if}
        <div class="row">
          <div class="col">{#Shop_f_summ_current#}:</div>
          <div class="col text-right">
            {$smarty.session.price_netto_zwi|numformat} {$currency_symbol}
          </div>
        </div>
    {/if}
    {if $show_vat_table == 1}
        {foreach from=$ust_vals item=ust}
            {assign var=ust_code value=$ust->Wert}
            {if $smarty.session.$ust_code}
                <div class="row">
                  <div class="col">{#Shop_f_exclVat#} {$ust->Wert}%:</div>
                  <div class="col text-right">
                    {$smarty.session.$ust_code|numformat} {$currency_symbol}
                  </div>
                </div>
            {/if}
        {/foreach}
    {/if}
    {if $smarty.session.shipping_summ}
        <div class="row">
          <div class="col">{#Shop_shipping_cost#}:</div>
          <div class="col text-right">
            {$smarty.session.shipping_summ|numformat} {$currency_symbol}
          </div>
        </div>
    {/if}
    {if $smarty.session.payment_summ_extra}
        <div class="row">
          <div class="col">
            {if $smarty.session.payment_summ_mipu == 'zzgl'}
                {#Shop_f_excl_pm#}:
            {else}
                {#Shop_f_icl_pm#}:
            {/if}
          </div>
          <div class="col text-right">
            {$smarty.session.payment_summ_symbol} {$smarty.session.payment_summ_extra|numformat} {$currency_symbol}
          </div>
        </div>
    {/if}
    <div class="border-top">
      <div class="row wrapper">
        <div class="col h4">{#Shop_f_summ_ovall#}:</div>
        <div class="col h4 text-right">
          {$basket_products_price|numformat} {$currency_symbol}
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col">{#Shop_parts_weight#}:</div>
      <div class="col text-right">
        {$smarty.session.gewicht_detail|numformat} {#Shop_weights_unit#}
      </div>
    </div>

  </div>
</div>




