<script>
<!-- //
$(function() {
    $('.product_table').on('click', function() {
        $('#slide_' + $(this).attr('id')).slideToggle('slow');
    });

    var options = { target: '#ajaxbasket', timeout: 3000 };
    $('.ajax_products').submit(function() {
        var id = '#mylist_' + $(this).attr('id');
        if ($(id).val() == 1) {
            showModal($('#favorite-products'), 3000);
        } else {
           showModal($('#basket-products'), 10000);
        }
        $(this).ajaxSubmit(options);
        $(id).val(0);
        return false;
    });
});
//-->
</script>

{include file="$incpath/shop/notification.tpl" dialog='products'}

{assign var=shop_q value=$smarty.request.shop_q|urlencode|default:'empty'}

{if $shopsettings->TopNewOffersPos == 'top'}
    {include file="$incpath/shop/categ_tabs.tpl"}
{/if}

{if $smarty.request.s == 1}
    {* {include file="$incpath/shop/search_extended.tpl"} *}
{/if}

{if $cat_desc}
    {$cat_desc}
{/if}

<div class="h3 title">{#Shop_productOverview#}</div>
{if !$products}
    <div class="box-content">
      {if !empty($smarty.request.shop_q)}
          {#Shop_searchNull#}
      {else}
          {#Shop_noProducts#}
      {/if}
    </div>
{else}
    {include file="$incpath/shop/products_headernavi.tpl" position="top"}

    <div class="wrapper">
      {foreach from=$products item=p name=pro}
          <div class="product_table pointer" id="product_table_{$p.Id}">
              <div class="row no-gutters row-header border">
                <div class="col-2">{$p.Artikelnummer|sanitize}</div>
                <div class="col">{$p.Titel|sanitize}</div>
                <div class="col-2 text-right">
                  {if $p.Preis > 0}
                      <span class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
                  {else}
                      <span class="shop-price">{#Zvonite#}</span>
                  {/if}
                </div>
              </div>
          </div>

          <div id="slide_product_table_{$p.Id}" style="display: none">
            <div class="box-content">
              <form method="post" name="products_{$p.Id}" id="ajax_{$p.Id}" class="ajax_products" action="{if empty($p.Vars)}index.php?p=shop&amp;area={$area}{else}index.php?p=shop&amp;area={$area}&amp;action=showproduct&amp;id={$p.Id}{/if}">

                <div class="row" id="prod_anchor_{$p.Id}">
                  <div class="col-2 d-flex flex-column align-items-center">

                    <div class="spacer">
                      {if $shopsettings->popup_product == 1}
                          <a class="colorbox" href="{$p.ProdLink}&amp;blanc=1">
                            <img class="img-fluid" src="{$p.Bild_Mittel}" alt="{$p.Titel|sanitize}" />
                          </a>
                      {else}
                          <a href="{$p.ProdLink}">
                            <img class="img-fluid" src="{$p.Bild_Mittel}" alt="{$p.Titel|sanitize}" />
                          </a>
                      {/if}
                    </div>

                    {if $p.Fsk18 == 1}
                        <img class="spacer img-fluid" src="{$imgpath_page}special.png" alt="{#Shop_isFSKWarning#}" />
                        <div class="spacer small">
                          {#Shop_isFSKWarning#}
                        </div>
                    {/if}
                  </div>

                  <div class="col-10">
                    <h4 class="text-truncate">
                      {if $shopsettings->popup_product == 1}
                          <a class="colorbox" href="{$p.ProdLink}&amp;blanc=1">{$p.Titel|sanitize}</a>
                      {else}
                          <a href="{$p.ProdLink}">{$p.Titel|sanitize}</a>
                      {/if}
                    </h4>

                    {if $shopsettings->Zeige_Text == 1 && !empty($p.Beschreibung)}
                        <div class="wrapper">
                          {$p.Beschreibung|striptags|truncate:$shopsettings->Prodtext_Laenge|sanitize}
                        </div>
                    {/if}

                    <div class="wrapper">
                      {if $shopsettings->Zeige_Verfuegbarkeit == 1 || $shopsettings->Zeige_Lagerbestand == 1 || $shopsettings->Zeige_Lieferzeit == 1 || $shopsettings->Zeige_ArtNr == 1 || $shopsettings->Zeige_Hersteller == 1 || $shopsettings->Zeige_ErschienAm == 1}
                          {if $shopsettings->Zeige_Verfuegbarkeit == 1}
                              <div class="row">
                                <div class="col-3">{#Shop_Availablility#}</div>
                                <div class="col">{$p.VIcon}</div>
                              </div>
                          {/if}
                          {if $p.Lieferzeit && $p.Lagerbestand>0}
                              {if $shopsettings->Zeige_Lagerbestand == 1}
                                  <div class="row">
                                    <div class="col-3">{#Shop_av_store#}</div>
                                    <div class="col">{#Shop_av_storeAv#} {$p.Lagerbestand}</div>
                                  </div>
                              {/if}
                              {if $shopsettings->Zeige_Lieferzeit == 1}
                                  <div class="row">
                                    <div class="col-3">{#Shop_shipping_timeinf#}</div>
                                    <div class="col">{$p.Lieferzeit|sanitize}</div>
                                  </div>
                              {/if}
                          {/if}
                          {if $shopsettings->Zeige_ArtNr == 1}
                              <div class="row">
                                <div class="col-3">{#Shop_ArticleNumber#}</div>
                                <div class="col">{$p.Artikelnummer}</div>
                              </div>
                          {/if}
                          {if $p.man->Id >= 1 && $shopsettings->Zeige_Hersteller == 1}
                              <div class="row">
                                <div class="col-3">{#Manufacturer#}</div>
                                <div class="col">
                                  <a href="index.php?p=shop&amp;action=showproducts&amp;man={$p.man->Id}">{$p.man->Name}</a>
                                </div>
                              </div>
                          {/if}
                          {if $shopsettings->Zeige_ErschienAm == 1}
                              <div class="row">
                                <div class="col-3">{#Added#}</div>
                                <div class="col">{$p.Erstellt|date_format:$lang.DateFormatSimple}</div>
                              </div>
                          {/if}
                      {/if}
                    </div>
                    {if $p.shipping_free == 1}
                        <div class="small spacer">{#Shop_freeshipping#}</div>
                    {/if}
                    {if $p.diffpro > 0}
                        <div class="small spacer">{#Shop_Billiger#}{$p.diffpro|numformat}%</div>
                    {/if}

                    <div class="d-flex justify-content-between border-bottom">
                      <div class="spacer">
                        {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                            {if $p.Preis_Liste != $p.Preis}
                                {#Shop_instead#}
                                <span class="shop-price-old">{$p.Preis_Liste|numformat} {$currency_symbol}</span>
                            {/if}
                            {if !empty($p.Vars)}
                                {#Shop_priceFrom#}
                            {/if}
                            {if $p.Preis > 0}
                                <span class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
                            {else}
                                <span class="shop-price">{#Zvonite#}</span>
                            {/if}
                        {else}
                            {#Shop_prices_justforUsers#}
                        {/if}
                      </div>

                      <div class="spacer">
                        {if empty($p.Vars) && $p.Lagerbestand > 0 && $p.Preis > 0 && empty($p.Frei_1) && empty($p.Frei_2) && empty($p.Frei_3)}
                            {if $p.Fsk18 == 1 && $fsk_user != 1}
                            {else}
                                {#Shop_amount#}
                                <input name="amount" type="text" style="width: 40px" value="1" maxlength="3" />
                            {/if}
                        {/if}
                      </div>
                    </div>

                    {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                        <div class="spacer text-right">
                          {if $p.Fsk18 == 1 && $fsk_user != 1}
                              {if $shopsettings->popup_product == 1}
                                  <button class="btn btn-secondary" type="button" onclick="newWindow('{$p.ProdLink}&amp;blanc=1', '90%', '97%');">
                                    {#buttonDetails#}
                                  </button>
                              {else}
                                  <button class="btn btn-secondary" type="button" onclick="location.href = '{$p.ProdLink}';">
                                    {#buttonDetails#}
                                  </button>
                              {/if}
                          {else}
                              {if empty($p.Vars) && $p.Lagerbestand > 0 && $p.Preis > 0 && empty($p.Frei_1) && empty($p.Frei_2) && empty($p.Frei_3)}
                                  <input type="hidden" name="action" value="to_cart" />
                                  <input type="hidden" name="redir" value="{page_link}#prod_anchor_{$p.Id}" />
                                  <input type="hidden" name="product_id" value="{$p.Id}" />
                                  <input type="hidden" name="mylist" id="mylist_ajax_{$p.Id}" value="0" />
                                  <input type="hidden" name="ajax" value="1" />
                                  <noscript>
                                  <input type="hidden" name="ajax" value="0" />
                                  </noscript>
                                  <button class="btn btn-primary" type="submit">
                                    {#Shop_toBasket#}
                                  </button>
                                  <button class="btn btn-secondary" onclick="document.getElementById('mylist_ajax_{$p.Id}').value = '1';" type="submit">
                                    {#Shop_WishList#}
                                  </button>
                              {else}
                                  <input type="hidden" name="parent" value="{$p.Parent}" />
                                  <input type="hidden" name="navop" value="{$p.Navop}" />
                                  <input type="hidden" name="cid" value="{$p.Kategorie}" />
                                  {if $shopsettings->popup_product == 1}
                                      <button class="btn btn-primary" type="button" onclick="newWindow('{$p.ProdLink}&amp;blanc=1', '90%', '97%');">
                                        {#buttonDetails#}
                                      </button>
                                  {else}
                                      <button class="btn btn-primary" type="button" onclick="location.href = '{$p.ProdLink}';">
                                        {#buttonDetails#}
                                      </button>
                                  {/if}
                              {/if}
                          {/if}
                        </div>
                    {/if}
                  </div>
                </div>

              </form>
            </div>
          </div>
      {/foreach}
    </div>

    {include file="$incpath/shop/products_headernavi.tpl" position="bottom"}

    {if $shopsettings->TopNewOffersPos == 'bottom'}
        {include file="$incpath/shop/categ_tabs.tpl"}
    {/if}
{/if}

{if $smarty.request.s != '1'}
    {include file="$incpath/shop/products_navi_bottom.tpl"}
{/if}

{if $shopsettings->seen_cat == 1}
    {$small_seen_products}
{/if}

{if $shopsettings->vat_info_cat == 1}
    {include file="$incpath/shop/vat_info.tpl"}
{/if}
