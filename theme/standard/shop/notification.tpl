{if empty($dialog)}
    {assign var=dialog value='default'}
{/if}

<script>
<!-- //
$(function() {
    $('#to-basket-{$dialog}').on('click', function() {
        {if isset($smarty.request.blanc) && $smarty.request.blanc == 1}parent.{/if}document.location = 'index.php?action=showbasket&p=shop';
    });
});
//-->
</script>


{* <a class="uk-button uk-button-default" href="#modal-center" uk-toggle>Open</a> *}


<div class="uk-flex-top" uk-modal id="basket-{$dialog}" tabindex="-1" role="dialog" aria-labelledby="basket-title-{$dialog}" aria-hidden="true">
<div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

  <button class="uk-modal-close-default" type="button" uk-close></button>

  <div class="uk-modal-header" id="basket-title-{$dialog}">
      <h2 class="uk-modal-title">{#Notification#}</h2>
  </div>

  <div class="uk-modal-body">{#Shop_ProdAddedToBasket#}</div>

  <div class="uk-modal-footer">
    <button class="uk-button uk-button-default uk-modal-close" type="button">{#WinClose#}</button>
    <button type="button" class="uk-button uk-button-primary" id="to-basket-{$dialog}">{#Shop_go_basket#}</button>
  </div>

</div>
</div>
