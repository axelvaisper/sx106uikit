{include file="$incpath/shop/steps.tpl"}

{if $sx_error}
    <div class="box-content">
      <div class="box-error">
        <div class="font-weight-bold">{#Error#}</div>
        {if $sx_error == 'to_much'}
            {#Shop_basket_summ_tohigh#} {$best_max|numformat} {$currency_symbol}.
        {else}
            {#Shop_basket_summ_tolow#} {$best_min|numformat} {$currency_symbol}.
        {/if}
      </div>
    </div>
{else}
    {if $error == 1}
        <div class="h3 title">{#Shop_step_3#}</div>
        <div class="box-content">
          <div class="box-error">
            {#Shop_noshipper_thisweight#}
          </div>
        </div>
    {else}
        <form method="post" action="index.php" name="refresh">
          <input type="hidden" name="p" value="shop" />
          <input type="hidden" name="area" value="{$area}" />
          <input type="hidden" name="action" value="shoporder" />
          <input type="hidden" name="subaction" value="step3" />
          {if $smarty.request.order == "guest"}
              <input type="hidden" name="order" value="guest" />
          {/if}
          <input type="hidden" name="versand_id" id="hvid" value="" />
        </form>

        <form method="post" name="shipper_selection" action="index.php">
          <div class="h3 title">{#Shop_payment_methods#}</div>
          <div class="box-content">
            {if $smarty.session.ship_ok == 1}
                {if $shipper_found == 1}
                    {if !$shipper}
                        {#Shop_noshipper_thisweight#}
                    {else}
<script>
<!-- //
function refresh_method(VID) {
    document.getElementById('hvid').value = VID;
    document.forms['refresh'].submit();
}
//-->
</script>

                        {foreach from=$shipper item=s name="slister"}
                            {if $smarty.session.ship_ok == 1 || $s->Id == 1 || $s->Id == 2}
                                <div class="wrapper">
                                  <div class="row-header">
                                    <div class="form-check">
                                      <input class="form-check-input" onclick="refresh_method('{$s->Id}');" type="radio" name="versand_id" id="versand_id_{$s->Id}" value="{$s->Id}" {if !isset($smarty.request.versand_id)}{if $smarty.foreach.slister.first}checked="checked"{/if}{else}{if $smarty.request.versand_id == $s->Id}checked="checked"{/if}{/if} />
                                      <label class="form-check-label" for="versand_id_{$s->Id}">{$s->Name}</label>
                                    </div>
                                  </div>

                                  {if !empty($s->Icon) || !empty($s->Beschreibung)}
                                      <div class="row-primary">
                                        {if !empty($s->Icon)}
                                            <div class="spacer">
                                              <img class="img-fluid" src="uploads/shop/shipper_icons/{$s->Icon}" alt="" />
                                            </div>
                                        {/if}

                                        {if !empty($s->Beschreibung)}
                                            {$s->Beschreibung}
                                        {/if}
                                      </div>
                                  {/if}
                                </div>
                            {/if}
                        {/foreach}

                        <noscript>
                        <div class="text-center">
                          <input class="btn btn-primary" type="submit" value="{#Shop_referesh_shipper#}" />
                        </div>
                        </noscript>

                        {if $smarty.session.ship_ok == 1}
                            <div class="text-right">
                              {#Shop_shipper_costs#}
                              {if $shipping_summ}
                                  {$shipping_summ|numformat} {$currency_symbol}
                              {else}
                                  {$shipperInf->Gebuehr|numformat} {$currency_symbol}
                              {/if}
                            </div>
                        {/if}
                    {/if}
                {else}
                    {#Shop_noshipper_thisCountry#}
                {/if}
            {/if}
          </div>

          <div class="h3 title">{#Shop_payment_methods#}</div>
          <div class="box-content" id="payments">
            {if $Payments}
                {foreach name=payment from=$Payments item=pm}
                    {if $pm->Id != 1 || $smarty.session.type_client != 1}
                        <div class="wrapper">
                          <div class="row-header" id="vid{$pm->Id}">
                            <div class="form-check">
                              {if $pm->MaxWert > 0 && $smarty.session.price > $pm->MaxWert}
                                  <input class="form-check-input" type="radio" name="payment_id" value="123456789" id="pid_12345" disabled="disabled" />
                              {else}
                                  <input class="form-check-input" type="radio" name="payment_id" value="{$pm->Id}" id="pid_{$pm->Id}" {if $smarty.request.payment_id == $pm->Id}checked="checked"{else}{if $smarty.foreach.payment.first}checked="checked"{/if}{/if} />
                              {/if}

                              <label class="form-check-label" for="pid_{$pm->Id}">
                                {$pm->Name}
                                {if $pm->MaxWert > 0 && $smarty.session.price > $pm->MaxWert}
                                    {if $pm->Kosten != '0.00'}
                                        {if $pm->KostenTyp == 'pro'}
                                            {$pm->KostenOperant} {$pm->Kosten}%
                                        {else}
                                            {if $pm->KostenOperant == '+'}
                                                {#Shop_f_zzgl#}
                                            {else}
                                                -
                                            {/if}
                                            {$pm->Kosten|numformat}
                                            {$currency_symbol}
                                        {/if}
                                    {/if}
                                {else}
                                    {if $pm->Kosten != '0.00'}
                                        {if $pm->KostenTyp == 'pro'}
                                            {$pm->KostenOperant} {$pm->Kosten}%
                                        {else}
                                            {if $pm->KostenOperant == '+'}
                                                {#Shop_f_zzgl#}
                                            {else}
                                                -
                                            {/if}
                                            {$pm->Kosten|numformat} {$currency_symbol}
                                        {/if}
                                    {/if}
                                {/if}
                              </label>
                            </div>
                          </div>

                          <div class="row-primary">
                            {if $pm->MaxWert > 0 && $smarty.session.price > $pm->MaxWert}
                                <div class="box-error">
                                  {assign var=lockedMsg value=$lang.Shop_paymentmethod_locked|replace: '__WERT__': $pm->MaxWert}
                                  {$lockedMsg|replace: '__WAEHRUNG__': $currency_symbol}
                                </div>
                            {/if}

                            {if !empty($pm->Icon)}
                                <div class="spacer">
                                  <img class="img-fluid" src="uploads/shop/payment_icons/{$pm->Icon}" alt="" />
                                </div>
                            {/if}

                            {if !empty($pm->Beschreibung)}
                                <div class="spacer">
                                  {$pm->Beschreibung}
                                </div>
                            {/if}

                            <a class="colorbox-sm" href="index.php?p=misc&amp;do=payment_info&amp;id={$pm->Id}">{#Shop_za_in#}</a>
                          </div>
                        </div>
                    {/if}
                {/foreach}
            {else}
                {if $shipper_found && $shipper}
                    {#Shop_noPayment#}
                {/if}
            {/if}
          </div>

          {if $Payments}
              <input type="hidden" name="p" value="shop" />
              <input type="hidden" name="area" value="{$area}" />
              <input type="hidden" name="action" value="shoporder" />
              <input type="hidden" name="subaction" value="step4" />
              {if $smarty.request.order == 'guest'}
                  <input type="hidden" name="order" value="guest" />
              {/if}
              <div class="text-center">
                <input class="btn btn-primary" type="submit" value="{#Shop_nextStep#}" />
              </div>
          {/if}
        </form>
    {/if}
{/if}
