<section id="map2" style="width: 100%; height: 400px"></section>

{script file="//api-maps.yandex.ru/2.1/?lang=ru_RU" position='head' priority='800'}
<script>
    ymaps.ready(init);
    function init(){
        var address = '{$p.PrCountry|sanitize}';
        var geocoder = ymaps.geocode(address);
        geocoder.then(
                function (res) {
                    var coordinates = res.geoObjects.get(0).geometry.getCoordinates();
                    var map2 = new ymaps.Map("map2", {
                        center: coordinates,
                        zoom: 15,
                        controls: [
                            'typeSelector',
                            'zoomControl'
                        ]
                    });
                    map2.behaviors.disable('scrollZoom');
                    // Подробнее про метки здесь https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Placemark-docpage/
                    map2.geoObjects.add(new ymaps.Placemark(
                            coordinates,
                            // объект с данными метки
                            // https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Placemark-docpage/#param-properties
                            // {
                            //   'hintContent': '{$settings.Stadt}',
                            // },
                    ));
                }
        );
    }
</script>