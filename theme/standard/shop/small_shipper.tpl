{if !empty($ShipperAll)}
    <div class="wrapper panel">
      <div class="panel-head">{#Shop_shipping_method#}</div>
      <div class="panel-main text-center">
        {foreach from=$ShipperAll item=SA}
            <div class="wrapper">
              <img class="img-fluid" data-toggle="tooltip" title="{$SA->Text|truncate:100}" src="{$baseurl}/uploads/shop/shipper_icons/{$SA->Icon}" alt="" />
            </div>
        {/foreach}
      </div>
    </div>
{/if}
