<!--shop/product_amount_submit.tpl-->
{if $not_on_store == 1}
    <h5>{$p.VMsg|sanitize}</h5>
{else}

        {* {if $p.EinzelBestellung != 1}
            {if $p.MinBestellung != 0}
                <div>
                  {#Shop_min_order#} {$p.MinBestellung}
                </div>
            {/if}
            {if $p.MaxBestellung != 0}
                {#Shop_max_order#} {$p.MaxBestellung}
            {else}
                {if $p.MaxBestellung == 0 && $p.MinBestellung == 0}
                    {#Shop_amount#}
                {/if}
            {/if}
        {/if} *}
        <input type="hidden" name="ajax" value="1" />
        <noscript>
        <input type="hidden" name="ajax" value="0" />
        </noscript>


        {* {if $p.EinzelBestellung == 1}
            <input class="form-control" name="dis_amount" type="text" value="1" maxlength="1" disabled="disabled" />
        {else}
            <input class="form-control" {if $not_possible_to_buy == 1}disabled="disabled"{/if} name="amount" type="text" value="{if $p.MinBestellung != 0}{$p.MinBestellung}{else}1{/if}" />
        {/if} *}


        {if $not_on_store != 1}
             {if $p.Preis > 0}
                {* <button {if $not_possible_to_buy == 1}disabled="disabled"{/if} class="btn btn-primary" type="submit">
                  {#Shop_toBasket#}
                </button> *}
            {/if}
            {* <button {if $not_possible_to_buy == 1}disabled="disabled"{/if} class="uk-button uk-button-default" onclick="document.getElementById('to_mylist').value = '1';" type="submit">
              {#Shop_WishList#}
            </button> *}
            <input type="hidden" name="mylist" id="to_mylist" value="0" />
        {/if}

    {if $not_possible_to_buy != 1}
        <input type="hidden" name="action" value="to_cart" />
        <input type="hidden" name="redir" value="{page_link|urldecode}" />
        <input type="hidden" name="product_id" value="{$p.Id}" />
    {/if}
{/if}
<!--end shop/product_amount_submit.tpl-->