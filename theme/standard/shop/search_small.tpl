{script file="$jspath/jsuggest.js" position='head'}
<script>
<!-- //
$(function () {
    $('#qs').suggest('{$baseurl}/lib/ajax.php?action=shop&key=' + Math.random(), {
        onSelect: function () {
            document.forms['shopssform'].submit();
        }
    });
});
//-->
</script>

<form method="post" id="shopssform" name="shopssform" action="{$shop_search_small_action}" class="uk-search uk-search-navbar uk-width-1-1">
  <input type="hidden" name="cid" value="0" />
  <input type="hidden" name="man" value="0" />
  <input name="shop_q" id="qs" value="{if  isset($smarty.request.shop_q) && $smarty.request.shop_q != 'empty'}{$smarty.request.shop_q|escape:html}{/if}" class="uk-search-input" type="search" placeholder="{#Search#}.." autofocus/>
  <input type="hidden" name="p" value="shop" />
  <input type="hidden" name="action" value="showproducts" />
</form>
<ul class="ac_results" style=""></ul>