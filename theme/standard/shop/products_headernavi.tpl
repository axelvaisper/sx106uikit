{if empty($no_colums)}
{* <script>
<!-- //
$(function () {
    $('#avail-click-{$position}').on('click', function (e) {
        $('#avail-popup-{$position}').slideToggle(300);
        e.stopPropagation();
    });
    $(document).on('click', function () {
        $('#avail-popup-{$position}').slideUp(300);
    });
});
//-->
</script> *}

{assign var=shop_q value=$smarty.request.shop_q|urlencode|default:'empty'}
{assign var=avail value=''}
{if !empty($smarty.request.avail)}
    {assign var=avail value='&amp;avail='|cat:$smarty.request.avail}
{/if}

{if $position == 'top'}
    <ul class="uk-flex-center uk-subnav uk-subnav-divider">
      {if !isset($smarty.request.action) || $smarty.request.action != 'start'}
          {#SortBy#}:
          <li><a href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$title_sort}&amp;s={$smarty.request.s|default:'0'}{$avail}">
            {#SortName#} <i uk-icon="icon:{$img_title_sort|default:'sort'};ratio:.035"></i>
          </a></li>
          {* <a href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$art_sort}&amp;s={$smarty.request.s|default:'0'}{$avail}">
            {#SortArtikel#}<i uk-icon="{$img_art_sort|default:"sort"}"></i>
          </a></li> *}
          <li><a href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$price_sort}&amp;s={$smarty.request.s|default:'0'}{$avail}">
            {#Shop_sortPrice#} <i uk-icon="icon:{$img_price_sort|default:"icon-sort"};ratio:.035"></i>
          </a></li>
          <li><a href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$date_sort}&amp;s={$smarty.request.s|default:'0'}{$avail}">
            {#SortDate#} <i uk-icon="icon:{$img_date_sort|default:"sort"};ratio:.035"></i>
          </a></li>
          <li><a href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$klick_sort}&amp;s={$smarty.request.s|default:'0'}{$avail}">
            {#SortKlicks#} <i uk-icon="icon:{$img_klick_sort|default:"sort"};ratio:.035"></i>
          </a></li>
      {/if}

      {* <div class="shop-avail-cont">
        <img id="avail-click-{$position}" src="{$imgpath}/shop/avail-{$smarty.request.avail|default:'0'}.png" alt="" data-toggle="tooltip" title="{#ShopShowLegend#}" />
        <div class="shop-avail-popup" id="avail-popup-{$position}">
          <li><a data-toggle="tooltip" title="{#ShopAllProducts#}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:'20'}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}&amp;avail=0"><img src="{$imgpath}/shop/avail-0.png" alt="" /></a>
          <li><a data-toggle="tooltip" title="{$available_array.0->Name|tooltip}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:'20'}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}&amp;avail=1"><img src="{$imgpath}/shop/avail-1.png" alt="" /></a>
            {if $shopsettings->AvailType == 1}
            <li><a data-toggle="tooltip" title="{$available_array.4->Name|tooltip}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:'20'}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}&amp;avail=5"><img src="{$imgpath}/shop/avail-5.png" alt="" /></a>
            <li><a data-toggle="tooltip" title="{$available_array.1->Name|tooltip}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:'20'}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}&amp;avail=2"><img src="{$imgpath}/shop/avail-2.png" alt="" /></a>
            <li><a data-toggle="tooltip" title="{$available_array.2->Name|tooltip}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:'20'}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}&amp;avail=3"><img src="{$imgpath}/shop/avail-3.png" alt="" /></a>
            <li><a data-toggle="tooltip" title="{$available_array.3->Name|tooltip}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:'20'}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}&amp;avail=4"><img src="{$imgpath}/shop/avail-4.png" alt="" /></a>
            {/if}
        </div>
      </div> *}
    </ul>
{/if}

{if $position == 'bottom'}
  <ul class="uk-flex-center uk-subnav uk-subnav-divider">
      <li>{#DataRecords#}:</li>
      <li class="{if $smarty.request.limit == 6}uk-active{else}{/if}">
        <a href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=6&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}{$avail}">6</a>
      </li>
      <li class="{if $smarty.request.limit == 10}uk-active{else}{/if}">
        <a  href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=10&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}{$avail}">10</a>
      </li>
      <li class="{if $smarty.request.limit == 20 || empty($smarty.request.limit)}uk-active{else}{/if}">
        <a href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=20&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}{$avail}">20</a>
      </li>
      <li class="{if $smarty.request.limit == 50}uk-active{else}{/if}">
        <a href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=50&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}{$avail}">50</a>
      </li>
      <li class="{if $smarty.request.limit == 100}uk-active{else}{/if}">
        <a href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=100&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}{$avail}">100</a>
      </li>
      <li class="{if $smarty.request.limit == 200}uk-active{else}{/if}">
        <a href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=200&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}{$avail}">200</a>
      </li>
    </ul>

    {if !empty($pages)}{$pages}{/if}
{/if}
{/if}
