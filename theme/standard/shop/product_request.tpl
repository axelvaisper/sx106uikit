{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file = "$incpath/other/jsvalidate.tpl"}
$(function () {
    $('#product_request').validate({
        rules: {
            product_request_email: { required: true, email: true },
            product_request_name:  { required: false },
            product_request_text:  { required: true, minlength: 10 }
            },
            submitHandler: function () {
            document.forms['product_request'].submit();
        }
    });
});
//-->
</script>


{if isset($msg_send) && $msg_send == 1}
    {#Shop_prod_request_thankyou#}
{else}
    {if !empty($error)}
    <div class="box-error">
        {foreach from=$error item=err}
        <div>{$err}</div>
        {/foreach}
    </div>
    {/if}

    <form name="product_request" id="product_request" method="post" action="{page_link}#product_request">

    <div class="uk-grid-small" uk-grid>

        <div class="uk-width-1-2@s">
          <input class="uk-input" id="request-email" name="product_request_email" value="{$smarty.request.product_request_email|default:$smarty.session.login_email|sanitize}" type="email" placeholder="{#SendEmail_Email#}.." />
        </div>

        <div class="uk-width-1-2@s">
          <input class="uk-input" id="request-name" name="product_request_name" value="{$smarty.request.product_request_name|default:$whole_name|sanitize}" type="text" placeholder="{#Contact_myName#}.." />
        </div>

        <div class="uk-width-1-1">
          <textarea class="uk-input" id="request-text" name="product_request_text" rows="3" placeholder="{#GlobalMessage#}..">{$smarty.request.product_request_text|sanitize}</textarea>
        </div>

        {include file="$incpath/other/captcha.tpl"}

        <div class="uk-width-1-1">
          <input class="uk-button uk-button-default uk-width-1-1 " type="submit" value="{#ButtonSend#}" />
        </div>

    </div>

    <input type="hidden" name="subaction" value="product_request" />
    <input type="hidden" name="id" value="{$smarty.request.id}" />
    <input type="hidden" name="cid" value="{$smarty.request.cid}" />
    <input type="hidden" name="red" value="{$red}" />
    <input type="hidden" name="sub" value="product_request" />
    <input type="hidden" name="prod_name" value="{$p.Titel|sanitize}" />
    </form>
{/if}