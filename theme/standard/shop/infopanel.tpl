<div class="uk-card uk-card-small uk-card-default uk-margin-small-bottom">
  <div class="uk-card-body">
    <h6 class="uk-card-title uk-margin-small-bottom">{#Info#}</h6>
    <ul class="uk-nav">
      <li><a href="index.php?p=shop&amp;action=showproducts&amp;page=1&amp;topseller=1{if !empty($smarty.request.cid)}&amp;cid={$smarty.request.cid}{/if}&amp;limit={$smarty.request.limit|default:$plim}">{#Shop_Topseller#}</a></li>
      <li><a href="index.php?p=shop&amp;action=prais">{#Shop_Preis#}</a></li>
    </ul>
  </div>
</div>
