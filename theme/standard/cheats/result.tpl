<div class="wrapper">
  <h1 class="title">{#Gaming_cheats#}</h1>
  {include file="$incpath/cheats/search.tpl"}
  {if empty($Entries)}
      <div class="box-content">{#Links_SNotFound#}</div>
  {/if}
  {$Results}
</div>
