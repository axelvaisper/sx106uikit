<i class="square-icon icon-bold" data-toggle="tooltip" title="{$lang.Format_Tip_Bold}" onclick="addCode('b');"></i>
<i class="square-icon icon-italic" data-toggle="tooltip" title="{$lang.Format_Tip_Italic}" onclick="addCode('i');"></i>
<i class="square-icon icon-underline" data-toggle="tooltip" title="{$lang.Format_Tip_Underline}" onclick="addCode('u');"></i>
<i class="square-icon icon-strike" data-toggle="tooltip" title="{$lang.Format_Tip_Line}" onclick="addCode('s');"></i>
<i class="square-icon icon-align-center" data-toggle="tooltip" title="{$lang.Format_Tip_Center}" onclick="addCode('center');"></i>
<i class="square-icon icon-align-right" data-toggle="tooltip" title="{$lang.Format_Tip_Right}" onclick="addCode('right');"></i>
<i class="square-icon icon-align-left" data-toggle="tooltip" title="{$lang.Format_Tip_Left}" onclick="addCode('left');"></i>
<i class="square-icon icon-align-justify" data-toggle="tooltip" title="{$lang.Format_Tip_Justify}" onclick="addCode('justify');"></i>
<i class="square-icon icon-list" data-toggle="tooltip" title="{$lang.Format_Tip_List}" onclick="addCode('list');"></i>
<i class="square-icon icon-lightbulb" data-toggle="tooltip" title="{$lang.Format_Tip_High}" onclick="addCode('highlight');"></i>
{if $settings.SysCode_Links == 1}
    <i class="square-icon icon-link" data-toggle="tooltip" title="{$lang.Format_Tip_Url}" onclick="addCode('url');"></i>
{/if}
{if $settings.SysCode_Email == 1}
    <i class="square-icon icon-mail" data-toggle="tooltip" title="{$lang.Format_Tip_Email}" onclick="addCode('mail');"></i>
{/if}
{if $settings.SysCode_Bild == 1}
    <i class="square-icon icon-picture" data-toggle="tooltip" title="{$lang.Format_Tip_Image}" onclick="addCode('img');"></i>
{/if}
<i class="square-icon icon-quote-right" data-toggle="tooltip" title="{$lang.Format_Tip_Quote}" onclick="addCode('quote');"></i>
<i class="square-icon icon-code" data-toggle="tooltip" title="{$lang.Format_Tip_Code}" onclick="addCode('code');"></i>
<i class="square-icon icon-codeopen" data-toggle="tooltip" title="{$lang.Format_Tip_Php}" onclick="addCode('php');"></i>
<i class="square-icon icon-cancel" data-toggle="tooltip" title="{$lang.Format_Tip_CloseAll}" onclick="closeCodes();"></i>
