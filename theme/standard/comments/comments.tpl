<a id="comments"></a>
{if $comments == 1}
{if $eintrag}
<script>
<!-- //
function edit_entry(ID) {
    {foreach from=$eintrag item=e}
    document.getElementById('eintrag_edit_' + {$e.Id}).style.display = 'none';
    document.getElementById('eintrag_' + {$e.Id}).style.display = '';
    {/foreach}
    document.getElementById('eintrag_' + ID).style.display = 'none';
    document.getElementById('eintrag_edit_' + ID).style.display = '';
}
//-->
</script>

<div class="wrapper">
  <div class="h3 title">{#Comments#}</div>
  {foreach from=$eintrag item=e}
      <div class="box-content{if $e.Aktiv != 1} border-error{/if}" id="comment_{$e.Id}">
        <div class="row no-gutters">
          <div class="col-auto mr-2">
            {$e.Avatar}
          </div>
          <div class="col">
            <div class="listing-head flex-line">
              <div class="mr-3">
                <span class="font-weight-bold">{#Date#}:</span> {$e.Datum|date_format: $lang.DateFormatExtended}
              </div>
              <div class="mr-3">
                <span class="font-weight-bold">{#GlobalAutor#}:</span>
                {if $e.Autor_Id}
                    <a href="index.php?p=user&amp;id={$e.Autor_Id}&amp;area={$area}">{$e.Autor|sanitize}</a>
                {else}
                    {$e.Autor|sanitize}
                {/if}
              </div>
              {if $e.Autor_Herkunft}
                  <div class="mr-3">
                    <span class="font-weight-bold">{#Town#}:</span> {$e.Autor_Herkunft|sanitize}
                  </div>
              {/if}
              {if $e.Autor_Web}
                  <a rel="nofollow" target="_blank" href="{$e.Autor_Web|sanitize}">
                    <i class="icon-link" data-toggle="tooltip" title="{$e.Autor_Web|sanitize}"></i>
                  </a>
              {/if}
            </div>

            <div id="eintrag_{$e.Id}">
              <!--START_NO_REWRITE-->
              {$e.Eintrag}
              <!--END_NO_REWRITE-->
            </div>

            {if permission('edit_comments')}
                <div id="eintrag_edit_{$e.Id}" style="display: none">
                  <form method="post" action="{page_link}#comment_{$e.Id}">
                    <div class="row form-group">
                      <div class="col-md-3 col-sm-12">
                        <label class="col-form-label" for="c-autor-{$e.Id}">{#GlobalName#}</label>
                      </div>
                      <div class="col-md-9 col-sm-12">
                        <input class="form-control" id="c-autor-{$e.Id}" name="E_Autor" type="text" value="{$e.Autor|sanitize}" />
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col-md-3 col-sm-12">
                        <label class="col-form-label" for="c-mail-{$e.Id}">{#Email#}</label>
                      </div>
                      <div class="col-md-9 col-sm-12">
                        <input class="form-control" id="c-mail-{$e.Id}" name="E_Email" type="email" value="{$e.Autor_Email|sanitize}" />
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col-md-3 col-sm-12">
                        <label class="col-form-label" for="c-site-{$e.Id}">{#Web#}</label>
                      </div>
                      <div class="col-md-9 col-sm-12">
                        <input class="form-control" id="c-site-{$e.Id}" name="E_Webseite" type="text" value="{$e.Autor_Web|sanitize}" />
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col-md-3 col-sm-12">
                        <label class="col-form-label" for="c-town-{$e.Id}">{#Town#}</label>
                      </div>
                      <div class="col-md-9 col-sm-12">
                        <input class="form-control" id="c-town-{$e.Id}" name="E_Herkunft" type="text" value="{$e.Autor_Herkunft|sanitize}" />
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col-md-3 col-sm-12">
                        <label class="col-form-label" for="c-text-{$e.Id}">{#GlobalMessage#}</label>
                      </div>
                      <div class="col-md-9 col-sm-12">
                        <textarea class="form-control" id="c-text-{$e.Id}" name="E_Eintrag" rows="3">{$e.Eintrag_Raw|sanitize}</textarea>
                      </div>
                    </div>
                    <input class="btn btn-primary btn-block-sm" type="submit" value="{#Save#}" />
                    <input type="hidden" name="id" value="{$smarty.request.id}" />
                    <input type="hidden" name="comment_action" value="edit" />
                    <input type="hidden" name="comment_id" value="{$e.Id}" />
                    <input type="hidden" name="page" value="{$smarty.request.page|default:1}" />
                  </form>
                </div>

                <div class="text-right">
                  {if $e.Aktiv != 1}
                      <a href="index.php?p=comments&amp;action=change&amp;page={$smarty.request.page|default:'1'}&amp;id={$e.Id}">
                        <i class="icon-ok" data-toggle="tooltip" title="{#Comment_SetActive#}"></i>
                      </a>
                  {/if}
                  <a href="javascript:void(0);" onclick="edit_entry('{$e.Id}');return false;">
                    <i class="icon-pencil" data-toggle="tooltip" title="{#GlobalEdit#}"></i>
                  </a>
                  {if permission('delete_comments')}
                      <a onclick="return confirm('{#Comment_DeleteC#}');" href="index.php?p=comments&amp;action=delete&amp;page={$smarty.request.page|default:'1'}&amp;id={$e.Id}">
                        <i class="icon-cancel" data-toggle="tooltip" title="{#Delete#}"></i>
                      </a>
                  {/if}
                </div>
            {/if}
          </div>
        </div>
      </div>
  {/foreach}
</div>

{if !empty($pages)}
    <div class="wrapper">
      {$pages}
    </div>
{/if}
{/if}

<div id="comment_new">
  {if permission('comments')}
      {if !isset($noComment)}
          {include file="$incpath/comments/form.tpl"}
      {/if}
  {else}
      <div class="box-content">
        {#Comment_NoPerm#}
      </div>
  {/if}
</div>
{/if}
