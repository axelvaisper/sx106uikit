<div class="h1 title">{#Poll_Archive#}</div>
{foreach from=$polls item=p name=pa}
    <div class="box-content">
      <div class="h2">
        {if $p->Aktiv == 1}
            <a href="index.php?p=poll&amp;area={$area}">{$p->Titel|sanitize}</a>
        {else}
            <a href="index.php?p=poll&amp;id={$p->Id}&amp;name={$p->Titel|translit}&amp;area={$area}">{$p->Titel|sanitize}</a>
        {/if}
      </div>

      <div class="wrapper">
        {foreach from=$p->PollItems item=pas}
            <div class="spacer">
              {if $pas->Perc == 1}
                  {assign var=PollVar value=0}
              {else}
                  {assign var=PollVar value=$pas->Perc|replace:',': '.'}
              {/if}
              <div>{$pas->Frage|sanitize}</div>
              <div class="progress">
                <div class="progress-bar progress-bar-striped" role="progressbar" style="width:{$PollVar|default:0}%;background-color:{$pas->Farbe}" aria-valuenow="{$PollVar|default:0}" aria-valuemin="0" aria-valuemax="100">
                  {if !empty($PollVar)}
                      {$PollVar|default:0}%
                  {else}
                      <span style="color:{$pas->Farbe}">{$PollVar|default:0}%</span>
                  {/if}
                </div>
              </div>
            </div>
        {/foreach}
      </div>

      <div class="row">
        <div class="col-4">{#Poll_Users#}:</div>
        <div class="col-6">{$p->HitsAll}</div>
      </div>
      <div class="row">
        <div class="col-4">{#Poll_Start#}:</div>
        <div class="col-6">{$p->Ende|date_format:$lang.DateFormatSimple}</div>
      </div>
      <div class="row">
        <div class="col-4">{#Poll_End#}:</div>
        <div class="col-6">{$p->Ende|date_format:$lang.DateFormatSimple}</div>
      </div>
      {if $p->Aktiv != 1}
          <div class="row">
            <div class="col-4">{#GlobalStatus#}:</div>
            <div class="col-6">{#Poll_Status_Inactive#}</div>
          </div>
      {/if}
    </div>
{/foreach}

{if isset($pollNavi)}
    <div class="wrapper">
      {$pollNavi}
    </div>
{/if}
