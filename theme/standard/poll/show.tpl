<div class="h1 title">{#Poll_Name#}</div>
{if $Inactive == 1}
    <div class="box-content">{#Poll_Ianactive#}</div>
{else}
    <div class="box-content">
      {$CPoll}
    </div>

    <div class="h3 title">{#Info#}</div>
    <div class="box-content">
      <div class="row">
        <div class="col-4">{#Poll_Users#}:</div>
        <div class="col-6">{$PollRes->HitsAll}</div>
      </div>
      <div class="row">
        <div class="col-4">{#Poll_Start#}:</div>
        <div class="col-6">{$PollRes->Ende|date_format:$lang.DateFormatSimple}</div>
      </div>
      <div class="row">
        <div class="col-4">{#Poll_End#}:</div>
        <div class="col-6">{$PollRes->Ende|date_format:$lang.DateFormatSimple}</div>
      </div>
      {if $PollRes->Aktiv != 1}
          <div class="row">
            <div class="col-4">{#GlobalStatus#}:</div>
            <div class="col-6">{#Poll_Status_Inactive#}</div>
          </div>
      {/if}
    </div>

    {if $PollRes->Kommentare == 1}
        {if $PollRes->Aktiv != 1}
            {assign var=noComment value=1}
        {/if}
        {$GetComments}
    {/if}
{/if}
