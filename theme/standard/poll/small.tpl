{if (!empty($PollResultsSmall) || !empty($PollAnswersSmall)) && $smarty.request.p != 'poll'}
    <div class="wrapper panel">
      <div class="panel-head">{#Poll_Name#}</div>
      <div class="panel-main" id="pollform">
        {include file="$incpath/poll/raw.tpl"}
      </div>
    </div>
{/if}
