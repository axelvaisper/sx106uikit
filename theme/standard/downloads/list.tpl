{foreach from=$Entries item=e}
    <div class="box-content">
      <h3 class="listing-head">
        <a title="{$e->Name|sanitize}" href="{$e->Link_Details}">{$e->Name|sanitize}</a>
      </h3>

      <div class="clearfix">
        {if !empty($e->Bild)}
            <a href="{$e->Link_Details}">
              <img class="img-fluid listing-img-right" src="uploads/downloads/{$e->Bild}" alt="{$e->Name|sanitize}" />
            </a>
        {/if}
        <div class="text-justify">
          {$e->Beschreibung|html_truncate:500}
        </div>
      </div>

      <div class="listing-foot flex-line justify-content-center">
        <span class="mr-4" data-toggle="tooltip" title="{#Added#}"><i class="icon-calendar"></i>{$e->Datum|date_format:$lang.DateFormatSimple}</span>
        <span class="mr-4" data-toggle="tooltip" title="{#DownloadsNum#}"><i class="icon-download"></i>{$e->Hits}</span>
          {if $Downloadssettings->Wertung == 1}
          <span class="mr-4" data-toggle="tooltip" title="{#Rating_Rating#}"><i class="icon-thumbs-up"></i>{$e->Wertung|default:0}</span>
          {/if}
          {if $Downloadssettings->Kommentare == 1}
          <span class="mr-4" data-toggle="tooltip" title="{#Comments#}"><a href="{$e->Link_Details}#comments"><i class="icon-chat"></i>{$e->CCount}</a></span>
          {/if}
        <a href="{$e->Link_Details}"><i class="icon-right-open"></i>{#MoreDetails#}</a>
      </div>
    </div>
{/foreach}

{if !empty($Navi)}
    <div class="wrapper">
      {$Navi}
    </div>
{/if}
