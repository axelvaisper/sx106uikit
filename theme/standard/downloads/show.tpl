<script>
<!-- //
$(function () {
    $('.linkextern').on('click', function () {
        var options = {
            target: '#plick',
            url: 'index.php?action=updatehitcount&p=downloads&id=' + $(this).data('id'),
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
});
//-->
</script>

<div id="plick"></div>
<div class="wrapper">
  <h1 class="title">{$link_res->Name|sanitize}</h1>
  <div class="wrapper clearfix">
    {if !empty($link_res->Bild)}
        <img class="img-fluid img-right" src="uploads/downloads/{$link_res->Bild}" alt="{$link_res->Name|sanitize}" />
    {/if}
    {$link_res->Beschreibung}
  </div>
</div>

<div class="wrapper">
  <div class="h3 title">{#Info#}</div>
  <div class="box-content">
    <div class="row">
      <div class="col-4">{#Added#}</div>
      <div class="col-8">
        {$link_res->Datum|date_format:$lang.DateFormatSimple}
      </div>
    </div>
    <div class="row">
      <div class="col-4">{#GlobalAutor#}</div>
      <div class="col-8">
        <a href="index.php?p=user&amp;id={$link_res->Autor}&amp;area={$area}">{$link_res->UserName}</a>
      </div>
    </div>
    {if empty($link_res->Url_Direct)}
        <div class="row">
          <div class="col-4">{#Downloads_Size#}</div>
          <div class="col-8">{$link_res->Size}</div>
        </div>
    {/if}
    <div class="row">
      <div class="col-4">{#Downloads_Hits#}</div>
      <div class="col-8">{$link_res->Hits}</div>
    </div>
    {if $link_res->BetriebsOs}
        <div class="row">
          <div class="col-4">{#Downloads_Os#}</div>
          <div class="col-8">{$link_res->BetriebsOs|replace:'|':', '}</div>
        </div>
    {/if}
    {if $link_res->SoftwareTyp}
        <div class="row">
          <div class="col-4">{#Downloads_SoftType#}</div>
          <div class="col-8">{$link_res->SoftwareTyp|sanitize}</div>
        </div>
    {/if}
    {if $link_res->Version}
        <div class="row">
          <div class="col-4">{#Downloads_SoftVersion#}</div>
          <div class="col-8">{$link_res->Version|sanitize}</div>
        </div>
    {/if}
    {if !empty($link_res->Sprache) && $Downloadssettings->Flaggen == 1}
        <div class="row">
          <div class="col-4">{#Country#}</div>
          <div class="col-8"><img src="{$imgpath}/flags/{$link_res->Sprache}.png" alt="" /></div>
        </div>
    {/if}
    {if $Downloadssettings->Wertung == 1}
        <div class="row">
          <div class="col-4">{#Rating_Rating#}</div>
          <div class="col-8 flex-line">
            <div class="rating-star" data-rating="{$link_res->Wertung}"></div>
          </div>
        </div>
    {/if}
  </div>
</div>

<div class="wrapper">
  <div class="h3 title">{#GlobalActions#}</div>
  <div class="box-content">
    {if permission('downloads_candownload')}
        {if !empty($link_res->Url)}
            <a class="linkextern" data-id="{$link_res->Id}" rel="nofollow" {if !empty($link_res->Url_Direct)}target="_blank" {/if}href="{if !empty($link_res->Url_Direct)}{$link_res->Url_Direct}{else}index.php?p=downloads&amp;action=getfile&amp;id={$link_res->Id}{/if}">
              <i class="icon-download"></i>{#DownloadsFile#}
            </a>
        {else}
            <div class="text-danger">
              <i class="icon-error"></i>{#FileNoLoad#}
            </div>
        {/if}
    {else}
        <div class="text-danger">
          <i class="icon-lock"></i>{#Downloads_NoPerm#}
        </div>
    {/if}
    {if $Downloadssettings->Kommentare == 1}
        <a class="d-block" href="#comments">
          <i class="icon-chat"></i>{#Comments#}
        </a>
    {/if}
    {if $Downloadssettings->DefektMelden == 1}
        {if !empty($link_res->DefektGemeldet)}
            <div class="text-danger">
              <i class="icon-warning-empty"></i>{#Links_ErrorSendBrokenImpos#}
            </div>
        {else}
            <a class="d-block" onclick="document.getElementById('broken-click').style.display = '';" href="javascript:void(0);">
              <i class="icon-warning-empty"></i>{#Links_ErrorSendBroken#}
            </a>
        {/if}
    {/if}
  </div>
</div>

{if $Downloadssettings->DefektMelden == 1}
    {include file="$incpath/other/broken.tpl"}
{/if}

{if $alternatives && permission('downloads_candownload')}
    <div class="wrapper">
      <div class="h3 title">{#Links#}</div>
      <div class="box-content">
        {foreach from=$alternatives item=a}
            <a class="d-block linkextern" data-id="{$link_res->Id}" rel="nofollow" href="{$a->Link}" target="_blank">
              <i class="icon-link"></i>{$a->Name}
            </a>
        {/foreach}
      </div>
    </div>
{/if}

{$RatingForm}
{$GetComments}
