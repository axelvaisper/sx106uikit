<section id="subscribe">
<div class="uk-container uk-container-small">

  <h1 class="uk-text-center">{#Newsletter#}</h1>

  <hr class="uk-divider-icon">

  {if !empty($email_error)}
      <div class="uk-alert-warning" uk-alert>
        <div class="uk-text-bold">{#Newsletter_e_inf#}</div>
        {foreach from=$email_error item=e}
            <div>{$e}</div>
        {/foreach}
      </div>
  {/if}

  {if isset($Entry_Ok) && $Entry_Ok == 1}
      <div class="uk-alert-success uk-text-center" uk-alert>{#Newsletter_okT#}<br>{#Newsletter_ok#}</div>
  {else}
        {#Newsletter_info#}

      <form method="post" action="index.php?p=newsletter&amp;area={$area}">
        <div class="row form-group">
          <div class="col-md-9 col-sm-12">
            <input class="form-control" id="nl-email" name="nl_email" type="email" value="{$smarty.post.nl_email|sanitize}" required="required" placeholder="{#Email#}" />
          </div>
        </div>

        {if $Nl_Count > 1}
            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label">{#Newsletter_sections#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                {foreach from=$nl_items item=nli}
                    <div class="form-check">
                      <input class="form-check-input" id="nl-welche[{$nli->Id}]" name="nl_welche[{$nli->Id}]" value="1" type="checkbox" />
                      <label class="form-check-label" for="nl-welche[{$nli->Id}]">{$nli->Name|sanitize}</label>
                    </div>
                {/foreach}
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label" for="nl-format">{#Newsletter_format#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                <select class="form-control" id="nl-format" name="nl_format">
                  <option value="html">{#GlobalHTML#}</option>
                  <option value="text">{#GlobalText#}</option>
                </select>
              </div>
            </div>

            <div class="text-center">
              <input class="btn btn-primary btn-block-sm" type="submit" value="{#Newsletter_aboButton#}" />
            </div>
            {if $Nl_Count < 2}
                {foreach from=$nl_items item=nli}
                    <input type="hidden" name="nl_welche[{$nli->Id}]" value="1" />
                {/foreach}
            {/if}
            <input type="hidden" name="action" value="abonew" />
        </form>
      {/if}
  {/if}


</div>
</section>