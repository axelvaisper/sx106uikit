{if $nl_items}
<section id="newsletter">
<div id="subscribe-big">
<form method="post" action="index.php?p=newsletter&amp;area={$area}" class="subscribe-form">

  <div class="seal">
      <i uk-icon="icon: mail; ratio: .08"></i>
  </div>
  <div class="title">
    {#Newsletter#}
  </div>
  <label for="email">
    Подпишись, чтобы быть в курсе наших акций!
  </label>

    <input class="uk-input" name="nl_email" type="email" id="nls-email" placeholder="{#Email#}" required="required" />
  {if $Nl_Count > 1}
      <div class="form-group">
        <label>{#Newsletter_sections#}</label>
        {foreach from=$nl_items item=nli}
            <div class="form-check">
              <input class="form-check-input" id="nls-welche[{$nli->Id}]" name="nl_welche[{$nli->Id}]" value="1" type="checkbox">
              <label class="form-check-label" for="nls-welche[{$nli->Id}]">{$nli->Name|sanitize}</label>
            </div>
        {/foreach}
      </div>
  {else}
      {foreach from=$nl_items item=nli}
          <input type="hidden" name="nl_welche[{$nli->Id}]" value="1" />
      {/foreach}
  {/if}
  <div hidden="">
    <label class="sr-only" for="nls-format">{#Newsletter_format#}</label>
    <select class="" id="nls-format" name="nl_format">
      <option value="html">{#GlobalHTML#}</option>
      <option value="text">{#GlobalText#}</option>
    </select>
  </div>
  <input class="uk-button uk-button-primary uk-width-1-1" value="{#Newsletter_aboButton#}" type="submit" />
  <input name="action" value="abonew" type="hidden" />
</form>
</div>
</section>
{/if}
