{script file="$jspath/jsuggest.js" position='head'}
<script>
<!-- //
$(function () {
    $('#ms').suggest('index.php?p=products&action=quicksearch&key=' + Math.random());
});
//-->
</script>

<div class="wrapper">
  <h1 class="title">{#Products#}</h1>
  <form method="post" action="index.php?p=products&amp;area={$area}">
    <div class="input-group">
      <input class="form-control" id="ms" name="q" value="{$smarty.request.q|sanitize}" type="text" />
      <span class="input-group-append">
        <input class="btn btn-primary" type="submit" value="{#Products_search#}" />
      </span>
    </div>
  </form>

  {if !$items}
      <div class="box-content">{#Products_none#}</div>
  {else}
      <div class="spacer flex-line justify-content-center">
        <span class="mr-3">{#SortBy#}:</span>
        <a class="mr-3" href="index.php?p=products&amp;area={$area}&amp;page={$smarty.request.page|default:1}&amp;sort={$datesort|default:'dateasc'}{if !empty($smarty.request.q)}&amp;q={$smarty.request.q}{/if}">
          {#SortDate#}<i class="{$img_date|default:'icon-sort'}"></i>
        </a>
        <a class="mr-3" href="index.php?p=products&amp;area={$area}&amp;page={$smarty.request.page|default:1}&amp;sort={$namesort|default:'nameasc'}{if !empty($smarty.request.q)}&amp;q={$smarty.request.q}{/if}">
          {#SortName#}<i class="{$img_name|default:'icon-sort'}"></i>
        </a>
        <a class="mr-3" href="index.php?p=products&amp;area={$area}&amp;page={$smarty.request.page|default:1}&amp;sort={$hitssort|default:'hitsdesc'}{if !empty($smarty.request.q)}&amp;q={$smarty.request.q}{/if}">
          {#Global_Hits#}<i class="{$img_hits|default:'icon-sort'}"></i>
        </a>
        <a class="mr-3" href="index.php?p=products&amp;area={$area}&amp;page={$smarty.request.page|default:1}&amp;sort={$genresort|default:'genredesc'}{if !empty($smarty.request.q)}&amp;q={$smarty.request.q}{/if}">
          {#Global_Categ#}<i class="{$img_genre|default:'icon-sort'}"></i>
        </a>
      </div>

      {foreach from=$items item=res}
          <div class="box-content">
            <h3 class="listing-head">
              <a title="{$res->Name|sanitize}" href="index.php?p=products&amp;area={$area}&amp;action=showproduct&amp;id={$res->Id}&amp;name={$res->Name|translit}">{$res->Name|sanitize}</a>
            </h3>
            <div class="clearfix">
              {if !empty($res->Bild)}
                  <a href="index.php?p=products&amp;area={$area}&amp;action=showproduct&amp;id={$res->Id}&amp;name={$res->Name|translit}">
                    <img class="img-fluid listing-img-right" src="uploads/products/{$res->Bild}" alt="{$res->Name|sanitize}" />
                  </a>
              {/if}
              <div class="text-justify">
                {$res->Beschreibung|truncate:500}
              </div>
            </div>

            <div class="listing-foot flex-line justify-content-center">
              <span class="mr-4" data-toggle="tooltip" title="{#Added#}"><i class="icon-calendar"></i>{$res->Datum|date_format:$lang.DateFormatSimple}</span>
              <span class="mr-4" data-toggle="tooltip" title="{#Global_Hits#}"><i class="icon-eye"></i>{$res->Hits}</span>
                {if $product_rate == 1}
                <span class="mr-4" data-toggle="tooltip" title="{#Rating_Rating#}">
                  <i class="icon-thumbs-up"></i>{$res->Wertung|default:0}
                </span>
              {/if}
              <span class="mr-4" data-toggle="tooltip" title="{#Comments#}">
                <a href="index.php?p=products&amp;area={$area}&amp;action=showproduct&amp;id={$res->Id}&amp;name={$res->Name|translit}#comments">
                  <i class="icon-chat"></i>{$res->CCount}
                </a>
              </span>
              <a href="index.php?p=products&amp;area={$area}&amp;action=showproduct&amp;id={$res->Id}&amp;name={$res->Name|translit}">
                <i class="icon-right-open"></i>{#MoreDetails#}
              </a>
            </div>
          </div>
      {/foreach}
  {/if}
</div>

{if !empty($Navi)}
    <div class="wrapper">
      {$Navi}
    </div>
{/if}
