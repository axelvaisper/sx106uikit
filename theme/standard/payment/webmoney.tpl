{include file="$incpath/shop/steps.tpl"}

{if empty($payment_error)}
<script>
<!-- //
$(window).load(function() {
    setTimeout(function() {
        $('#process').submit();
    }, 2500);
});
//-->
</script>

<div class="h3 title">{#Shop_thankyou_title#}</div>
<div class="box-content">
  {if !empty($payment_data.Icon)}
      <div class="wrapper text-center">
        <img src="uploads/shop/payment_icons/{$payment_data.Icon}" alt="" />
      </div>
  {/if}

  {if !empty($payment_data.Text)}
      <div class="wrapper">
        {$payment_data.Text}
      </div>
  {/if}

  {if !empty($payment_data.TextLang)}
      {$payment_data.TextLang}
  {/if}

  <form method="post" name="process" id="process" action="https://merchant.webmoney.ru/lmi/payment.asp">
    <input name="reason" type="hidden" value="{$smarty.session.order_number}" />
    <input name="user_variable_0" type="hidden" value="{$smarty.session.order_number}" />
    <input name="user_variable_1" type="hidden" value="{$smarty.session.price_final|numf}" />
    <input name="hash" type="hidden" value="{$payment_hash}" />
    <input name="rkeys" type="hidden" value="{$smarty.session.order_number}" />
    <input type="hidden" name="LMI_PAYMENT_AMOUNT" value="{$smarty.session.price_final}" />
    <input type="hidden" name="LMI_PAYMENT_DESC" value="{$inf_payment}" />
    <input type="hidden" name="LMI_PAYMENT_NO" value="{$smarty.session.id_num_order}" />
    <input type="hidden" name="LMI_PAYEE_PURSE" value="{$payment_data.Install_Id}" />
    {if $payment_data.Testmodus != 3}
        <input type="hidden" name="LMI_SIM_MODE" value="{$payment_data.Testmodus}" />
    {/if}
    <input type="hidden" name="LMI_RESULT_URL" value="{$baseurl}/index.php?payment=wm&p=shop&action=callback&reply=result" />
    <input type="hidden" name="LMI_SUCCESS_URL" value="{$baseurl}/index.php?payment=wm&p=shop&action=callback&reply=success" />
    <input type="hidden" name="LMI_SUCCESS_METHOD" value="2" />
    <input type="hidden" name="LMI_FAIL_URL" value="{$baseurl}/index.php?payment=wm&p=shop&action=callback&reply=error" />
    <input type="hidden" name="LMI_FAIL_METHOD" value="2" />
  </form>
</div>
{else}
    <div class="h3 title">{#Global_error#}</div>
    <div class="box-content">
      {#Payment_Error#}
    </div>
{/if}
