{include file="$incpath/shop/steps.tpl"}

<div class="h3 title">{#Shop_thankyou_title#}</div>
<div class="box-content">
  <div class="wrapper">
    {#Shop_order_thankyou#}
  </div>
  {if !empty($smarty.session.DetailInfo)}
      <div class="wrapper">
        {$smarty.session.DetailInfo}
      </div>
  {/if}
</div>

<form method="post" action="index.php?p=shop">
  <div class="text-center">
    <input class="btn btn-primary" type="submit" value="{#Shop_back_shop#}" />
  </div>
</form>
