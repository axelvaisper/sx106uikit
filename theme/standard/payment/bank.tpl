<script>
<!-- //
$(function() {
    $('.popup-print').colorbox({ height: "95%", width: "80%", iframe: true });
});
//-->
</script>

{include file="$incpath/shop/steps.tpl"}

{if $payment_error != 1}
    <div class="h3 title">{#Shop_thankyou_title#}</div>
    <div class="box-content">
      <div class="wrapper">
        Счет оформлен
      </div>
      <div class="spacer">
        <a class="popup-print" title="Распечатать счет" href="index.php?p=misc&do=viewpayorder&oid={$smarty.session.id_num_order}">
          <i class=icon-print></i>Распечатать счет
        </a>
      </div>
      <div class="spacer">
        <a title="{#Shop_go_myorders#}" href="index.php?p=shop&amp;action=myorders">
          <i class=icon-basket></i>{#Shop_go_myorders#}
        </a>
      </div>
    </div>
{else}
    <div class="h3 title">{#Global_error#}</div>
    <div class="box-content">
      {#Payment_Error#}
    </div>
{/if}
