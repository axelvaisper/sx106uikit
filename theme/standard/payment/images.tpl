<div class="wrapper panel">
  <div class="panel-head">{#Shop_payment_images_inf#}</div>
  <div class="panel-main">
    {foreach from=$payment item=payments}
        <div class="wrapper">
          {if empty($payments->Icon)}
              <a class="d-block colorbox-sm" href="index.php?p=misc&amp;do=payment_info&amp;id={$payments->Id}">{$payments->Name|sanitize}</a>
          {else}
              <div class="text-center" data-toggle="tooltip" title="{$payments->Name|tooltip}">
                <a class="colorbox-sm" href="index.php?p=misc&amp;do=payment_info&amp;id={$payments->Id}">
                  <img class="img-fluid" src="{$baseurl}/uploads/shop/payment_icons/{$payments->Icon}" alt="" />
                </a>
              </div>
          {/if}
        </div>
    {/foreach}
  </div>
</div>
