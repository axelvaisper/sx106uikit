{include file="$incpath/shop/steps.tpl"}

{if empty($payment_error)}
<script>
<!-- //
$(window).load(function() {
    setTimeout(function() {
        $('#process').submit();
    }, 2500);
});
//-->
</script>

<div class="h3 title">{#Shop_thankyou_title#}</div>
<div class="box-content">
  {if !empty($payment_data.Icon)}
      <div class="wrapper text-center">
        <img src="uploads/shop/payment_icons/{$payment_data.Icon}" alt="" />
      </div>
  {/if}

  {if !empty($payment_data.Text)}
      <div class="wrapper">
        {$payment_data.Text}
      </div>
  {/if}

  {if !empty($payment_data.TextLang)}
      {$payment_data.TextLang}
  {/if}

  <form method="post" name="process" id="process" action="https://liqpay.com/?do=clickNbuy" accept-charset="{$charset}">
    <input type="hidden" name="version" value="1.1" />
    <input type="hidden" name="merchant_id" value="{$payment_data.Install_Id}" />
    <input type="hidden" name="amount" value="{$smarty.session.price_final}" />
    <input type="hidden" name="currency" value="{$payment_data.Testmodus}" />
    <input type="hidden" name="description" value="{$inf_payment}" />
    <input type="hidden" name="order_id"  value="{$smarty.session.id_num_order}" />
    <input type="hidden" name="result_url" value="{$baseurl}/index.php?payment=lp&p=shop&action=callback&reply=reset" />
    <input type="hidden" name="server_url" value="{$baseurl}/index.php?payment=lp&p=shop&action=callback&reply=result" />
    <input type="hidden" name="order_number" value="{$smarty.session.order_number}" />
    <input type="hidden" name="hash" value="{$payment_hash}" />
  </form>
</div>
{else}
    <div class="h3 title">{#Global_error#}</div>
    <div class="box-content">
      {#Payment_Error#}
    </div>
{/if}
