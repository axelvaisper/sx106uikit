<h1 class="title text-truncate">{$payment_inf->Name}</h1>
{if $payment_inf->Icon}
    <div class="wrapper text-center">
      <img src="uploads/shop/payment_icons/{$payment_inf->Icon}" alt="" />
    </div>
{/if}
{if !empty($payment_inf->Beschreibung)}
    <div class="wrapper text-center">
      {$payment_inf->Beschreibung}
    </div>
{/if}
{if !empty($payment_inf->BeschreibungLang)}
    <div class="wrapper text-center">
      {$payment_inf->BeschreibungLang}
    </div>
{/if}

<div class="text-center">
  <input class="btn btn-primary" onclick="window.print();" type="button" value="{#PrintNow#}" />
  <input class="btn btn-secondary" onclick="closeWindow();" type="button" value="{#WinClose#}" />
</div>
