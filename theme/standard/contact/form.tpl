{if $contact_fields}
{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function() {
    $('#{$form_id}').validate({
        rules: {
            {foreach from=$contact_fields item=cf name=valic}
           '{$cf->Name}': {
                {if $cf->Email == 1}
                email: true{if $cf->Pflicht == 1 || $cf->Zahl == 1},{/if}
                {/if}
                {if $cf->Pflicht == 1}
                required: true{if $cf->Zahl == 1},{/if}
                {/if}
                {if $cf->Zahl == 1}
                 number: true
                {/if}
            },
            {/foreach}
            '{#Contact_myName#}': { required: false },
            '{#SendEmail_Email#}': { required: false, email: true }
       },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                success: function(data) {
                    if (data === 'true') {
                        $(form).resetForm();
                        UIkit.notification("<span uk-icon='icon: check'></span> Спасибо. В ближайшее время с вами свяжется наш сотрудник.");
                        // $('#contact-text-{$form_id}').text('{#Contact_thankyou#}');
                    } else {
                        // $('#contact-text-{$form_id}').text('{#Global_error#}');
                    }
                     // showModal($('#contact-{$form_id}'), 5000);
                },
                clearForm: false
            });
        }
    });
});
function submitOtherData() {
    document.getElementById('___hmail').value = document.getElementById('ye_{$form_id}').value;
    document.getElementById('___hname').value = document.getElementById('yn_{$form_id}').value;
}
//-->
</script>

{* <h2 class="uk-text-center">
  {$contact_title|sanitize}
</h2> *}

{if $form_intro}
    <div class="uk-text-center">
      {$form_intro|sanitize}
      <br><br>
    </div>
{/if}

<form onsubmit="submitOtherData();" id="{$form_id}" method="post" enctype="multipart/form-data" action="index.php?p=contact" class="uk-grid-small uk-margin-top" uk-grid>
  <input type="hidden" name="__hmail" id="___hmail" />
  <input type="hidden" name="__hname" id="___hname" />
  <input type="hidden" name="id" value="{$form_id_raw}" />

  <div class="uk-width-1-2@m">
    <input class="uk-input" id="yn_{$form_id}" name="{#Contact_myName#}" type="text" value="{$smarty.session.user_name}" placeholder="{#Contact_myName#}.." />
  </div>

  <div class="uk-width-1-2@m">
    <input class="uk-input" id="ye_{$form_id}" name="{#SendEmail_Email#}" type="text" value="{$smarty.session.login_email}" placeholder="{#SendEmail_Email#}.." />
  </div>

  {foreach from=$contact_fields item=cf}
      {if $cf->Typ == 'textfield'}
      <div class="uk-width-1-1@m">
        <input class="uk-input" id="cf_{$cf->Id}" name="{$cf->Name}" type="text" value="{$cf->Werte|sanitize}" placeholder="{$cf->Name|sanitize}.." />
      </div>
      {elseif $cf->Typ == 'radio'}
          <div class="uk-width-1-2@m">
              <label class="col-form-label">{$cf->Name|sanitize}</label>
            <div >
              <div id="cf_{$cf->Id}"></div>
              {foreach from=$cf->OutElemVal item=rw key=key}
                  <div class="form-check">
                    <input class="form-check-input" id="cf_{$cf->Id}_{$key}" name="{$cf->Name}" value="{$rw}" type="radio"{if $smarty.foreach.rwn.first} checked="checked"{/if} />
                    <label class="form-check-label" for="cf_{$cf->Id}_{$key}">{$cf->Name|sanitize}</label>
                  </div>
              {/foreach}
            </div>
          </div>
      {elseif $cf->Typ == 'checkbox'}
          <div class="uk-width-1-2@m">
            <div>
              <label class="col-form-label">{$cf->Name|sanitize}</label>
            </div>
            <div >
              <div id="cf_{$cf->Id}"></div>
              {foreach from=$cf->OutElemVal item=rw key=key}
                  <div class="form-check">
                    <input class="form-check-input" id="cf_{$cf->Id}_{$key}" name="{$cf->Name}[]" value="{$rw}" type="checkbox" {if $smarty.foreach.rwn.first}checked="checked"{/if} />
                    <label class="form-check-label" for="cf_{$cf->Id}_{$key}">{$cf->Name|sanitize}</label>
                  </div>
              {/foreach}
            </div>
          </div>
      {elseif $cf->Typ == 'dropdown'}
         <div class="uk-width-1-2@m uk-text-center">
          <div uk-form-custom="target: > * > span:first-child">
            <select>
                <option value="">{$cf->Name|sanitize}..</option>
                {foreach from=$cf->OutElemVal item=rw}
                    <option value="{$rw}">{$rw}</option>
                {/foreach}
            </select>
            <button class="uk-button uk-button-default" type="button" tabindex="-1">
                <span></span>
                <span uk-icon="icon: chevron-down"></span>
            </button>
          </div>
        </div>
      {elseif $cf->Typ == 'textarea'}
        <div class="uk-width-1-1">
          <textarea class="uk-input" id="cf_{$cf->Id}" name="{$cf->Name}" rows="5" placeholder="{$cf->Name|sanitize}.."></textarea>
        </div>
      {/if}
  {/foreach}
  {if $form_attachment}
        {section name=xx loop=$form_attachment}
        <div uk-form-custom="target: true" class="uk-width-1-2@m">
            <input name="files[]" type="file">
            <input class="uk-input" type="text" placeholder="{#Contact_attachment_mes#}">
        </div>
        {/section}
  {/if}

  {include file="$incpath/other/captcha.tpl"}

{*         {if $loggedin}
      <div class="row form-group">
        <div class="col-md-12 col-sm-12">
          <div class="form-check">
            <input class="form-check-input" id="mailcopy" name="mailcopy" value="1" type="checkbox" checked="checked" />
            <label class="form-check-label" for="mailcopy">{#Contact_wish_mailcopy#}</label>
          </div>
        </div>
      </div>
  {/if} *}

  <div class="uk-width-1-1 uk-text-center">
  <input class="uk-button uk-button-primary" type="submit" value="{$contact_button|sanitize|default:$lang.ButtonSend}" />
  </div>

</form>
{/if}
