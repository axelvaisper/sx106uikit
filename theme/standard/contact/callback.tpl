{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function() {
    $('#{$form_id}').validate({
        rules: {
            {foreach from=$contact_fields item=cf name=valic}
           '{$cf->Name}': {
                {if $cf->Email == 1}
                email: true{if $cf->Pflicht == 1 || $cf->Zahl == 1},{/if}
                {/if}
                {if $cf->Pflicht == 1}
                required: true{if $cf->Zahl == 1},{/if}
                {/if}
                {if $cf->Zahl == 1}
                 number: true
                {/if}
            },
            {/foreach}
            // '{#Contact_myName#}': { required: true },
            // '{#SendEmail_Email#}': { required: true, email: true }
       },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                success: function(data) {
                    if (data === 'true') {
                        $(form).resetForm();
                        UIkit.notification("<span uk-icon='icon:check'></span> Спасибо.<br>В ближайшее время с вами свяжется наш сотрудник.");
                        // $('#contact-text-{$form_id}').text('Спасибо. В ближайшем времнени с вами свяжется наш сотрудник.');
                    } else {
                        // $('#contact-text-{$form_id}').text('{#Global_error#}');
                    }
                     // showModal($('#contact-{$form_id}'), 5000);
                },
                clearForm: false
            });
        }
    });
});
function submitOtherData() {
    document.getElementById('___hmail').value = document.getElementById('ye_{$form_id}').value;
    document.getElementById('___hname').value = document.getElementById('yn_{$form_id}').value;
}
//-->
</script>


<form onsubmit="submitOtherData();" id="{$form_id}" method="post" enctype="multipart/form-data" action="index.php?p=contact" uk-grid>
  <input type="hidden" name="__hmail" id="___hmail" />
  <input type="hidden" name="__hname" id="___hname" />
  <input type="hidden" name="id" value="{$form_id_raw}" />

  {foreach from=$contact_fields item=cf}
      {if $cf->Typ == 'textfield'}
          <input id="num_callback" class="uk-input uk-width-1-2" name="{$cf->Name}" type="text" value="{$cf->Werte|sanitize}" placeholder="{$cf->Name|sanitize}.." autofocus/>
      {elseif $cf->Typ == 'radio'}
              <label class="col-form-label">{$cf->Name|sanitize}</label>
              <div id="cf_{$cf->Id}"></div>
              {foreach from=$cf->OutElemVal item=rw key=key}
                  <div class="form-check">
                    <input class="form-check-input" id="cf_{$cf->Id}_{$key}" name="{$cf->Name}" value="{$rw}" type="radio"{if $smarty.foreach.rwn.first} checked="checked"{/if} />
                    <label class="form-check-label" for="cf_{$cf->Id}_{$key}">{$cf->Name|sanitize}</label>
                  </div>
              {/foreach}
          </div>
      {elseif $cf->Typ == 'checkbox'}
          <div class="row form-group">
            <div class="col-md-3 col-sm-12">
              <label class="col-form-label">{$cf->Name|sanitize}</label>
            </div>
            <div class="col-md-9 col-sm-12">
              <div id="cf_{$cf->Id}"></div>
              {foreach from=$cf->OutElemVal item=rw key=key}
                  <div class="form-check">
                    <input class="form-check-input" id="cf_{$cf->Id}_{$key}" name="{$cf->Name}[]" value="{$rw}" type="checkbox" {if $smarty.foreach.rwn.first}checked="checked"{/if} />
                    <label class="form-check-label" for="cf_{$cf->Id}_{$key}">{$cf->Name|sanitize}</label>
                  </div>
              {/foreach}
            </div>
          </div>
      {elseif $cf->Typ == 'dropdown'}
            <div class="col-md-6 col-sm-12">
              <label class="col-form-label" for="cf_{$cf->Id}">{$cf->Name|sanitize}</label>
              <select class="form-control" id="cf_{$cf->Id}" name="{$cf->Name}">
                {foreach from=$cf->OutElemVal item=rw}
                    <option value="{$rw}">{$rw}</option>
                {/foreach}
              </select>
            </div>
      {elseif $cf->Typ == 'textarea'}
            <div class="col-sm-12 order-last">
              <label class="col-form-label" for="cf_{$cf->Id}">{$cf->Name|sanitize}</label>
              <textarea class="form-control" id="cf_{$cf->Id}" name="{$cf->Name}" rows="5"></textarea>
            </div>
      {/if}
  {/foreach}
  {if $form_attachment}
        <div class="col-md-6 col-sm-12">
          <label class="col-form-label">{#Contact_attachment_mes#}</label>
          {section name=xx loop=$form_attachment}
              <input class="form-control" name="files[]" type="file" />
          {/section}
        </div>
  {/if}

  {include file="$incpath/other/captcha.tpl"}

  <input class="uk-button uk-button-primary uk-width-1-2" type="submit" value="{$contact_button|sanitize|default:$lang.ButtonSend}"/>

</form>