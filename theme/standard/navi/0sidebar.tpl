{if !empty($SiteNavigation)}
    <div class="sidebar-menu">
      <div class="wrapper panel">
        <div class="panel-head">{$navi_title|default:$lang.Title_Navi}</div>
        <div class="panel-main">

          <ul class="nav flex-column">
            {foreach from=$SiteNavigation item=navi}
                <li class="nav-item">
                  <a class="nav-link{if $navi->document == $document || !empty($navi->active)} active{/if}" title="{$navi->AltTitle|sanitize}" target="{$navi->target|default:'_self'}" href="{$navi->document|escape:'html'}">
                    {$navi->title|sanitize}
                  </a>

                  {if !empty($navi->sub_navi) && count($navi->sub_navi)}
                      <div class="collapse{if $navi->document == $document || !empty($navi->active)} show{/if}">
                        <ul class="nav flex-column">
                          {foreach from=$navi->sub_navi item=sub_navi}
                              <li class="nav-item ml-3">
                                <a class="nav-link{if $sub_navi->document == $document || !empty($sub_navi->active)} active{/if}" title="{$sub_navi->AltTitle|sanitize}" target="{$sub_navi->target|default:'_self'}" href="{$sub_navi->document|escape:'html'}">
                                  {$sub_navi->title|sanitize}
                                </a>

                                {if !empty($sub_navi->sub_navi) && count($sub_navi->sub_navi)}
                                    <div class="collapse{if $sub_navi->document == $document || !empty($sub_navi->active)} show{/if}">
                                      <ul class="nav flex-column">
                                        {foreach from=$sub_navi->sub_navi item=sub_sub_navi}
                                            <li class="nav-item ml-3">
                                              <a class="nav-link {if $sub_sub_navi->document == $document || !empty($sub_sub_navi->active)} active{/if}" title="{$sub_sub_navi->AltTitle|sanitize}" target="{$sub_sub_navi->target|default:'_self'}" href="{$sub_sub_navi->document|escape:'html'}">
                                                {$sub_sub_navi->title|sanitize}
                                              </a>
                                            </li>
                                        {/foreach}
                                      </ul>
                                    </div>
                                {/if}

                              </li>
                          {/foreach}
                        </ul>
                      </div>
                  {/if}

                </li>
            {/foreach}
          </ul>

        </div>
      </div>
    </div>
{/if}
