<nav uk-navbar="mode:click" class="topbar uk-navbar-container uk-navbar-transparent" style="min-height:40px">

  {*MOBILE NAV*}
  <div class="uk-navbar-left overlay-1 uk-hidden@m uk-visible" uk-lightbox>
    {* <a data-caption="Карта" data-type="iframe" href="index.php?p=content&id=3&name=blank&area=1" class="uk-navbar-item" uk-icon="icon:fa-map-marker; ratio:1.2" uk-tooltip="Показать на карте"></a> *}
  </div>

  <div class="uk-navbar-center overlay-1 uk-hidden@m uk-visible">
    <a class="uk-navbar-item" uk-toggle="target: .overlay-1" href="#callback" uk-icon="icon:fa-mobile; ratio:1.2" uk-tooltip="Заказать обратный звонок">
      {$settings.Telefon}
    </a>
  </div>

  <div class="uk-navbar-right overlay-1 uk-hidden@m uk-visible">
    {* <a class="uk-navbar-item" uk-toggle="target: .overlay-cart" href="#" uk-icon="icon:cart; ratio: 2" uk-tooltip="Корзина"></a> *}
    {* <a class="uk-navbar-item" uk-toggle="target: #offcanvas-right" href="#sign-in" {if $loggedin}uk-icon="icon:user; ratio:1.2" uk-tooltip="{#Login#}"{else}uk-icon="icon:sign-in; ratio:1.2" uk-tooltip="{#Login_Button#}"{/if}></a> *}
  </div>
  {*/MOBILE NAV*}

  {* PC NAV *}
  <div class="uk-navbar-left overlay-1 uk-text-center uk-visible@m" uk-lightbox>
    {* <a data-caption="Карта" data-type="iframe" href="index.php?p=content&id=3&name=blank&area=1" class="uk-navbar-item" uk-tooltip="Показать на карте">
      <i uk-icon="icon:fa-map-marker; ratio: 1.2"></i> {$settings.Strasse}
    </a> *}
  </div>

  <div class="uk-navbar-center overlay-1 uk-visible@m">
    <a class="uk-navbar-item" uk-toggle="target: .overlay-1" href="#callback" uk-tooltip="Заказать обратный звонок">
      <i uk-icon="icon:fa-mobile; ratio: 1.2"></i> {$settings.Telefon}
    </a>
  {* <ul class="uk-navbar-nav uk-text-bold"> *}
{*     <li><a href="#">
      название
      <span class="uk-navbar-item" uk-icon="chevron-down"></span>
    </a>
    <div class="uk-navbar-dropdown uk-navbar-dropdown-bottom-center"
    uk-drop="cls-drop: uk-navbar-dropdown; boundary: .uk-navbar-container; boundary-align: true; pos: bottom-justify; flip: x">
      <div class="uk-navbar-dropdown-grid uk-child-width-expand@m" uk-grid>
        <div class="uk-first-column">
          <div uk-grid>

            <div class="uk-width-auto@m">

              <ul class="uk-tab-left" uk-tab="connect: #component-tab-left; animation: uk-animation-fade">
              {assign var=cid value=$smarty.request.cid}
              {foreach from=$MyShopNavi item=sn}
                <li><a class="{if $cid == $sn->Id || in_array($sn->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sn->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sn->Entry|translit}">
                  <span class="uk-margin-small-right">{$sn->Icon}</span>
                  {$sn->Entry|sanitize}
                </a></li>
                {/foreach}
              </ul>
            </div>

            <div class="uk-width-expand@m">

              <ul id="component-tab-left" class="uk-switcher">
              {foreach from=$MyShopNavi item=sn}
              <li>
                  {if !empty($sn->Sub1) && count($sn->Sub1)}
                  <div class="uk-grid-divider uk-child-width-expand@s" uk-grid>
                  {foreach from=$sn->Sub1 item=sub1}
                  <div>
                  <ul class="uk-nav">

                    <li class="uk-parent"><a class="{if $cid == $sub1->Id || in_array($sub1->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub1->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub1->Entry|translit}">
                      {$sub1->Entry|sanitize}</a></li>

                      {if !empty($sub1->Sub2) && count($sub1->Sub2)}
                      {foreach from=$sub1->Sub2 item=sub2}
                      <ul class="uk-nav-sub">

                          <li><a class="{if $cid == $sub2->Id || in_array($sub2->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub2->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub2->Name|translit}">
                            {$sub2->Name|sanitize}</a></li>

                            {if !empty($sub2->Sub3) && count($sub2->Sub3)}
                            {foreach from=$sub2->Sub3 item=sub3}
                            <ul class="uk-nav-sub">

                              <li><a class="{if $cid == $sub3->Id || in_array($sub3->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub3->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub3->Name|translit}">
                                {$sub3->Name|sanitize}</a></li>

                                {if !empty($sub3->Sub4) && count($sub3->Sub4)}
                                {foreach from=$sub3->Sub4 item=sub4}

                                   <li><a class="{if $cid == $sub4->Id || in_array($sub4->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub4->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub4->Name|translit}">
                                      {$sub4->Name|sanitize}</a></li>

                                {/foreach}
                                {/if}
                            </ul>
                            {/foreach}
                            {/if}
                      </ul>
                      {/foreach}
                      {/if}

                    </ul>
                    </div>
                    {/foreach}
                    </div>
                    {/if}

              </li>
              {/foreach}
              </ul>
            </div>

          </div>
        </div>
      </div>
    </div>
    </li> *}

    {* навигация пк*}
    {if !empty($SiteNavigation)}
    {foreach from=$SiteNavigation item=navi}
    {if empty($navi->sub_navi)}
      <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
        <a href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}">
          {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
          {$navi->title|sanitize}<br>
          {$navi->AltTitle|sanitize}
        </a>
      </li>
    {else}
      <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
        <a href="{$navi->document|escape:'html'}">
          {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
          {$navi->title|sanitize}<br>
          {$navi->AltTitle|sanitize}
          <span class="uk-margin-small-left" uk-icon="chevron-down"></span>
        </a>
        <div class="uk-margin-remove uk-padding-remove" uk-dropdown="animation: uk-animation-slide-top-small">
          <ul class="uk-nav uk-navbar-dropdown-nav">
          {foreach from=$navi->sub_navi item=sub_navi}
            <li class="{if !empty($sub_navi->active) || $sub_navi->document == $document} uk-active{/if}">
              <a href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
                {* <i class="{$sub_navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
                {$sub_navi->title|sanitize}<br>
                {$sub_navi->AltTitle|sanitize}
              </a>
            </li>
          {/foreach}
          </ul>
        </div>
      </li>
    {/if}
    {/foreach}
    </ul>
    {/if}
  </div>

  {* <div class="uk-visible@m uk-navbar-right overlay-1">
    <a class="uk-navbar-toggle" uk-icon="icon:cart; ratio: 1" uk-toggle="target: .overlay-cart" href="#cart" uk-tooltip="Корзина"></a>
    {if $loggedin}
      <a class="uk-navbar-item" uk-toggle="target: #offcanvas-right" href="#sign-in">
      <i uk-icon="icon:user; ratio:1.2"></i> {#Login#}</a>
      {else}
      <a class="uk-navbar-item" uk-toggle="target: #offcanvas-right" href="#sign-in">
      <i uk-icon="icon:sign-in; ratio:1.2"></i> {#Login_Button#}</a>
    {/if}
  </div> *}
  {* /PC NAV *}


  {* overlay callback *}
  <div class="uk-navbar-left uk-flex-1 uk-background-default overlay-1" hidden>
    <div class="uk-navbar-item uk-width-expand">
      {contact id=2 tpl='callback.tpl'}{* contact/ *}
      <a class="uk-navbar-toggle" uk-icon="icon:close; ratio: 1.2" uk-toggle="target:.overlay-1;" href="#"></a>
    </div>
  </div>

  </nav>

{*MOB. AND PC OFFCANVAS SIDE RIGHT*}
<div id="offcanvas-right" uk-offcanvas="flip:true; overlay:true; mode:slide">
<div class="uk-offcanvas-bar uk-padding-small">

    <button class="uk-offcanvas-close uk-width-1-1" type="button" uk-close></button>

    {$user_login}

    {* моб.категории магазина *}
    {* <ul class="uk-nav-default uk-nav-parent-icon" uk-nav> *}
    {* {foreach from=$MyShopNavi item=sn}
    <li class="uk-parent">
    <a class="top-item {if $cid == $sn->Id || in_array($sn->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sn->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sn->Entry|translit}">
      <span class="uk-margin-small-right">{$sn->Icon}</span>
      {$sn->Entry|truncate: 22|sanitize}
    </a>
    {if !empty($sn->Sub1) && count($sn->Sub1)}
          <ul class="uk-nav-sub">
          {foreach from=$sn->Sub1 item=sub1}
                <li >
                  <a class="{if $cid == $sub1->Id || in_array($sub1->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub1->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub1->Entry|translit}">
                    {$sub1->Entry|sanitize}
                  </a></li>
                  {if !empty($sub1->Sub2) && count($sub1->Sub2)}
                          {foreach from=$sub1->Sub2 item=sub2}
                              <li>
                                <a class="{if $cid == $sub2->Id || in_array($sub2->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub2->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub2->Name|translit}">
                                  {$sub2->Name|sanitize}
                                </a></li>
                                {if !empty($sub2->Sub3) && count($sub2->Sub3)}
                                        {foreach from=$sub2->Sub3 item=sub3}
                                            <li>
                                              <a class="{if $cid == $sub3->Id || in_array($sub3->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub3->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub3->Name|translit}">
                                                {$sub3->Name|sanitize}
                                              </a></li>

                                              {if !empty($sub3->Sub4) && count($sub3->Sub4)}

                                                      {foreach from=$sub3->Sub4 item=sub4}
                                                          <li>
                                                            <a class="{if $cid == $sub4->Id || in_array($sub4->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub4->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub4->Name|translit}">
                                                              {$sub4->Name|sanitize}
                                                            </a>
                                                          </li>
                                                      {/foreach}
                                              {/if}
                                        {/foreach}
                                {/if}
                          {/foreach}
                  {/if}
            {/foreach}
            </ul>
    {/if}
</li>
<li class="uk-nav-divider"></li>
{/foreach} *}

    {* моб.навигация *}
    {if !empty($SiteNavigation)}
    {foreach from=$SiteNavigation item=navi}
    {if empty($navi->sub_navi)}
        <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
          <a href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}">
            {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
            {$navi->title|sanitize}
            {$navi->AltTitle|sanitize}
          </a>
        </li>
    {else}
        <li class="uk-parent {if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
          <a class="top-item">
            {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
            {$navi->title|sanitize}
            {$navi->AltTitle|sanitize}
          </a>
            <ul class="uk-nav-sub">
            {foreach from=$navi->sub_navi item=sub_navi}
              <li class="{if !empty($sub_navi->active) || $sub_navi->document == $document} uk-active{/if}">
                <a href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
                  {* <i class="{$sub_navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
                  {$sub_navi->title|sanitize}
                  {$sub_navi->AltTitle|sanitize}
                </a>
              </li>
            {/foreach}
            </ul>
        </li>
      {/if}
    {/foreach}
      </ul>
    {/if}

</div>
</div><!--offcanvas-right-->
