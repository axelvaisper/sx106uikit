{if !empty($SiteNavigation)}
<nav uk-navbar="mode:click" class="zindex-2 uk-navbar-container" data-uk-sticky="show-on-up: true; animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-default uk-box-shadow-large uk-navbar-sticky; cls-inactive: uk-navbar-transparent; top: 0">

    {*MOBILE NAV links*}
    <div class="uk-navbar-left overlay-2 uk-hidden@m uk-visible">
      <a href="#" uk-toggle="target: #offcanvas-mobnav"
      class="uk-margin-small-left" uk-icon="icon:menu; ratio: 2"></a>
    </div>

    <div class="uk-navbar-center overlay-2 uk-hidden@m uk-visible">
      <a href="/" class="uk-navbar-item">
        <img src="{$imgpath}/page/logo.svg" width="45" height="45" alt="">
      </a>
    </div>

    <div class="uk-navbar-right overlay-2 uk-hidden@m uk-visible">
      <a class="uk-navbar-item uk-button uk-button-text" href="#order" uk-scroll>Заказать</a>
    </div>
    {*/MOBILE NAV*}

  {* PC NAV *}
  <div class="uk-navbar-left overlay-2  uk-text-center uk-visible@m">
    <a href="/" class="uk-navbar-item uk-logo">
      <img src="{$imgpath}/page/logo.svg" width="45" height="45" alt="">
      <div class="uk-margin-small-left uk-dark uk-margin-small-bottom uk-visible@l">
        <small>{$settings.Firma|sanitize}</small>
        <h6 class="uk-margin-remove uk-text-bold uk-text-uppercase">{$settings.Seitenname|sanitize}</h6>
      </div>
    </a>
  </div>

  <div class="uk-navbar-center overlay-2  uk-visible@m">
  <ul class="uk-navbar-nav uk-text-bold">


  {if get_active('shop')}{* категории магазина пк*}
    <li><a href="#">
      {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
      База<br>недвижимости
      <span class="uk-margin-small-left" uk-icon="chevron-down"></span>
    </a>
    <div class="uk-navbar-dropdown uk-margin-remove uk-padding-remove" uk-drop="cls-drop: uk-navbar-dropdown; boundary: !nav; boundary-align: true; pos: bottom-justify; flip: x; animation: uk-animation-slide-bottom-small; mode:click">
      <div class="uk-navbar-dropdown-grid uk-margin-small-left" uk-grid>

              {* таб меню *}
              <ul class="uk-tab-right uk-width-auto" uk-tab="connect: #tab-cat; animation: uk-animation-slide-top-small">
              {assign var=cid value=$smarty.request.cid}
              {foreach from=$MyShopNavi item=sn}
                <li><a class="{if $cid == $sn->Id || in_array($sn->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sn->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sn->Entry|translit}">
                  <span>{$sn->Icon}</span>
                  {$sn->Entry|sanitize}
                </a></li>
                {/foreach}
              </ul>



              {* таб контент *}
              <ul id="tab-cat" class="uk-switcher uk-width-expand">
              {foreach from=$MyShopNavi item=sn}
              <li>
                {* <a class="{if $cid == $sn->Id || in_array($sn->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sn->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sn->Entry|translit}">
                          {$sn->Icon} {$sn->Entry|sanitize}
                        </a> *}
                  {if !empty($sn->Sub1) && count($sn->Sub1)}
                  <div class="uk-grid-divider uk-grid-collapse uk-margin-remove uk-child-width-expand" uk-grid>
                  {foreach from=$sn->Sub1 item=sub1}
                  <div>
                  <ul class="uk-nav">

                    <li class="uk-parent"><a class="{if $cid == $sub1->Id || in_array($sub1->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub1->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub1->Entry|translit}">
                      {$sub1->Entry|sanitize}</a></li>

                      {if !empty($sub1->Sub2) && count($sub1->Sub2)}
                      {foreach from=$sub1->Sub2 item=sub2}
                      <ul class="uk-nav-sub">

                          <li><a class="{if $cid == $sub2->Id || in_array($sub2->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub2->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub2->Name|translit}">
                            {$sub2->Name|sanitize}</a></li>

                            {if !empty($sub2->Sub3) && count($sub2->Sub3)}
                            {foreach from=$sub2->Sub3 item=sub3}
                            <ul class="uk-nav-sub">

                              <li><a class="{if $cid == $sub3->Id || in_array($sub3->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub3->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub3->Name|translit}">
                                {$sub3->Name|sanitize}</a></li>

                                {if !empty($sub3->Sub4) && count($sub3->Sub4)}
                                {foreach from=$sub3->Sub4 item=sub4}

                                   <li><a class="{if $cid == $sub4->Id || in_array($sub4->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub4->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub4->Name|translit}">
                                      {$sub4->Name|sanitize}</a></li>

                                {/foreach}
                                {/if}
                            </ul>
                            {/foreach}
                            {/if}
                      </ul>
                      {/foreach}
                      {/if}

                    </ul>
                    </div>
                    {/foreach}
                    </div>
                    {/if}

              </li>
              {/foreach}
              </ul>

      </div>
    </div>
    </li>
    {/if}

    {* навигация пк*}
    {foreach from=$SiteNavigation item=navi}
    {if empty($navi->sub_navi)}
      <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
        <a href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}" uk-scroll>
          {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
          {$navi->title|sanitize}<br>
          {$navi->AltTitle|sanitize}
        </a>
      </li>
    {else}
      <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
        <a href="{$navi->document|escape:'html'}">
          {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
          {$navi->title|sanitize}<br>
          {$navi->AltTitle|sanitize}
          <span class="uk-margin-small-left" uk-icon="icon:chevron-down;ratio:1"></span>
        </a>
        <div class="uk-margin-remove uk-padding-remove" uk-dropdown="animation: uk-animation-slide-top-small; mode:click">
          <ul class="uk-nav uk-navbar-dropdown-nav">
          {foreach from=$navi->sub_navi item=sub_navi}
            <li class="{if !empty($sub_navi->active) || $sub_navi->document == $document} uk-active{/if}">
              <a href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
                {* <i class="{$sub_navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
                {$sub_navi->title|sanitize}<br>
                {$sub_navi->AltTitle|sanitize}
              </a>
            </li>
          {/foreach}
          </ul>
        </div>
      </li>
    {/if}
    {/foreach}

  </ul>
  </div>

  <div class="uk-visible@m uk-navbar-right overlay-2 ">
     <a class="uk-navbar-item uk-button uk-button-primary uk-light" href="#order" uk-scroll>Связаться</a>
  </div>



  {* overlay search *}
  <div class="uk-navbar-left uk-flex-1 uk-background-default uk-box-shadow-large uk-dark overlay-2" hidden>
    <div class="uk-navbar-item uk-width-expand">
      {$shop_search_small}
    </div>
    <a class="uk-navbar-toggle" uk-icon="icon:close; ratio: 2" uk-toggle="target:.overlay-2" href="#"></a>
  </div>

</nav>

{*MOB. AND PC OFFCANVAS SIDE LEFT*}
<div id="offcanvas-mobnav" uk-offcanvas="overlay: true; mode: slide;">
  <div class="uk-offcanvas-bar uk-padding-remove">

    <button class="uk-offcanvas-close uk-width-1-1" type="button" uk-close></button>

    <div>
    <ul class="uk-nav-primary uk-nav-parent-icon" uk-nav>

  {if get_active('shop')}{* моб.категории магазина *}
    {foreach from=$MyShopNavi item=sn}
    <li class="uk-parent">
    <a class="top-item {if $cid == $sn->Id || in_array($sn->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sn->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sn->Entry|translit}">
      <span class="uk-margin-small-right">{$sn->Icon}</span>
      {$sn->Entry|truncate: 22|sanitize}
    </a>
    {if !empty($sn->Sub1) && count($sn->Sub1)}
          <ul class="uk-nav-sub">
          {foreach from=$sn->Sub1 item=sub1}
          <li class="uk-nav-divider"></li>
                <li >
                  <a class="{if $cid == $sub1->Id || in_array($sub1->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub1->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub1->Entry|translit}">
                    {$sub1->Entry|sanitize}
                  </a></li>
                  {if !empty($sub1->Sub2) && count($sub1->Sub2)}
                              <li class="uk-nav-divider"></li>
                          {foreach from=$sub1->Sub2 item=sub2}
                              <li>
                                <a class="{if $cid == $sub2->Id || in_array($sub2->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub2->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub2->Name|translit}">
                                  {$sub2->Name|sanitize}
                                </a></li>
                                {if !empty($sub2->Sub3) && count($sub2->Sub3)}
                                        {foreach from=$sub2->Sub3 item=sub3}
                                            <li>
                                              <a class="{if $cid == $sub3->Id || in_array($sub3->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub3->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub3->Name|translit}">
                                                {$sub3->Name|sanitize}
                                              </a></li>

                                              {if !empty($sub3->Sub4) && count($sub3->Sub4)}

                                                      {foreach from=$sub3->Sub4 item=sub4}
                                                          <li>
                                                            <a class="{if $cid == $sub4->Id || in_array($sub4->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub4->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub4->Name|translit}">
                                                              {$sub4->Name|sanitize}
                                                            </a>
                                                          </li>
                                                      {/foreach}
                                              {/if}
                                        {/foreach}
                                {/if}
                          {/foreach}
                  {/if}
            {/foreach}
            </ul>
    {/if}
    </li>
    <li class="uk-nav-divider"></li>
    {/foreach}
  {/if}

    {* моб.навигация *}
    {foreach from=$SiteNavigation item=navi}
    {if empty($navi->sub_navi)}
        <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
          <a href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}">
            {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
            {$navi->title|sanitize}
            {$navi->AltTitle|sanitize}
          </a>
        </li>
    {else}
        <li class="uk-parent {if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
          <a {* href="{$navi->document|escape:'html'}" *} class="top-item">
            {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
            {$navi->title|sanitize}
            {$navi->AltTitle|sanitize}
          </a>
            <ul class="uk-nav-sub">
            {foreach from=$navi->sub_navi item=sub_navi}
              <li class="{if !empty($sub_navi->active) || $sub_navi->document == $document} uk-active{/if}">
                <a href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
                  {* <i class="{$sub_navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
                  {$sub_navi->title|sanitize}
                  {$sub_navi->AltTitle|sanitize}
                </a>
              </li>
            {/foreach}
            </ul>
        </li>
        {/if}
    {/foreach}
      </ul>
    </div>
  </div>
</div>

{/if}
