<head>
<meta name="robots" content="{$robots}" />
<meta charset="{$charset}" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="theme-color" content="#fff">
<meta name='google' value='notranslate' />

<title>{$pagetitle}</title>
<meta name="keywords" content="{$keywords}" />
<meta name="description" content="{$description}" />

<meta name="author" content="{$settings.Seitenbetreiber}" />
<meta name="generator" content="{#meta_generator#}" />

<link rel="apple-touch-icon" sizes="180x180" href="{$baseurl}/apple-touch-icon.png?v=1">
<link rel="icon" type="image/png" sizes="32x32" href="{$baseurl}/favicon-32x32.png?v=1">
<link rel="icon" type="image/png" sizes="16x16" href="{$baseurl}/favicon-16x16.png?v=1">
<link rel="manifest" href="{$baseurl}/site.webmanifest?v=1">
<link rel="mask-icon" href="{$baseurl}/safari-pinned-tab.svg?v=1" color="#5bbad5">
<link rel="shortcut icon" href="{$baseurl}/favicon.ico?v=1">
<meta name="msapplication-TileColor" content="#ece470">
<meta name="theme-color" content="#ffffff">

{if $settings.meta_google == 1 && !empty($settings.code_google)}<meta name="google-site-verification" content="{$settings.code_google}" />{/if}

{if $settings.meta_yandex == 1 && !empty($settings.code_yandex)}<meta name="yandex-verification" content="{$settings.code_yandex}" />{/if}

{* <a rel="author" href="ССЫЛКА НА G+ АККАУНТ">ИМЯ ФАМИЛИЯ</a>  *}
<meta property="og:locale" content="{$langcode}">
<meta property="og:title" content="{$pagetitle}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{$baseurl}" />
<meta property="og:image" content="{$baseurl}/uploads/content/onas.jpg" />

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="{$pagetitle}">
<meta name="twitter:description" content="{$description}">
<meta name="twitter:image" content="{$baseurl}/uploads/content/onas.jpg">

{if !empty($canonical)}<link rel="canonical" href="{$baseurl}/{$canonical}" />{/if}

<link type="application/atom+xml" rel="alternate" title="{$settings.Seitenname|sanitize}" href="{$baseurl}/index.php?p=rss&amp;area={$area}" />

{if get_active('News')}<link type="application/atom+xml" rel="alternate" title="{#Newsarchive#}" href="{$baseurl}/index.php?p=rss&amp;area={$area}&amp;action=news" />{/if}

{if get_active('articles')}<link type="application/atom+xml" rel="alternate" title="{#Gaming_articles#}" href="{$baseurl}/index.php?p=rss&amp;area={$area}&amp;action=articles" />{/if}

{if get_active('forums')}<link type="application/atom+xml" rel="alternate" title="{#Forums_Title#}" href="{$baseurl}/index.php?p=rss&amp;area={$area}&amp;action=forum" />{/if}

{* СТИЛИ *}

{include file="$incpath/head/headcss.tpl"}
{style file="{$csspath}/uikit.css?3" position='head' priority='1000'}
{style file="{$csspath}/uikit-custom.css?3" position='head' priority='1000'}
{style file="{$csspath}/style.css?3" position='head' priority='1000'}
{style file="{$csspath}/home.css?3" position='head' priority='900'}
{if get_active('partners')}  {style file="{$csspath}/partner.css"    position='head' priority='200'} {/if}
{if get_active('newsletter')}{style file="{$csspath}/newsletter.css" position='head' priority='200'} {/if}
{*плагины*}

{* {style file="{$csspath}/colorbox.css" position='head' priority='800'} *}

{* СКРИПТЫ *}

{script file="{$themepath}/js/jquery-3.3.1.min.js" position='head' priority='1000'}
{script file="{$themepath}/js/uikit.js?3" position='head' priority='900'}
{script file="{$themepath}/js/uikit-icon.js?3" position='head' priority='800'}
{script file="{$themepath}/js/vk_openapi.min.js?3" position='head' priority='800'}

{script file="{$jspath}/jpatch.js" position='head' priority='1000'}
{* {script file="{$jspath}/jcolorbox.js" position='head' priority='800'} *}
{script file="{$jspath}/jcookie.js" position='head' priority='800'}
{script file="{$jspath}/jform.js" position='head' priority='800'}
{script file="{$jspath}/jtextcopy.js" position='head' priority='800'}

{* плагины *}
{* {script file="{$themepath}/js/jmaskedinput.js" position='head' priority='800'} *}

{script file="{$themepath}/js/main.js" position='head' priority='800'}

{result type='style'  format='file' position='head'}
{result type='style'  format='code' position='head'}
{result type='script' format='file' position='head'}
{result type='script' format='code' position='head'}

<script>
  VK.Retargeting.Init('VK-RTRG-268912-7v6Ec');
  VK.Retargeting.Hit();

$(function() {

  $('#preloader').fadeOut('slow', function () { $(this).remove(); });

  // $('[data-toggle="tooltip"]').tooltip();
  {if get_active('shop_bewertung')}$('.rating-star').rating();{/if}
  $('body').textcopy({ text: '{#MoreDetails#}' });

  {* $("#num_callback").mask("+7 (999) 999-99-99"); *}

});
</script>

{result type='code' format='code' position='head'}
</head>
