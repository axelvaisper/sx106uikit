{literal}
<style>
@charset "UTF-8";
/*аним.плавной загрузки */
#preloader{overflow:hidden;background:var(--gray-vk);height:100%;left:0;position:fixed;top:0;width:100%;z-index:999999}
.uk-spinner,progress{left:calc(50% - 20px);position:relative;top:calc(50% - 20px);z-index:9}
:focus {outline:none}
select:-moz-focusring{color:transparent;text-shadow:0 0 0 #000}
</style>
{/literal}