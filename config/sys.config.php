<?php
########################################################################
# ******************  SX CONTENT MANAGEMENT SYSTEM  ****************** #
# *       Copyright © Alexander Voloshin * All Rights Reserved       * #
# ******************************************************************** #
# *  http://sx-cms.ru   *  cms@sx-cms.ru  *   http://www.status-x.ru * #
# ******************************************************************** #
########################################################################
if (!defined('SX_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}

// Режим отладки
$config['debug'] = '1';
// 1-включено, 0-сайт в рабочем режиме

// Режим ведения логов
$config['loger'] = 'ca6p@ya.ru';
// 0-не пишем, 1-пишем в папку temp/logs, 2-пишем по настройкам PHP, шлём на почту - впишите адрес

// Настройка работы системы по расписанию
$config['cron']  = '1';
// 1-подсчитывая хиты, 0-через cron сервера

// Настройка отвечает за работу SSL
$config['ssl']   = '2';
// 0 - http://, 1 - https://, 2 - http:// и https://

// Включает https только на определённых страницах.
$config['https'] = array('*:*');
// Например array('shop:callback') включит https только на странице index.php?p=shop&action=callback, * - любое значение. Пример array('shop:*', '*:callback','*:*')

// Вывод ID шаблонов в комментах по типу: <!-- имя/путь шаблона --> и <!-- End имя/путь шаблона -->
$config['tplcleanid']   = '0';
// 1 - Удаляет ID, 0 - выводит. При изменении, очистка кеша шаблона обязательна ( кнопка на дашборде )

// Отключение сайта, безопасно для работы с базой
$config['site']['aktiv'] = '1';
// 1 - сайт работает, 0 - сайт отключен

$config['site']['time']  = 'Идет обновление системы. Через некоторое время сайт будет снова доступен :)'; // Информация для посетителей
$config['site']['ip'] = '127.0.0.2'; // IP адрес для которого сайт остается активным, несколько адресов разделяются запятой без пробелов

## Страницы на которых запрещаем использовать магазин
$config['shop'] = array('showforums', 'showforum', 'showtopic', 'forum', 'forums', 'members', 'newpost', 'pn', 'addpost', 'user');

// Страницы на которых запрещаем использовать календарь
$config['kalendar'] = array('shop', 'showforums', 'showforum', 'showtopic', 'forum', 'forums', 'members', 'newpost', 'pn', 'addpost', 'user');

// Выдача тега noindex,nofollow для указанных страниц (запрет индексирования поисковиками)
$config['noindex'][] = 'q=empty';
$config['noindex'][] = 'title_asc';
$config['noindex'][] = 'title_desc';
$config['noindex'][] = 'price_asc';
$config['noindex'][] = 'price_desc';
$config['noindex'][] = 'klick_asc';
$config['noindex'][] = 'klick_desc';
$config['noindex'][] = 'date_asc';
$config['noindex'][] = 'date_desc';
$config['noindex'][] = 'art_asc';
$config['noindex'][] = 'art_desc';
$config['noindex'][] = 'messages';
$config['noindex'][] = 'search';
$config['noindex'][] = 'forum-action';
$config['noindex'][] = 'print.html';
$config['noindex'][] = 'print=1';

error_reporting($config['debug'] != '1' ? 0 : E_ALL);
ini_set('magic_quotes_runtime', 0);
ini_set('magic_quotes_sybase', 0);
ini_set('arg_separator.input', '&amp;');
ini_set('arg_separator.output', '&amp;');
ini_set('url_rewriter.tags', '1');
ini_set('session.use_cookies', 1);       // Храним session id куках
ini_set('session.use_only_cookies', 1);  // Храним session id только в куках
ini_set('session.use_trans_sid', 0);     // Отключаем прозрачную поддержку sid
