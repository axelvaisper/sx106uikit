//отключаем при скроле hover у элементов
var root = document.documentElement, timer;
window.addEventListener( 'scroll', function() {
clearTimeout( timer );
if ( !root.style.pointerEvents ) {
    root.style.pointerEvents = 'none';
}
timer = setTimeout(function() {
    root.style.pointerEvents = '';
}, 300 );
}, false );

function focusArea(elem, height) {
    var cache = elem.style.height;
    elem.style.height = height + 'px';
    elem.onblur = function () {
        elem.style.height = cache;
    };
}
function uploadBrowser(typ, target, elemid) {
    var w = 1000;
    var h = 520;
    var left = screen.width ? (screen.width - w) / 2 : 0;
    var top = screen.height ? (screen.height - h) / 2 : 0;
    var url = '?pop=1&do=browser&typ=' + typ + '&target=' + target + '&mode=system&elemid=' + elemid;
    var features = 'scrollbars=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left;
    window.open(url, 'mpool', features);
}
function openWindow(seite, name, w, h, scroll) {
    if (typeof w === 'undefined' || w === '') {
        w = screen.width;
    }
    if (typeof h === 'undefined' || h === '') {
        h = screen.height;
    }
    var left = screen.width ? (screen.width - w) / 2 : 0;
    var top = screen.height ? (screen.height - h) / 2 : 0;
    var settings = 'height=' + h + ',width=' + w + ',top=' + top + ',left=' + left + ',scrollbars=' + scroll + ',resizable';
    window.open(seite, name, settings);
}
function printWindow(id) {
    var html = document.getElementById(id).innerHTML;
    html = html.replace(/&lt;/gi, '<');
    html = html.replace(/&gt;/gi, '>');
    var p = window.open('', null, 'height=520,width=780,toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes');
    var c = '<html><head></head><body style="font-family:arial,verdana;font-size:12px" onload="window.print();">' + html + '</body></html>';
    p.document.write(c);
    p.document.close();
}
function multiCheck() {
    var obj = document.kform;
    for (var i = 0; i < obj.elements.length; i++) {
        var e = obj.elements[i];
        if (e.name !== 'allbox' && e.type === 'checkbox' && !e.disabled) {
            e.checked = obj.allbox.checked;
        }
    }
}
function insertEditor(name, text) {
    var editor = CKEDITOR.instances[name];
    editor.insertHtml(text);
}

function toggleCookie(click, target, expires, basepath) {
    $(function () {
        if ($.cookie(click) === 'hide') {
            $('#' + target).css('display', 'none');
        }
        $('#' + click).on('click', function () {
            $('#' + target).slideToggle(
                    500,
                    function () {
                        var display = $('#' + target).css('display') === 'none' ? 'hide' : 'ss';
                        $.cookie(click, display, {expires: expires, path: basepath});
                    }
            );
        });
    });
}
function showNotice(text, time, overlay) {
    $(function () {
        if (typeof overlay === 'undefined' || overlay === '') {
            overlay = true;
        }
        $.blockUI({showOverlay: overlay, message: text, css: {cursor: 'pointer'}});
        setTimeout($.unblockUI, time);
    });
}
function closeWindow(reload) {
    $(function () {
        parent.$.colorbox.close();
        if (reload === true) {
            parent.location.href = parent.location;
        }
    });
}
function newWindow(url, width, height) {
    $(function () {
        $.colorbox({
            href: url,
            width: width + 'px',
            height: height + 'px',
            iframe: true
        });
    });
}

function closeIFrame(){
    $(function () {
  $('#ukclose').click(function(){
    UIkit.lightbox().hide();
  });
});
}
