<h4>{#Shop_articles_spez#} - {#HTML_Allowed#} ({$language.name.$langcode})</h4>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  {if !empty($Spez_Def_1)}
    <tr>
      <td class="row_left">{$Spez_Def_1}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_1">{$Spez_1}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_2)}
    <tr>
      <td class="row_left">{$Spez_Def_2}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_2">{$Spez_2}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_3)}
    <tr>
      <td class="row_left">{$Spez_Def_3}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_3">{$Spez_3}</textarea></td>
    </tr>
  {/if}

  {if !empty($Spez_Def_4)}
    <tr>
      <td class="row_left">{$Spez_Def_4}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_4">{$Spez_4}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_5)}
    <tr>
      <td class="row_left">{$Spez_Def_5}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_5">{$Spez_5}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_6)}
    <tr>
      <td class="row_left">{$Spez_Def_6}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_6">{$Spez_6}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_7)}
    <tr>
      <td class="row_left">{$Spez_Def_7}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_7">{$Spez_7}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_8)}
    <tr>
      <td class="row_left">{$Spez_Def_8}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_8">{$Spez_8}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_9)}
    <tr>
      <td class="row_left">{$Spez_Def_9}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_9">{$Spez_9}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_10)}
    <tr>
      <td class="row_left">{$Spez_Def_10}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_10">{$Spez_10}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_11)}
    <tr>
      <td class="row_left">{$Spez_Def_11}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_11">{$Spez_11}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_12)}
    <tr>
      <td class="row_left">{$Spez_Def_12}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_12">{$Spez_12}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_13)}
    <tr>
      <td class="row_left">{$Spez_Def_13}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_13">{$Spez_13}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_14)}
    <tr>
      <td class="row_left">{$Spez_Def_14}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_14">{$Spez_14}</textarea></td>
    </tr>
  {/if}
  {if !empty($Spez_Def_15)}
    <tr>
      <td class="row_left">{$Spez_Def_15}</td>
      <td class="row_right"><textarea cols="" rows="" class="input article_spez" name="Spez_15">{$Spez_15}</textarea></td>
    </tr>
  {/if}
</table>
