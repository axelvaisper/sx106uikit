<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{#LoginName#}</title>
{include file="$incpath/head/head-admin-login.tpl"}
</head>
<body id="login">
  {$content}
</body>
</html>
