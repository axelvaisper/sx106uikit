<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{$title|default:'Панель управления'}</title>
{include file="$incpath/head/head-admin-blank.tpl"}
</head>
<body id="blank">
  <div id="com_loader"><img src="{$imgpath}/loading_big.gif" alt="" /></div>
  <div id="com" style="display: none">
  {if !empty($mesage_save)}{$mesage_save}{/if}
  {$content}
</div>
</body>
</html>
