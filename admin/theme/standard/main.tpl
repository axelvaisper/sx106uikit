<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{$title|default:'Панель управления'}</title>
{include file="$incpath/head/head-admin.tpl"}
</head>
<body style="display:none">

{* аним.загрузки *}
<div id="preloader"><div uk-spinner="ratio: 3"></div></div>

<div class="uk-offcanvas-content">

  {include file="$incpath/adminbar.tpl"}
  <div class="uk-grid-small uk-margin-medium-top" uk-grid>

    {include file="$incpath/navielements/navielements.tpl"}
    <main data-uk-height-viewport="expand: true" class="uk-width-expand">

        {if !empty($mesage_save)}{$mesage_save}{/if}
        {$content}
        <footer class="uk-section uk-section-small uk-text-center">
          <hr>
          Работает на <a href="https://sx-cms.ru">
          <img src="{$imgpath}/logo.png" alt="logo">
          SX CMS</a> и 
          <a href="http://getuikit.com"><span data-uk-icon="uikit"></span> UI KIT</a>
      </footer>
    </main>
  </div>


  <!-- OFFCANVAS RIGHT-->
  <div id="offcanvas-right" data-uk-offcanvas="flip: true; overlay: true">
    <div class="uk-offcanvas-bar uk-offcanvas-bar-animation uk-offcanvas-slide">
      <button class="uk-offcanvas-close uk-close uk-icon" type="button" data-uk-close></button>

      {#Global_LoggedInAs#} <strong>{$smarty.session.user_name}</strong>

          {$section_switch}
          {$theme_switch}

      <ul class="uk-nav uk-nav-default">
        <li><a href="index.php?logout=1">{#Global_Logout#}</a></li>
        <li class="uk-parent">
          <a href="#">Parent</a>
          <ul class="uk-nav-sub">
            <li><a href="#">Sub item</a></li>
            <li><a href="#">Sub item</a></li>
          </ul>
        </li>
        <li class="uk-nav-header">Header</li>
        <li><a href="#js-options"><span class="uk-margin-small-right uk-icon" data-uk-icon="icon: table"></span> Item</a></li>
        <li><a href="#"><span class="uk-margin-small-right uk-icon" data-uk-icon="icon: thumbnails"></span> Item</a></li>
        <li class="uk-nav-divider"></li>
        <li><a href="#"><span class="uk-margin-small-right uk-icon" data-uk-icon="icon: trash"></span> Item</a></li>
      </ul>
      <h3>Title</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
  </div>
  <!-- /OFFCANVAS RIGHT-->

  {if perm('notes') && $admin_settings.Aktiv_Notes == 1}
  <div id="offcanvas-notes" data-uk-offcanvas="flip: true; overlay: true">
      {include file="$incpath/notes/notes.tpl"}
  </div>
  {/if}

</div>

</body>
</html>
