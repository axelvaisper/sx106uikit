<meta http-equiv="Content-Type" content="text/html; charset={$charset}" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="imagetoolbar" content="no" />
<meta name="robots" content="none" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link type="text/css" rel="stylesheet" href="{$csspath}/uikit.css" />
<link type="text/css" rel="stylesheet" href="{$csspath}/uikit-custom.css" />
<link type="text/css" rel="stylesheet" href="{$csspath}/ui.css" />
<link type="text/css" rel="stylesheet" href="{$csspath}/colorbox.css" />
<link type="text/css" rel="stylesheet" href="{$csspath}/style.css" />
{* <link type="text/css" rel="stylesheet" href="{$csspath}/gridtab.css" /> *}
{* <link type="text/css" rel="stylesheet" href="{$csspath}/fontello/css/fontello.css" /> *}

<script src="{$themepath}/js/jquery-2.1.1.js"></script>
<script src="{$jspath}/jpatch.js"></script>
<script src="{$jspath}/jquery.ui.js"></script>
<script src="{$jspath}/jtooltip.js"></script>
<script src="{$jspath}/jcolorbox.js"></script>

<script src="{$themepath}/js/uikit.js"></script>
<script src="{$themepath}/js/uikit-fa-icons.js"></script>


<script src="{$themepath}/js/main.js"></script>

<script src="{$jspath}/jcookie.js"></script>
<script src="{$jspath}/jtoggle.js"></script>
<script src="{$jspath}/jvalidate.js"></script>
<script src="{$jspath}/jsuggest.js"></script>
<script src="{$jspath}/jblock.js"></script>
<script src="{$jspath}/jform.js"></script>
<script src="{$themepath}/js/ddaccordion.js"></script>
{* <script src="{$themepath}/js/gridtab.js"></script> *}

<script>
<!-- //
$(function() {
    $('.colorbox').colorbox({ height: '97%', width: '90%', iframe: true, fastIframe: false });
    $('.colorbox-sm').colorbox({ height: '70%', width: '60%', iframe: true, fastIframe: false });
    $('.stip').tooltip();
    $('#com').show();setTimeout(function(){ $('#com_loader').hide(); }, 500);

    {* $('.gridtab_addproduct').gridtab({
      grid: 4,
      borderWidth: 2,
      contentPadding: 10,
      config:{
        layout: 'tab',
        activeTab:1
      },
      responsive: [{
          breakpoint: 600,
          settings: {
              grid: 1,
              contentPadding: 10
          }
      }]
  }); *}

});
//-->
</script>
