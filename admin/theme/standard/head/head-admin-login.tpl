<meta charset="{$charset}"/>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />

<meta name="robots" content="none" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link type="text/css" rel="stylesheet" href="{$csspath}/uikit.css" />
<link type="text/css" rel="stylesheet" href="{$csspath}/uikit-custom.css" />
<link type="text/css" rel="stylesheet" href="{$csspath}/style.css" />

<script src="{$themepath}/js/jquery-2.1.1.js"></script>
<script src="{$jspath}/jpatch.js"></script>
<script src="{$jspath}/jtooltip.js"></script>
<script src="{$themepath}/js/main.js"></script>

<script src="{$jspath}/jcookie.js"></script>
<script src="{$jspath}/jvalidate.js"></script>
<script src="{$jspath}/jsuggest.js"></script>
<script src="{$jspath}/jform.js"></script>

<script>
<!-- //
$(function() {
  $('.stip').tooltip();
  $('#com').show();setTimeout(function(){ $('#com_loader').hide(); }, 500);
});
//-->
</script>
