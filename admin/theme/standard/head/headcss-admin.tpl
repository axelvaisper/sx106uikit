{literal}
<style>
@charset "UTF-8";

/* переменные */
:root {
    --blue: #007bff;
    --blue-light:#d8e9f2;
    --blue-vk:#2a5885;
    --indigo: #6610f2;
    --purple: #6f42c1;
    --pink: #e83e8c;
    --red: #dc3545;
    --orange: #fd7e14;
    --yellow: #ffd086;
    --green: #28a745;
    --teal: #20c997;
    --cyan: #17a2b8;
    --white: #fff;
    --gray: #6c757d;
    --gray-vk:#edeef0;
    --gray-dark: #343a40;
    --gray-light:#bebebe;

    --primary: #007bff;
    --secondary: #6c757d;
    --success: #28a745;
    --info: #17a2b8;
    --warning: #ffc107;
    --danger: #dc3545;
    --light: #f8f9fa;
    --dark: #343a40;

    --c-link:var(--blue-vk);

    --breakpoint-s: 576px;
    --breakpoint-m: 768px;
    --breakpoint-l: 992px;

    --font-family-main: -apple-system, BlinkMacSystemFont, "Ubuntu", "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
    --font-family-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Ubuntu mono", "Liberation Mono", "Courier New", monospace;
    --font-size: 1rem;
  }

/* переменные адаптивности*/
media all and (max-width: 600px) {
    :root {
      --font-size: 12px;
    }
  }

/*аним.плавной загрузки */
#preloader{overflow:hidden;background:var(--gray-vk);height:100%;left:0;position:fixed;top:0;width:100%;z-index:999999}
.uk-spinner{left:calc(50% - 20px);position:relative;top:calc(50% - 20px);z-index:9}
</style>
{/literal}