<div class="header">{#Download_categs#}</div>
<div class="subheaders">
  <a class="colorbox-sm" title="{#GlobalAddCateg#}" href="index.php?do=downloads&amp;sub=addcateg&amp;noframes=1"><img src="{$imgpath}/add.png" alt="" /> {#GlobalAddCateg#}</a>&nbsp;&nbsp;&nbsp;
    {if $admin_settings.Ahelp == 1}
    <a class="colorbox" href="index.php?do=help&amp;sub={$helpquery}&amp;noframes=1"><img src="{$imgpath}/s_help.png" alt="" /> {#GlobalHelp#}</a>&nbsp;&nbsp;&nbsp;
    {/if}
  <a class="colorbox" href="index.php?do=support&amp;sub=send_order&amp;noframes=1"><img src="{$imgpath}/send.png" alt="" /> {#SendOrder#}</a>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
  <tr class="headers">
    <td class="headers">{#Global_Name#}</td>
    <td class="headers">{#Navigation_doc#}</td>
    <td class="headers">{#Global_Actions#}</td>
  </tr>
  {foreach from=$Categs item=g}
    <tr class="{cycle values='second,first'}">
      <td>
        {if $g->Parent_Id == 0}
          <strong>{$g->Name|sanitize}</strong>
        {else}
          {$g->visible_title|sanitize}
        {/if}
      </td>
      <td><a target="_blank" href="../index.php?p=downloads&amp;area={$area}&amp;categ={$g->Id}&amp;name={$g->Name|translit}">index.php?p=downloads&area={$area}&categ={$g->Id}&name={$g->Name|translit}</a></td>
      <td>
        <a class="colorbox-sm stip" title="{$lang.Global_CategEdit|sanitize}" href="index.php?do=downloads&amp;sub=editcateg&amp;id={$g->Id}&amp;noframes=1"><img src="{$imgpath}/edit.png" alt="" /></a>
          {if perm('downloads_categs')}
          <a onclick="return confirm('{#ConfirmGlobal#}{$g->Name|jsspecialchars}');" href="index.php?do=downloads&amp;sub=delcateg&amp;id={$g->Id}&amp;backurl={$backurl}"><img src="{$imgpath}/delete.png" alt="" /></a>
          {/if}
      </td>
    </tr>
  {/foreach}
</table>
