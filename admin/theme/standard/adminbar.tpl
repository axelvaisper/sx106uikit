<header class="adminbar uk-position-fixed uk-box-shadow-large">
    <nav class="uk-navbar" data-uk-navbar="mode:click">

      <div class="uk-navbar-left">
        <a class="uk-navbar-item uk-logo" data-uk-tooltip title="{#Global_Site#}" href="{$baseurl}" target="_blank">
          <img height="40" width="40" src="{$baseurl}/theme/standard/images/page/logo.svg" alt=""> 
          <span class="uk-margin-small-left uk-visible@m">{$settings.Seitenname|sanitize}</span>
        </a>
        <a class="uk-navbar-toggle" data-uk-toggle data-uk-navbar-toggle-icon uk-toggle="target: #offcanvas-left"></a>
        <ul class="uk-navbar-nav">
          <li><a href="index.php">{#StartPage#}</a></li>
        </ul>
      </div>

      <div class="uk-navbar-center">
        <ul class="uk-navbar-nav">
          <li><a href="#" data-uk-icon="icon: settings"></a></li>
        </ul>
      </div>

      <div class="uk-navbar-right">
        <ul class="uk-navbar-nav">
          {if perm('notes') && $admin_settings.Aktiv_Notes == 1}
            <li><a id="notes" uk-toggle="target: #offcanvas-notes">{#Notes#}</a></li>
          {/if}
          <li><a href="#" data-uk-icon="icon: settings"></a></li>
          <li><a href="#" data-uk-icon="icon: cog"></a></li>
          <li><a class="uk-navbar-toggle" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-right"></a></li>
        </ul>
      </div>

    </nav>
</header>