{foreach from=$notes item=note}
<script>
<!-- //
$(function() {
    $('#delnotes_{$note->Id}').on('click', function() {
        var options = {
            target: '#outajax',
            url: 'index.php?do=notes&sub=delnotes&notid={$note->Id}',
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
    $('#editnotes_{$note->Id}').on('click', function() {
        var options = {
            target: '#outajax',
            url: 'index.php?do=notes&sub=editnotes&notid={$note->Id}&edit=1',
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
});
//-->
</script>

<div style="background:var(--gray-vk);padding:5px">
  {* <strong>{#Global_Date#}: </strong>*} 
  {$note->Datum|date_format: $lang.DateFormat}&nbsp;|&nbsp;
  {* <strong>{#Global_Author#}: </strong> *}
  <strong>{$note->Autor}</strong>&nbsp;|&nbsp;
  {* <strong>{#Global_Type#}: </strong> *}
  {if $note->Type == 'main'}{#NotesMain#}{else}{#NotesPub#}{/if}

  <div style="background:var(--white)">{$note->Text|nl2br}</div>

  {if $note->UserId == $smarty.session.benutzer_id || $smarty.session.benutzer_id == 1}
    &nbsp;|&nbsp;<a id="editnotes_{$note->Id}" href="javascript: void(0);">{#Edit#}</a>&nbsp;|&nbsp
    <a id="delnotes_{$note->Id}" href="javascript: void(0);">{#Global_Delete#}</a>
  {/if}
</div>
<hr>
{/foreach}
