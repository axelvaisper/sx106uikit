<script>
<!-- //
$(function() {
    $('#notes, #shownotes').on('click', function() {
        var options = {
            target: '#outajax',
            url: 'index.php?do=notes&sub=shownotes&type=all&key=' + Math.random(),
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
    $('#main_notes').on('click', function() {
        var options = {
            target: '#outajax',
            url: 'index.php?do=notes&sub=shownotes&type=main&key=' + Math.random(),
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
    $('#pub_notes').on('click', function() {
        var options = {
            target: '#outajax',
            url: 'index.php?do=notes&sub=shownotes&type=pub&key=' + Math.random(),
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
   $('#addnotes').on('click', function() {
        var options = {
            target: '#outajax',
            url: 'index.php?do=notes&sub=addnotes&key=' + Math.random(),
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
});
//-->
</script>


<div id="slidedown_content" class="uk-offcanvas-bar uk-padding-remove">
  <input class="uk-button uk-button-small uk-button-primary uk-width-1-1" id="addnotes" type="button" onclick="javascript: void(0);" value="{#Global_Add#}" />

  <div class="uk-button-group uk-width-1-1">
    <input class="uk-button uk-button-small uk-button-default uk-width-1-3" id="shownotes" type="button" onclick="javascript: void(0);" value="{#Global_All#}" />
    <input class="uk-button uk-button-small uk-button-default uk-width-1-3" id="main_notes" type="button" onclick="javascript: void(0);" value="{#NotesMain#}" />
    <input class="uk-button uk-button-small uk-button-default uk-width-1-3" id="pub_notes" type="button" onclick="javascript: void(0);" value="{#NotesPub#}" />
  </div>

  <div style="height:100vh;overflow: auto" id="outajax"> </div> 
</div>
