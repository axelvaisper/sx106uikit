{if perm('contact_forms')}
  <dt href="#"><i class="icon-copyright"></i><br>{#ContactForms#}</dt>
  <dd class="submenu">
    <ul>
      <li><a class="{if isset($smarty.request.do) && isset($smarty.request.do) && $smarty.request.do == 'contactforms'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=contactforms">{#Global_Overview#}</a></li>
      <li><a class="nav_subs colorbox" title="{#ContactForms_new#}" href="index.php?do=contactforms&amp;sub=new&amp;noframes=1">{#ContactForms_new#}</a></li>
    </ul>
  </dd>
{/if}
