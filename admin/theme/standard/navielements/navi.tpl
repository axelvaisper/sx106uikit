{if perm('navigation_edit')}
    <dt><i class="icon-user"></i><br>{#Navigation#}</dt>
    <dd>
      <ul>
        <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'navigation' && isset($smarty.request.sub) && $smarty.request.sub == 'list'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=navigation&amp;sub=list">{#Global_Overview#}</a></li>
        <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'navigation' && isset($smarty.request.sub) && $smarty.request.sub == 'list'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=navigation&sub=edit&id=1">{#Edit#}</a></li>
          {if admin_active('flashtag')}
          <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'navigation' && isset($smarty.request.sub) && $smarty.request.sub == 'flashtag'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=navigation&amp;sub=flashtag">{#Flashtag#}</a></li>
          {/if}
      </ul>
    </dd>
{/if}
