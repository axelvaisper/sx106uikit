{if perm('manufacturer') && admin_active('manufacturer')}
  <dt href="#"><i class="icon-plus"></i><br>{#Manufacturer#}</dt>
  <dd class="submenu">
    <ul>
      <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'manufacturer' && isset($smarty.request.sub) && $smarty.request.sub == 'overview'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=manufacturer&amp;sub=overview">{#Global_Overview#}</a></li>
      <li><a class="nav_subs colorbox" title="{#Manufacturer_new#}" href="index.php?do=manufacturer&amp;sub=new&amp;noframes=1">{#Manufacturer_new#}</a></li>
    </ul>
  </dd>
{/if}
