{if empty($smarty.request.noframes)}
<script>
<!-- //
{if $admin_settings.Navi_Anime == 1}
  $(function() {
      toggleCookie('navielements', 'offcanvas-left', 30, '{$basepath}');
  });
{/if}
//-->
</script>

{* MOBILE MENU *}
<aside id="offcanvas-left" data-uk-offcanvas="overlay: true">

  <div id="gridtab-mob" class="glossymenu uk-offcanvas-bar">
    {include file="$incpath/navielements/settings.tpl"}
    {include file="$incpath/navielements/theme.tpl"}
    {include file="$incpath/navielements/navi.tpl"}

    {include file="$incpath/navielements/insert.tpl"}
    {include file="$incpath/navielements/codewidgets.tpl"}
    {include file="$incpath/navielements/content.tpl"}

    {include file="$incpath/navielements/contact.tpl"}
    {include file="$incpath/navielements/media.tpl"}
    {include file="$incpath/navielements/banners.tpl"}

    {include file="$incpath/navielements/newsletter.tpl"}

    {include file="$incpath/navielements/shop.tpl"}
    {include file="$incpath/navielements/manufacturer.tpl"}
    {include file="$incpath/navielements/products.tpl"}

    {include file="$incpath/navielements/news.tpl"}
    {include file="$incpath/navielements/articles.tpl"}
    {include file="$incpath/navielements/linksdownloads.tpl"}
    {include file="$incpath/navielements/gallery.tpl"}

    {include file="$incpath/navielements/usergroups.tpl"}
    {include file="$incpath/navielements/users.tpl"}
    {include file="$incpath/navielements/forums.tpl"}

    {include file="$incpath/navielements/polls.tpl"}
    {include file="$incpath/navielements/faq.tpl"}

    {include file="$incpath/navielements/seo.tpl"}
    {include file="$incpath/navielements/roadmap.tpl"}
    {include file="$incpath/navielements/stats.tpl"}

    {include file="$incpath/navielements/cheats.tpl"}
    {include file="$incpath/navielements/other.tpl"}
    {include file="$incpath/settings/navi_modul.tpl"}
</div>

</aside>

{* PC MENU *}
<aside class="uk-width-1-4@m uk-width-1-5@l uk-visible@m">

  <div id="gridtab-pc" class="glossymenu">
    {include file="$incpath/navielements/settings.tpl"}
    {include file="$incpath/navielements/theme.tpl"}
    {include file="$incpath/navielements/navi.tpl"}

    {include file="$incpath/navielements/insert.tpl"}
    {include file="$incpath/navielements/codewidgets.tpl"}
    {include file="$incpath/navielements/content.tpl"}

    {include file="$incpath/navielements/contact.tpl"}
    {include file="$incpath/navielements/media.tpl"}
    {include file="$incpath/navielements/banners.tpl"}

    {include file="$incpath/navielements/newsletter.tpl"}

    {include file="$incpath/navielements/shop.tpl"}
    {include file="$incpath/navielements/manufacturer.tpl"}
    {include file="$incpath/navielements/products.tpl"}

    {include file="$incpath/navielements/news.tpl"}
    {include file="$incpath/navielements/articles.tpl"}
    {include file="$incpath/navielements/linksdownloads.tpl"}
    {include file="$incpath/navielements/gallery.tpl"}

    {include file="$incpath/navielements/usergroups.tpl"}
    {include file="$incpath/navielements/users.tpl"}
    {include file="$incpath/navielements/forums.tpl"}

    {include file="$incpath/navielements/polls.tpl"}
    {include file="$incpath/navielements/faq.tpl"}

    {include file="$incpath/navielements/seo.tpl"}
    {include file="$incpath/navielements/roadmap.tpl"}
    {include file="$incpath/navielements/stats.tpl"}

    {include file="$incpath/navielements/cheats.tpl"}
    {include file="$incpath/navielements/other.tpl"}
    {include file="$incpath/settings/navi_modul.tpl"}
</div>

</aside>
{/if}
